/**
 * @projectName   CSOT_EEAD_ALG2_DEMURA
 * @brief         Demura
 * @author        wxq
 * @date          2021-04-07 19:53
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#ifndef NVTTCON_H
#define NVTTCON_H
#include "tconfactor.h"
#include "tconbase.h"
typedef struct NT71102_Header {
  unsigned int header : 32;
  unsigned int PARAMETER_CRC : 16;
  unsigned int RESERVED00 : 8;
  unsigned int DEMURA_PLANE_NUM : 8;
  unsigned int DEMURA_TBL_H : 16;
  unsigned int DEMURA_TBL_V : 16;
  unsigned int DEMURA_BLK_H : 8;
  unsigned int DEMURA_BLK_V : 8;
  unsigned int RESERVED01 : 8;
  unsigned int RESERVED02 : 8;
  unsigned int RESERVED03 : 8;
  unsigned int RESERVED04 : 8;
  unsigned int RESERVED05 : 8;
  unsigned int RESERVED06 : 8;
  unsigned int RESERVED07 : 8;
  unsigned int RESERVED08 : 8;
  unsigned int RESERVED09 : 8;
  unsigned int RESERVED10 : 8;
  unsigned int LOWER_BOUND : 16;
  unsigned int UPPER_BOUND : 16;
  unsigned int RESERVED11 : 8;
  unsigned int RESERVED12 : 8;
  unsigned int RESERVED13 : 8;
  unsigned int RESERVED14 : 8;
  unsigned int RESERVED15 : 8;
  unsigned int RESERVED16 : 8;
  unsigned int RESERVED17 : 8;
  unsigned int RESERVED18 : 8;
  unsigned int RESERVED19 : 8;
  unsigned int RESERVED20 : 8;
  unsigned int RESERVED21 : 8;
  unsigned int RESERVED22 : 8;
  unsigned int PLANE00_LV : 16;
  unsigned int PLANE01_LV : 16;
  unsigned int PLANE02_LV : 16;
  unsigned int PLANE03_LV : 16;
  unsigned int PLANE04_LV : 16;
  unsigned int PLANE05_LV : 16;
  unsigned int PLANE06_LV : 16;
  unsigned int PLANE07_LV : 16;
  unsigned int PLANE08_LV : 16;
  unsigned int RESERVED23 : 8;
  unsigned int RESERVED24 : 8;
  unsigned int RESERVED25 : 8;
  unsigned int RESERVED26 : 8;
  unsigned int RESERVED27 : 8;
  unsigned int RESERVED28 : 8;
} NT71102_Header;

class NVT71102 : public TCONabstract {

 public:
  explicit NVT71102();
  ~NVT71102();
  int ToBin(CompMat *compMat, string &strSaveName) override;
  int PMicToBin(PMicCompMat *compMat, string &strSaveName) override;
  int GmToBin(GmCompMat *compMat, string &strSaveName) override;
 private:

  int ToBinNoDll(CompMat *compMat, string &strSaveName);
  void WriteBinFile(const DEMURA_TABLE_BUFFER tbls[], string &strSaveName);
  void SetHeader(NT71102_Header &header);
  void WriteHeader(ofstream &qs, NT71102_Header &header);
  void NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src, int tbl_num,
                             int tbl_h, int tbl_v);
  void NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                             DEMURA_SETTING setting, unsigned char tcon_idx,
                             bool shift_en = false, bool dummy_en = false);
  void NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                      DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                      unsigned char tcon_idx);
  void TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                    DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                    const int begin, const int end);
  void ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                  const CompMat &compMat);
  void CreatVD60HzFromat(const GmCompMat &compMat, char &burnbuffer);
  NT71102_Header Init_NT71102_header();
 private:
  CompMat *m_pCompMat;
  const int m_pLOWER_BOUND = 48;    //参考精测
  const int m_pUPPER_BOUND = 4080;  //参考精测
};
#endif // NVTTCON_H
