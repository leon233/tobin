/**
 * @projectName   toBin
 * @brief         base
 * @author        cxh
 * @date          2021-08-05 19:54
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#include "stdafx.h"
#include "tconfactor.h"

//#include "base/base.h"
#include "nvttcon.h"
#include "nvttcon71267.h"
#include "VDformat.h"
#include "csot_format.h"
#include "nvttcon71733B.h"
#include "nvttcon71265.h"
#include "csot_format_dual.h"
#include "EU3.h"
#include "OscarFormat.h"
#include "OscarFormat_Dual.h"
#include "tconbase.h"

TCONabstract::~TCONabstract() {}

TCONabstract* TCONfactor::Create(const int nIndex) {
    TCONabstract* pTconAbstract = nullptr;
    switch (nIndex) {
    case DemuraTCONtype::NT_71102: {
        pTconAbstract = new class NVT71102();
        break;
    }
    case DemuraTCONtype::NT_71267: {
        pTconAbstract = new class NVT71267();
        break;
    }
    case DemuraTCONtype::VD_8K_Format: {
        pTconAbstract = new class VD_FORMAT();
        break;
    }
    case DemuraTCONtype::CSOT_FORMAT: {
        pTconAbstract = new class CSOT_FORMAT();
        break;
    }
    case DemuraTCONtype::NT_71733B: {
        pTconAbstract = new class NVT71733B();
        break;
    }
    case DemuraTCONtype::NT_71265: {
        pTconAbstract = new class NVT71265();
        break;
    }
    case DemuraTCONtype::CSOT_FORMAT_Dual: {
        pTconAbstract = new class CSOT_FORMAT_DUAL();
        break;
    }
    case DemuraTCONtype::EU_3: {
        pTconAbstract = new class EU3();
        break;
    }
    case DemuraTCONtype::Oscar_Single: {
        pTconAbstract = new class Oscar_FORMAT();
        break;
    }
    case DemuraTCONtype::Oscar_Double: {
        pTconAbstract = new class Oscar_FORMAT_DUAL();
        break;
    }

    }

  return pTconAbstract;
}
