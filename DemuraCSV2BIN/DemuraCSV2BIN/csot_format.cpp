#include "stdafx.h"
#include "csot_format.h"
#include <WINDOWS.H>
#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include "tconbase.h"
#include "tconfactor.h"
#include "easylogging++.h"
using namespace std;

CSOT_FORMAT::CSOT_FORMAT() {}

CSOT_FORMAT::~CSOT_FORMAT() {}

int CSOT_FORMAT::ToBin(CompMat *compMat, string &strSaveName) {
  if (compMat == nullptr || strSaveName.empty()) {
      LOG(INFO) << "Demura compMat == nullptr or strSaveName empty";
      return 0;
  }
    return(ToBinNoDll(compMat, strSaveName));
}

int CSOT_FORMAT::PMicToBin(PMicCompMat* compMat, string& strSaveName) {
    if (compMat == nullptr || strSaveName.empty()) {
        LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
        return 0;
    }
    LOG(INFO) << "Start Save  CSOT format PMIC to:" << strSaveName.c_str();
    ifstream infile(compMat->PMIC_CodeDir, ios::out | ios::binary);
    if (infile.is_open())
    {
        char* buffer = (char*)malloc(sizeof(char*) * 100);
        unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
        if (buffer && checksum_out) {

            //ifstream infile;
            //infile.open(compMat->PMIC_CodeDir, ios::out | ios::binary);
            infile.read(buffer, 42);
            infile.close();
            int VCOM = compMat->DefaultVcom - 64 + compMat->burnData[0];
            buffer[37] = (VCOM >> 4) & 0xFF;
            buffer[38] = ((VCOM << 4) & 0xF0) + (buffer[38] & 0x0F);

            CSOTCRC16(40, (unsigned char*)buffer, checksum_out, 0);
            buffer[40] = (*checksum_out >> 8) & 0xFF;
            buffer[41] = (*checksum_out) & 0xFF;
            LOG(INFO) << "PMIC CRC is:" << *checksum_out;
            ofstream outfile;
            // outfile.open(strSaveName, ios::out | ios::binary);
            outfile.open(strSaveName, ios::_Nocreate | ios::binary);
            outfile.seekp(compMat->VcomAddress, ios::beg);
            outfile.write(buffer, 42);
            outfile.close();
            FreeMallocInt(checksum_out);
            if (buffer != NULL) {
                free(buffer);
                buffer = NULL;
                LOG(INFO) << "free PMIC burnbuffer!";
            }
            LOG(INFO) << "PMIC2bin finished";
            return 1;
        }
        else {
            LOG(INFO) << "PMIC code2bin fail,malloc fail!";
            return 0;
        }
    }
    else
    {
        LOG(INFO) << "PMIC code2bin fail,PMIC default code can't found!";
        return 0;
    }
    
}


int CSOT_FORMAT::GmToBin(GmCompMat *compMat, string &strSaveName) {

  LOG(INFO) << "Start Save Gamma to:" << strSaveName.c_str();
  if (compMat == nullptr) {
      LOG(INFO) << "Gamma code2bin fail,compMat == nullptr!";
      return 0;
  }
  if (strSaveName.empty()) {
      LOG(INFO) << "Gamma code2bin fail,strSaveName empty!";
      return 0;
  }
  unsigned int burnbuffer_Size = pow(2, compMat->GMInputBits) * 3 * 2 + 2;
  char *burnbuffer = (char *)malloc(
      sizeof(char *) *
      burnbuffer_Size);  // pow(2, compMat.GMInputBits) * 3 *compMat.GMOutputBits /8);
  if (burnbuffer && CreatCSOTFromat(*compMat, *burnbuffer) == burnbuffer_Size) {
      compMat->lut_size = burnbuffer_Size;

      ofstream outfile;
      // outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(compMat->Address, ios::beg);
      outfile.write(burnbuffer, compMat->lut_size);
      outfile.close();
      if (burnbuffer != NULL) {
          free(burnbuffer);
          burnbuffer = NULL;
          LOG(INFO) << "free Gamma burnbuffer!";
      }
      return 1;
  }
  else {
      LOG(INFO) << "Gamma code2bin fail,malloc/burnbuffer_Size err!";
      return 0;
  }
  
}

int CSOT_FORMAT::CreatCSOTFromat(const GmCompMat &compMat,
                                    char &burnbuffer) {
  unsigned char *ptr = (unsigned char*) &burnbuffer;
  unsigned int* crc_out = (unsigned int*)malloc(sizeof(unsigned int*));
  unsigned int data_len = 0;
  unsigned int j = 0;
  unsigned int GrayNum = pow(2, compMat.GMInputBits);
  for (int i = 0; i < GrayNum; i++) {
      ptr[j++] = (compMat.burnData_R[i] & 0xFF);
      ptr[j++] = char(compMat.burnData_R[i] >> 8 & 0x0F);
  }
  for (int i = 0; i < GrayNum; i++) {
      ptr[j++] = (compMat.burnData_G[i] & 0xFF);
      ptr[j++] = char(compMat.burnData_G[i] >> 8 & 0x0F);
  }
  for (int i = 0; i < GrayNum; i++) {
      ptr[j++] = (compMat.burnData_B[i] & 0xFF);
      ptr[j++] = char(compMat.burnData_B[i] >> 8 & 0x0F);
  }
  CSOTCRC16(j, ptr, crc_out, 0);
  ptr[j++] = (*crc_out >> 8 & 0xFF);
  ptr[j++] = (*crc_out & 0xFF);

  FreeMallocInt(crc_out);
  return data_len = j;
}
int CSOT_FORMAT::ToBinNoDll(CompMat *compMat, string &strSaveName) {
  m_pCompMat = compMat;

  // Step 1 – Create source buffer
  DEMURA_SOURCE_BUFFER src;
  NoDll_CreateSrcBuffer(&src, compMat->channels * compMat->compNum,
                        compMat->burnWidth, compMat->burnHeight);
  // Step 2 – Read demura compensation data
  ReadDemuraCompensationData(src, *compMat);

  // Step 3 – Demura setting
  DEMURA_SETTING setting ;
  setting.total_tcon_count = 0;
  setting.table.h = compMat->burnWidth;
  setting.table.v = compMat->burnHeight;
  setting.scale_x = 8;
  setting.scale_y = 8;
  setting.node_num = compMat->channels * compMat->compNum;
  setting.plane_num = compMat->compNum;
  // Step 4 – Create table buffer
  DEMURA_TABLE_BUFFER tbls[2];
  NoDll_CreateTblBuffer(&tbls[0], setting, setting.total_tcon_count);

  // Step 5 – Convert demura table
  NoDll_TableGen(src, tbls[0], setting, setting.total_tcon_count);

  //// Step 6 – Save demura table
  WriteBinFile(tbls, strSaveName);

  // Step 7 –Free source and table buffer
  if (tbls[0].buffer != NULL) {
	  free(tbls[0].buffer);
	  tbls[0].buffer = NULL;
      LOG(INFO) << "free demura burnbuffer!";
  }

	/*FreeMallocInt(tbls[0].checksum);
	FreeMallocInt(tbls[0].crc);*/
	Free3Darray(src.buffer);
    LOG(INFO) << "free demura databuffer!";
//  NvtDemura_FreeSrcBuffer(&src);
//  NvtDemura_FreeTblBuffer(&tbls[0]);
//  NvtDemura_FreeTblBuffer(&tbls[1]);
    return 1;
}
void CSOT_FORMAT::ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                          const CompMat &compMat) {
  int *ptr = &src.buffer[0][0][0];
  int nPlaneSize = compMat.burnHeight * compMat.burnWidth;
  //float R1, G1, B1, R2, G2, B2, R3, G3, B3, R4, G4, B4;  //补偿值
  float TEMP;
  for (int i = 0; i < nPlaneSize; i++) {
      for (int j = 0; j < compMat.compNum; j++) {
          if (compMat.channels == 1) {
              TEMP = compMat.burnData[nPlaneSize * 3 * j + i] - compMat.compGrayList[j];
              ptr[nPlaneSize * j + i] = (int)round(TEMP * 4);  // for 四舍五入
          }
          else if (compMat.channels == 3) {
              TEMP = compMat.burnData[nPlaneSize * 3*j + i] - compMat.compGrayList[j];
              ptr[nPlaneSize * 3 * j + i] = (int)round(TEMP * 4);  // for 四舍五入
              TEMP = compMat.burnData[nPlaneSize * (3 * j + 1) + i] - compMat.compGrayList[j];
              ptr[nPlaneSize * (3 * j + 1) + i] = (int)round(TEMP * 4);  // for 四舍五入
              TEMP = compMat.burnData[nPlaneSize * (3 * j + 2) + i] - compMat.compGrayList[j];
              ptr[nPlaneSize * (3 * j + 2) + i] = (int)round(TEMP * 4);  // for 四舍五入
          }
          
      }
     
  }


 // for (int i = 0; i < nPlaneSize; i++) {

 //   R1 = compMat.burnData[nPlaneSize * 0 + i] - compMat.compGrayList[0];
 //   G1 = compMat.burnData[nPlaneSize * 1 + i] - compMat.compGrayList[0];
 //   B1 = compMat.burnData[nPlaneSize * 2 + i] - compMat.compGrayList[0];
 //   R2 = compMat.burnData[nPlaneSize * 3 + i] - compMat.compGrayList[1];
 //   G2 = compMat.burnData[nPlaneSize * 4 + i] - compMat.compGrayList[1];
 //   B2 = compMat.burnData[nPlaneSize * 5 + i] - compMat.compGrayList[1];
 //   R3 = compMat.burnData[nPlaneSize * 6 + i] - compMat.compGrayList[2];
 //   G3 = compMat.burnData[nPlaneSize * 7 + i] - compMat.compGrayList[2];
 //   B3 = compMat.burnData[nPlaneSize * 8 + i] - compMat.compGrayList[2];
 //   R4 = compMat.burnData[nPlaneSize * 9 + i] - compMat.compGrayList[3];
 //   G4 = compMat.burnData[nPlaneSize * 10 + i] - compMat.compGrayList[3];
 //   B4 = compMat.burnData[nPlaneSize * 11 + i] - compMat.compGrayList[3];

	//ptr[nPlaneSize * 0 + i] = (int)round(R1 * 4);  // for 四舍五入
	//ptr[nPlaneSize * 1 + i] = (int)round(G1 * 4);
	//ptr[nPlaneSize * 2 + i] = (int)round(B1 * 4);

 //   ptr[nPlaneSize * 3 + i] = (int)round(R2 * 4);  // for 四舍五入
 //   ptr[nPlaneSize * 4 + i] = (int)round(G2 * 4);
 //   ptr[nPlaneSize * 5 + i] = (int)round(B2 * 4);

 //   ptr[nPlaneSize * 6 + i] = (int)round(R3 * 4);  // for 四舍五入
 //   ptr[nPlaneSize * 7 + i] = (int)round(G3 * 4);
 //   ptr[nPlaneSize * 8 + i] = (int)round(B3 * 4);

 //   ptr[nPlaneSize * 9 + i] = (int)round(R4 * 4);  // for 四舍五入
 //   ptr[nPlaneSize * 10 + i] = (int)round(G4 * 4);
 //   ptr[nPlaneSize * 11 + i] = (int)round(B4 * 4);

 // }
}
void CSOT_FORMAT::NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src,
                                     int tbl_num, int tbl_h, int tbl_v) {
  demura_src->buffer = Create3Darray(tbl_num, tbl_v, tbl_h);
  demura_src->tbl_h = tbl_h;
  demura_src->tbl_v = tbl_v;
  demura_src->tbl_num = tbl_num;
  demura_src->size = tbl_num * tbl_h * tbl_v;
}

void CSOT_FORMAT::NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                                     DEMURA_SETTING setting,
                                     unsigned char tcon_idx, bool shift_en,
                                     bool dummy_en) {
  /*Q_UNUSED(shift_en)
  Q_UNUSED(dummy_en)*/
  int Table_H;
  switch (tcon_idx) {
    case 0: {
      Table_H = setting.table.h;
      break;
    }
    default: {
      Table_H = setting.table.h / 2 + 1;
      break;
    }
  }

  // NT71102_2_TCON demura table description.pdf相关公式
  int Table_V = setting.table.v;
  int nNodeNumber = setting.node_num;
  int Block_H = setting.scale_x;
//  double tmp = (Table_H + (Block_H / 8)) * Table_V * nNodeNumber * 12 / 256;
//  int Table_SIZE = ceil(tmp + 0.5) * 32;  //向上取整
  int Table_SIZE= ((Table_H-1)*1.5+ Table_H/10+16)*Table_V*nNodeNumber;
  demura_tbl->buffer = nullptr;
  demura_tbl->buffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * Table_SIZE);
  demura_tbl->size = Table_SIZE;
  //demura_tbl->crc = (unsigned int *)malloc(sizeof(unsigned int *));
  //demura_tbl->checksum = (unsigned int *)malloc(sizeof(unsigned int *));
  /*if (demura_tbl->buffer != nullptr) {
	  free(demura_tbl->buffer);
	  demura_tbl->buffer = nullptr;
  }*/
}

void CSOT_FORMAT::NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                              DEMURA_TABLE_BUFFER &demura_tbl,
                              DEMURA_SETTING setting, unsigned char tcon_idx) {
  /*demura_tbl.buffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * demura_tbl.size);
  demura_tbl.crc = (unsigned int *)malloc(sizeof(unsigned int *));
  demura_tbl.checksum = (unsigned int *)malloc(sizeof(unsigned int *));*/

  switch (tcon_idx) {
    case 0: {
      int begin = 0;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 1: {
      int begin = 0;
      int end = setting.table.h / 2 + 2;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 2: {
      int begin = setting.table.h / 2 - 1;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
  }

//  NvtCRC(demura_tbl.buffer, demura_tbl.size, demura_tbl.crc,
//                           demura_tbl.checksum);
}

void CSOT_FORMAT::TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                            DEMURA_TABLE_BUFFER &demura_tbl,
                            DEMURA_SETTING setting, const int colBegin,
                            const int colEnd) {
  //将两个12bits，存储在三个byte中，高8bits+（合并：低4bits+高4bits）+低8bits
  int cnt = 0;
  bool bEven = true;
  int nRow = setting.table.v;
  int nCol = setting.table.h;
  int nNodeNum = setting.node_num;
  int nFull8bits, nMerge8bits;
  int *p = &demura_src.buffer[0][0][0];
  int cnt2 = 0;
  for (int k = 0; k < nRow; k++) {
      for(int j=0; j<nNodeNum; j++){
        for (int i = colBegin; i < colEnd; i++) {
            int tmp = p[k*nCol +nRow*nCol*j + i];
            if (bEven) {
                nFull8bits = tmp & 0xFF;
                nMerge8bits = (tmp >> 8) & 0x0F;
                bEven = false;
            } else {
            nFull8bits = (tmp >> 4) & 0xFF;
            nMerge8bits += (tmp << 4) & 0xF0;
            demura_tbl.buffer[cnt++] = nMerge8bits;  //存储合并的8bits
            bEven = true;
            }
            demura_tbl.buffer[cnt++] = nFull8bits;
            if((cnt+1)%16==0){
                demura_tbl.buffer[cnt++] = 0;
                bEven = true;
            }
        }
        demura_tbl.buffer[cnt++] = nMerge8bits;
        bEven = true;
		int d1 = 16 - round(((colEnd - colBegin) % 10)*1.5);
		for (int d = 0; d<16 - round(((colEnd - colBegin) % 10)*1.5); d++)
        demura_tbl.buffer[cnt++] = 0;    //填充14个dummy 数据
      }
  }
}

void CSOT_FORMAT::WriteBinFile(const DEMURA_TABLE_BUFFER tbls[],
                            string &strSaveName) {

    LOG(INFO) << "Save CSOT format Demura to: " << strSaveName.c_str();

  
  // write header
//CSOTformat_Header header;
//  CSOTformat_Header header = InitHeader<CSOTformat_Header>(TCONtype::CSOT_UD_FORMAT);
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  if (checksum_out) {
      ofstream outfile;
      //outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(m_pCompMat->DemuraAddress, ios::beg);

      CSOTformat_Header header = Init_CSOTformat_Header();
      CSOTCRC16(tbls[0].size, tbls[0].buffer, checksum_out, 0);
      m_pCompMat->lut_checksum = *checksum_out;
      LOG(INFO) << "Demura CRC/checksunm is: " << *checksum_out;

      m_pCompMat->lut_size = tbls[0].size;
      SetHeader(header);

      unsigned char* ptr = (unsigned char*)&header + 0x20;
      unsigned int size = sizeof(header) - 0x20;
      CSOTCRC16(size, ptr, checksum_out, 0);
      CSOTCRC16(tbls[0].size, tbls[0].buffer, checksum_out, *checksum_out);

      header.FileTotalCRC = *checksum_out;
      LOG(INFO) << "FileTotal CRC is: " << *checksum_out;

      ptr = (unsigned char*)&header;
      CSOTCRC16(0x1E, ptr, checksum_out, 0);
      header.FileHeaderCRC = *checksum_out;

      /*int length = sizeof(header);
      char *p = new char[length];
      memmove(p, &header, length);

      for (int idx = 0; idx < length; idx++) {
          outfile << char(p[idx]);
      }*/

      WriteHeader(outfile, header);
      LOG(INFO) << "FileHeader CRC is: " << *checksum_out;

      // Write table, 2 TCONs
      for (int nTCONindex = 0; nTCONindex < 1; nTCONindex++) {
          DEMURA_TABLE_BUFFER tbl = tbls[nTCONindex];
          for (int idx = 0; idx < tbl.size; idx++) {
              outfile << tbl.buffer[idx];
          }
      }

      outfile.close();
      LOG(INFO) << "Demura CSV2bin finished!";
      FreeMallocInt(checksum_out);
  }
}

void CSOT_FORMAT::SetHeader(CSOTformat_Header &header) {
  if (m_pCompMat == nullptr) {
    return;
  }
  if(m_pCompMat->channels==3)
      header.enDemuraMode = 1; //1:color
  else
      header.enDemuraMode = 0; //0:mono

  header.totalSize = (m_pCompMat->lut_size)+0x110;
  header.PlaneNUm = m_pCompMat->compNum;
  header.HblockSize= m_pCompMat->width/ (m_pCompMat->burnWidth-1); //8
  header.VblockSize= m_pCompMat->height / (m_pCompMat->burnHeight - 1); //8
  header.HLutNum = (m_pCompMat->burnWidth);
  header.VLutNum = (m_pCompMat->burnHeight);

  header.LUTsize= (m_pCompMat->lut_size);
  header.LUTSectionCRC=(m_pCompMat->lut_checksum);

  header.BlackLimitR = (m_pCompMat->lower_Bound);
  header.BlackLimitG = (m_pCompMat->lower_Bound);
  header.BlackLimitB = (m_pCompMat->lower_Bound);

  header.WhiteLimitR = (m_pCompMat->high_Bound);
  header.WhiteLimitG = (m_pCompMat->high_Bound);
  header.WhiteLimitB = (m_pCompMat->high_Bound);

  header.Plane1LevelR = (m_pCompMat->compGrayList[0]);
  header.Plane1LevelG = (m_pCompMat->compGrayList[0]);
  header.Plane1LevelB = (m_pCompMat->compGrayList[0]);

  header.Plane2LevelR = (m_pCompMat->compGrayList[1]);
  header.Plane2LevelG = (m_pCompMat->compGrayList[1]);
  header.Plane2LevelB = (m_pCompMat->compGrayList[1]);

  header.Plane3LevelR = (m_pCompMat->compGrayList[2]);
  header.Plane3LevelG = (m_pCompMat->compGrayList[2]);
  header.Plane3LevelB = (m_pCompMat->compGrayList[2]);

  header.Plane4LevelR = (m_pCompMat->compGrayList[3]);
  header.Plane4LevelG = (m_pCompMat->compGrayList[3]);
  header.Plane4LevelB = (m_pCompMat->compGrayList[3]);

  header.Plane5LevelR = (m_pCompMat->compGrayList[4]);
  header.Plane5LevelG = (m_pCompMat->compGrayList[4]);
  header.Plane5LevelB = (m_pCompMat->compGrayList[4]);
//  header.Plane6LevelR = _byteswap_ushort(m_pCompMat->compGrayList[5] * 4);
//  header.Plane6LevelG = _byteswap_ushort(m_pCompMat->compGrayList[5] * 4);
//  header.Plane6LevelB = _byteswap_ushort(m_pCompMat->compGrayList[5] * 4);
//  header.Plane7LevelR = _byteswap_ushort(m_pCompMat->compGrayList[6] * 4);
//  header.Plane7LevelG = _byteswap_ushort(m_pCompMat->compGrayList[6] * 4);
//  header.Plane7LevelB = _byteswap_ushort(m_pCompMat->compGrayList[6] * 4);

  header.PlaneB1SlopeR = (65535/(m_pCompMat->compGrayList[0]- m_pCompMat->lower_Bound));
  header.PlaneB1SlopeG = header.PlaneB1SlopeR;
  header.PlaneB1SlopeB = header.PlaneB1SlopeR;
  unsigned int COEF[8]={0,0,0,0,0,0,0,0};
  for(int i=0;i<8;i++){
      if(m_pCompMat->compGrayList[i+1]!=0)
          COEF[i]=(65535/(m_pCompMat->compGrayList[i+1]- m_pCompMat->compGrayList[i]));
      else
       {   COEF[i]=(65535/(m_pCompMat->high_Bound - m_pCompMat->compGrayList[i]));
          break;}
  }
  header.Plane12SlopeR = COEF[0];
  header.Plane12SlopeG = COEF[0];
  header.Plane12SlopeB = COEF[0];

  header.Plane23SlopeR = COEF[1];
  header.Plane23SlopeG = COEF[1];
  header.Plane23SlopeB = COEF[1];

  header.Plane34SlopeR = COEF[2];
  header.Plane34SlopeG = COEF[2];
  header.Plane34SlopeB = COEF[2];

  header.Plane45SlopeR = COEF[3];
  header.Plane45SlopeG = COEF[3];
  header.Plane45SlopeB = COEF[3];

  header.Plane56SlopeR = COEF[4];
  header.Plane56SlopeG = COEF[4];
  header.Plane56SlopeB = COEF[4];

  header.Plane67SlopeR = COEF[5];
  header.Plane67SlopeG = COEF[5];
  header.Plane67SlopeB = COEF[5];

  header.Plane78SlopeR = COEF[6];
  header.Plane78SlopeG = COEF[6];
  header.Plane78SlopeB = COEF[6];

  header.Plane89SlopeR = COEF[7];
  header.Plane89SlopeG = COEF[7];
  header.Plane89SlopeB = COEF[7];

  header.Plane9WSlopeR = 0;
  header.Plane9WSlopeG = 0;
  header.Plane9WSlopeB = 0;

  unsigned char *ptr = (unsigned char *)&header + 64;
  unsigned int size = sizeof(header) - 64;
  unsigned int *crc_out = (unsigned int *)malloc(sizeof(unsigned int *));
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  //NvtDemura_CalCrcChecksum(ptr, size, crc_out, checksum_out);
  CSOTCRC16(size, ptr, crc_out,0);
  header.ParaSectionCRC = (*crc_out);  //需要修改计算公式

  /*Log4Info(QString("NVTTCON  Header, CRC = 0x%1, Checksum = 0x%2")
               .arg(QString::number(*crc_out, 16))
               .arg(QString::number(*checksum_out, 16)));*/
  FreeMallocInt(crc_out);
  FreeMallocInt(checksum_out);
}

void CSOT_FORMAT::WriteHeader(ofstream &qs, CSOTformat_Header &header) {
  //CByteArray byte = StructToByte(header);
  //qs.writeRawData(byte, sizeof(CSOTformat_Header));

  int length = sizeof(header);
  BYTE *p = new BYTE[length+1];
  memmove(p, &header, length);
  for (int idx = 0; idx < length; idx++) {
	  qs << p[idx];
  }

}


CSOTformat_Header CSOT_FORMAT::Init_CSOTformat_Header() {
  CSOTformat_Header header;
  header.header = _byteswap_ulong(0x43534F54);  //CSOT的ACSII碼
  //header.name="demuratestonly";
  char* sname="demuratestonly";

  header.name1 = *sname++;
  header.name2 = *sname++;
  header.name3 = *sname++;
  header.name4 = *sname++;
  header.name5 = *sname++;
  header.name6 = *sname++;
  header.name7 = *sname++;
  header.name8 = *sname++;
  header.name9 = *sname++;
  header.name10 = *sname++;
  header.name11 = *sname++;
  header.name12 = *sname++;
  header.name13 = *sname++;
  header.name14 = *sname++;

  header.version=0x0100;
  header.sectionNum =0x0002;
  header.RESERVED00 =0;
  header.totalSize = 0x0009BAE0;
  header.FileTotalCRC =0;//待計算
  header.FileHeaderCRC =0;//待計算

  header.sectionTypeA =0x00000001;
  header.Paraoffset =0x00000040;
  header.Parasize =0x000000D0;//待計算
  header.ParaSectionCRC =0;//待計算
  header.RESERVED01 =0;

  header.sectionTypeB =0x00000002;
  header.LUToffset =0x00000110;
  header.LUTsize =0x0009B9D0;
  header.LUTSectionCRC =0;//待計算
  header.RESERVED02 =0;

  header.enDemuraMode =0; //0:mono   1:color
  header.PlaneNUm =3;
  header.HblockSize =8;
  header.VblockSize =8;
  header.HLutNum =481;
  header.VLutNum =271;
  header.IntBitWidth =10;//
  header.DecimalsBitWidth =0x02;
  header.TwoChipEN =0;//0:單TCON  1：雙TCON
  header.CompressEN =0;  //0：無壓縮  1：COST壓縮  2：供應商壓縮
  header.ComValGainR =0x0010;
  header.ComValGainG =0x0010;
  header.ComValGainB =0x0010;
  header.RESERVED03 =0;
  header.ComValOffsetR =0;
  header.ComValOffsetG =0;
  header.ComValOffsetB =0;
  header.RESERVED04 =0;
  header.BlackLimitR =12;
  header.BlackLimitG =12;
  header.BlackLimitB =12;
  header.RESERVED05 =0;
  header.WhiteLimitR = 1020;
  header.WhiteLimitG = 1020;
  header.WhiteLimitB = 1020;
  header.RESERVED06 =0;
  header.Plane1LevelR =100;
  header.Plane2LevelR =240;
  header.Plane3LevelR =900;
  header.Plane4LevelR =0;
  header.Plane5LevelR =0;
  header.Plane6LevelR =0;
  header.Plane7LevelR =0;
  header.Plane8LevelR =0;
  header.Plane9LevelR =0;
  header.Plane1LevelG =100;
  header.Plane2LevelG =240;
  header.Plane3LevelG =900;
  header.Plane4LevelG =0;
  header.Plane5LevelG =0;
  header.Plane6LevelG =0;
  header.Plane7LevelG =0;
  header.Plane8LevelG =0;
  header.Plane9LevelG =0;
  header.Plane1LevelB =100;
  header.Plane2LevelB =240;
  header.Plane3LevelB =900;
  header.Plane4LevelB =0;
  header.Plane5LevelB =0;
  header.Plane6LevelB =0;
  header.Plane7LevelB =0;
  header.Plane8LevelB =0;
  header.Plane9LevelB =0;
  header.RESERVED07 =0;
  header.RESERVED08 =0;
  header.RESERVED09 =0;
  header.RESERVED10 =0;
  header.RESERVED11 =0;
  header.RESERVED12 =0;
  header.RESERVED13 =0;
  header.RESERVED14 =0;
  header.RESERVED15 =0;
  header.PlaneB1SlopeR =0x028F;
  header.Plane12SlopeR =0x01D4;
  header.Plane23SlopeR =0x0063;
  header.Plane34SlopeR =0x0214;
  header.Plane45SlopeR =0;
  header.Plane56SlopeR =0;
  header.Plane67SlopeR =0;
  header.Plane78SlopeR =0;
  header.Plane89SlopeR =0;
  header.Plane9WSlopeR =0;

  header.PlaneB1SlopeG =0x028F;
  header.Plane12SlopeG =0x01D4;
  header.Plane23SlopeG =0x0063;
  header.Plane34SlopeG =0x0214;
  header.Plane45SlopeG =0;
  header.Plane56SlopeG =0;
  header.Plane67SlopeG =0;
  header.Plane78SlopeG =0;
  header.Plane89SlopeG =0;
  header.Plane9WSlopeG =0;

  header.PlaneB1SlopeB =0x028F;
  header.Plane12SlopeB =0x01D4;
  header.Plane23SlopeB =0x0063;
  header.Plane34SlopeB =0x0214;
  header.Plane45SlopeB =0;
  header.Plane56SlopeB =0;
  header.Plane67SlopeB =0;
  header.Plane78SlopeB =0;
  header.Plane89SlopeB =0;
  header.Plane9WSlopeB =0;

  header.RESERVED16 =0;
  header.RESERVED17 =0;
  header.RESERVED18 =0;
  header.RESERVED19 =0;
  header.RESERVED20 =0;
  header.RESERVED21 =0;
  header.RESERVED22 =0;
  header.RESERVED23 =0;
  header.RESERVED24 =0;
  header.RESERVED25 =0;
  header.RESERVED26 =0;
  header.RESERVED27 =0;
  header.RESERVED28 =0;
  header.RESERVED29 =0;
  header.RESERVED30 =0;
  header.RESERVED31 =0;

  return header;
}

