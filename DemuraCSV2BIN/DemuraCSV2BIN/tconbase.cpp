/**
 * @projectName   CSOT_EEAD_ALG2_DEMURA
 * @brief         Demura
 * @author        wxq
 * @date          2021-04-07 19:54
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#include "stdafx.h"
#include "tconbase.h"
#include<malloc.h>
#include <cmath>

void VD_Checksum(unsigned int size, const unsigned char *buf,
            unsigned int *checksum_out) {
	unsigned int checksum = 0;
	unsigned int i = 0;
	unsigned int length = size;

	for(i=0;i<length;){
	checksum+=(buf[i]<<8)+buf[i+1];
	i=i+2;

	}
  *checksum_out = (checksum & 0xFFFF);
}

void VD_Oscar_Checksum(unsigned int size, const unsigned char* buf,
    unsigned int* checksum_out) {

    unsigned int checksum = 0;
    unsigned int i = 0;
    unsigned int length = size;

    for (i = 0; i < length;) {
        checksum += (buf[i+1] << 8) + buf[i];
        i = i + 2;
    }
    *checksum_out = (checksum & 0xFFFF);
}
void SDC_Checksum(unsigned int size, const unsigned char* buf,
    unsigned int* checksum_out) {

    unsigned int checksum = 0;
    unsigned int i = 0;
    unsigned int length = size;

    for (i = 0; i < length;i++) {
        checksum += buf[i];
    }
    *checksum_out = (checksum & 0x00FF);
}
void NvtCRC8(unsigned int size, const unsigned char *buf, unsigned int *crc_out,
	unsigned int *checksum_out) {

	unsigned char out_crc[2];
	unsigned char tmp_dat;
	unsigned char tmp_crc;
	unsigned char ini_crc[2] = {0xF2,0xF0};  //initial value
	unsigned int length = size/2;

	for (int idx = 0; idx < 2; idx++){
		out_crc[idx] = ini_crc[idx % 2];
	}
	for (unsigned int grp_idx = 0; grp_idx < length; grp_idx++){
		for (int idx = 0; idx < 2; idx++){
			tmp_dat = buf[grp_idx * 2 + idx];
			tmp_crc = out_crc[idx];
			out_crc[idx] = 0;
			out_crc[idx] += (tmp_dat >> 7) & 0x01 ^ (tmp_dat >> 6) & 0x01 ^ (tmp_dat) & 0x01 ^                             (tmp_crc) & 0x01 ^ (tmp_crc >> 6) & 0x01 ^ (tmp_crc >> 7) & 0x01;
			out_crc[idx] += ((tmp_dat >> 6) & 0x01 ^ (tmp_dat >> 1) & 0x01 ^ (tmp_dat) & 0x01 ^                             (tmp_crc) & 0x01 ^ (tmp_crc >> 1) & 0x01 ^ (tmp_crc >> 6) & 0x01)<<1;
			out_crc[idx] += ((tmp_dat >> 6) & 0x01 ^ (tmp_dat >> 2) & 0x01 ^ (tmp_dat >> 1) & 0x01 ^ (tmp_dat) & 0x01 ^     (tmp_crc) & 0x01 ^ (tmp_crc>>1) & 0x01 ^ (tmp_crc >> 2) & 0x01 ^ (tmp_crc >> 6) & 0x01)<<2;
			out_crc[idx] += ((tmp_dat >> 7) & 0x01 ^ (tmp_dat >> 3) & 0x01 ^ (tmp_dat >> 2) & 0x01 ^ (tmp_dat >>1) & 0x01 ^ (tmp_crc >> 1) & 0x01 ^ (tmp_crc >> 2) & 0x01 ^ (tmp_crc >> 3) & 0x01 ^ (tmp_crc >> 7) & 0x01)<<3;
			
			out_crc[idx] += ((tmp_dat >> 4) & 0x01 ^ (tmp_dat >> 3) & 0x01 ^ (tmp_dat >> 2) & 0x01 ^                         (tmp_crc >> 2) & 0x01 ^ (tmp_crc >> 3) & 0x01 ^ (tmp_crc >> 4) & 0x01)<<4;
			out_crc[idx] += ((tmp_dat >> 5) & 0x01 ^ (tmp_dat >> 4) & 0x01 ^ (tmp_dat >> 3) & 0x01 ^                         (tmp_crc >> 3) & 0x01 ^ (tmp_crc >> 4) & 0x01 ^ (tmp_crc >> 5) & 0x01)<<5;
			out_crc[idx] += ((tmp_dat >> 6) & 0x01 ^ (tmp_dat >> 5) & 0x01 ^ (tmp_dat >> 4) & 0x01 ^                         (tmp_crc >> 4) & 0x01 ^ (tmp_crc >> 5) & 0x01 ^ (tmp_crc >> 6) & 0x01)<<6;
			out_crc[idx] += ((tmp_dat >> 7) & 0x01 ^ (tmp_dat >> 6) & 0x01 ^ (tmp_dat >> 5) & 0x01 ^                         (tmp_crc >> 5) & 0x01 ^ (tmp_crc >> 6) & 0x01 ^ (tmp_crc >> 7) & 0x01)<<7;
		}
	}
	unsigned short temp = 0;
	temp = out_crc[0];
	temp = (temp<<8) + out_crc[1];
	*crc_out = temp & 0xFFFF;
}

void NvtCRC(unsigned int size, const unsigned char *buf, unsigned int *crc_out,
            unsigned int *checksum_out) {

  unsigned int i=0, j=0;
  unsigned short crc = 0x0000;
  unsigned short crc_temp_1=0, crc_temp_2=0, crc_temp_3=0;
  unsigned char data_buffer=0;
  unsigned int length = size;
  unsigned int checksum = 0;

  while (length--) {
    data_buffer = buf[i++];
    checksum += data_buffer;
    crc_temp_1 = 0;
    crc_temp_2 = 0;
    crc_temp_3 = 0;

    for (j = 0; j < 8; j++) {
      crc_temp_1 = ((crc >> 15) ^ data_buffer) & 0x0001;
      crc_temp_2 = ((crc >> 1) ^ crc_temp_1) & 0x0001;
      crc_temp_3 = ((crc >> 14) ^ crc_temp_1) & 0x0001;
      crc = crc << 1;
      crc &= 0x7FFA;
      crc |= crc_temp_1;
      crc |= (crc_temp_2 << 2);
      crc |= (crc_temp_3 << 15);
      data_buffer >>= 1;
    }
  }
  *crc_out = crc & 0xFFFF;
  *checksum_out = (checksum & 0xFFFF);
}
const unsigned short CRC16_TABLE[256] = {
0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011, 0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072, 0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2, 0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1, 0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192, 0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1, 0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151, 0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132, 0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312, 0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371, 0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1, 0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2, 0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291, 0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2, 0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252, 0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231, 0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
};


//计算*pData指针指向数组的CRC
void CSOTCRC16(unsigned int size, const unsigned char *buf,
unsigned int *checksum_out,unsigned int preCRC){

    unsigned char dat=0;
    unsigned short crcData = preCRC;

    while(size--){
        dat = (crcData>>8) & 0xff; // 以8 位二进制数暂存CRC 的高8 位
        crcData <<= 8; // 左移8 位
        crcData ^= CRC16_TABLE[dat^*buf++]; // 高字节和当前数据XOR 再查表
    }
    *checksum_out= crcData;
}

int ***Create3Darray(int nTableNum, int nRow, int nCol) {

  int i = 0;
  int k = 0;
  int ***result = NULL;

  if ((nTableNum > 0) && (nRow > 0) && (nCol > 0)) {
    int **pp = NULL;
    int *p = NULL;
    result = (int ***)malloc(nTableNum * sizeof(int **));      // key
    pp = (int **)malloc(nTableNum * nRow * sizeof(int *));     // key
    p = (int *)malloc(nTableNum * nRow * nCol * sizeof(int));  // key
    if ((result != NULL) && (pp != NULL) && (p != NULL)) {
      for (i = 0; i < nTableNum; i++) {
        result[i] = pp + i * nRow;  // 三维元素存二维地址
        for (k = 0; k < nRow; k++) {
          result[i][k] = p + k * nCol;  // // 二维元素存一维地址
        }
        p = p + nRow * nCol;
      }
    } else {
      free(result);
      free(pp);
      free(p);
      result = NULL;
      pp = NULL;
      p = NULL;
    }
  }

  return result;
}

void Free3Darray(int ***p) {
  if (*p != NULL) {
    if (**p != NULL) {
      free(**p);
      **p = NULL;
    }
    free(*p);
    *p = NULL;
  }
  free(p);
  p = NULL;
}

void FreeMallocInt(unsigned int *p) {
	if (p != NULL) {
		free(p);
		p = NULL;
	}
}

CompMat* CreateCompMat(int compNum, int* compGrayList,
                       int channels, int width, int height,
                       int burnWidth, int burnHeight,int lowBound,int highBound)
{
    //CompMat CM;
    CompMat *pCM=new CompMat();
    pCM->compNum=compNum;

    for (int i = 0; i < compNum; i++) {
      pCM->compGrayList[i] = *compGrayList++;
	  pCM->compGrayList[i] *= 4;
    }
    pCM->channels=channels;
    pCM->width=width;
    pCM->height=height;
    pCM->burnWidth=burnWidth;
    pCM->burnHeight=burnHeight;
    pCM->burnData = (float*)malloc(sizeof(float)*burnWidth*burnHeight*compNum*3);
	pCM->lower_Bound = lowBound;
	pCM->high_Bound = highBound;
    
    return pCM;
}
PMicCompMat* CreatePMicCompMat(int VcomAddress, int DefaultVcom) {

  PMicCompMat *pCM = new PMicCompMat();
  pCM->burnData = (int *)malloc(sizeof(int) * 49);
  pCM->DefaultVcom = DefaultVcom;
  pCM->VcomAddress = VcomAddress;

  return pCM;
}

GmCompMat *CreateGmCompMat(int Address,
                           int GMInputBits, int GMOutputBits){

  GmCompMat *pCM = new GmCompMat();

  pCM->GMInputBits = GMInputBits;
  pCM->GMOutputBits = GMOutputBits;
  pCM->Address = Address;
  //pCM->burnData =
  //    (int *)malloc(sizeof(int) * pow(2, GMInputBits) * 3 * GMOutputBits / 8);

  return pCM;
}

CompMat *CreateAllCompMat(int compNum, int *compGrayList, int channels, int height, int width,
                        int burnWidth, int burnHeight, int lowBound, int highBound,
                       int DemuraAddress) {

  CompMat *pCM = new CompMat();
  pCM->compNum = compNum;

  for (int i = 0; i < compNum; i++) {
    pCM->compGrayList[i] = *compGrayList++;
    pCM->compGrayList[i] *= 4;
  }
  pCM->channels = channels;
  pCM->width = width;
  pCM->height = height;
  pCM->burnWidth = burnWidth;
  pCM->burnHeight = burnHeight;
  pCM->burnData =
      (float *)malloc(sizeof(float) * burnWidth * burnHeight * compNum * 3);
  pCM->lower_Bound = lowBound;
  pCM->high_Bound = highBound;
  pCM->DemuraAddress = DemuraAddress;

  return pCM;
}