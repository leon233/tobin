/**
 * @projectName   CSOT_EEAD_ALG2_DEMURA
 * @brief         Demura
 * @author        cxh
 * @date          2022-02-09 14:46
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#include "stdafx.h"
#include "OscarFormat.h"
#include <WINDOWS.H>
#include <iostream>
#include <fstream>
#include <cmath>
#include "tconbase.h"
#include "tconfactor.h"
#include "easylogging++.h"
using namespace std;

Oscar_FORMAT::Oscar_FORMAT() {}
Oscar_FORMAT::~Oscar_FORMAT() {}

int Oscar_FORMAT::ToBin(CompMat *compMat, string &strSaveName) {

  if (compMat == nullptr || strSaveName.empty()) {
    LOG(INFO) << "Demura compMat == nullptr or strSaveName empty";
    return 0;
  }
    return(ToBinNoDll(compMat, strSaveName)); 
    
}

int Oscar_FORMAT::CellInforToBin(PMicCompMat* compMat, string& strSaveName) {
    if (compMat == nullptr || strSaveName.empty()) {
        // Log4Error("Tobin error");
        LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
        return 0;
    }
    LOG(INFO) << "Start Save  VD format Cellinfor to:" << strSaveName.c_str();
    
    unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
    char* CellInfobuffer = (char*)malloc(sizeof(char*) * 96);
    if (CellInfobuffer && checksum_out) {
        *checksum_out = 0;
        for (int i = 0; i < 64;) {
            *checksum_out += (compMat->CellInfo[i + 1] << 8) + compMat->CellInfo[i];
            i = i + 2;
        }
        compMat->CellInfo[64] = *checksum_out & 0xFF;
        compMat->CellInfo[65] = (*checksum_out >> 8) & 0xFF;

        for (int i = 0; i < 24; i++) {
            CellInfobuffer[4 * i] = compMat->CellInfo[4 * i + 3];
            CellInfobuffer[4 * i + 1] = compMat->CellInfo[4 * i + 2];
            CellInfobuffer[4 * i + 2] = compMat->CellInfo[4 * i + 1];
            CellInfobuffer[4 * i + 3] = compMat->CellInfo[4 * i];
        }
        ofstream outfile1;
        // outfile.open(strSaveName, ios::out | ios::binary);
        outfile1.open(strSaveName, ios::_Nocreate | ios::binary);
        outfile1.seekp(compMat->VcomAddress - 0x1000, ios::beg);
        outfile1.write(CellInfobuffer, 96);
        outfile1.close();
        LOG(INFO) << "CellInfo code2bin finished!";
        if (CellInfobuffer != NULL) {
            free(CellInfobuffer);
            CellInfobuffer = NULL;
            LOG(INFO) << "free CellInfobuffer!";
        }
        FreeMallocInt(checksum_out);
        return 1;
    }
    else {
        LOG(INFO) << "CellInfo to bin fail,malloc fail!";
        return 0;
    }

}

int Oscar_FORMAT::PMicToBin(PMicCompMat* compMat, string& strSaveName) {

  if (compMat == nullptr || strSaveName.empty()) {

    LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
    return 0;
  }
  if (CellInforToBin(compMat, strSaveName) == 0)
      return 0;

  LOG(INFO) << "Start Save  VD format VCOM to:" << strSaveName.c_str();

  compMat->burnsize = 8;
  unsigned char *burnbuffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * compMat->burnsize);
  unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
  if (burnbuffer && checksum_out) 
  {
      burnbuffer[0] = 0x00;
      burnbuffer[1] = 0x40;
      burnbuffer[2] = 0x00;
      burnbuffer[3] = 0x05;
      burnbuffer[4] = 0x00;
      int vcom = (compMat->burnData[0] << 1) + 1;
      burnbuffer[5] = vcom & 0xFF;
      burnbuffer[6] = 0x00;
      burnbuffer[7] = 0x00;

      VD_Checksum(compMat->burnsize - 2, burnbuffer, checksum_out);
      burnbuffer[7] = *checksum_out;

      ofstream outfile;
      // outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(compMat->VcomAddress, ios::beg);
      for (int idx = 0; idx < compMat->burnsize;) {
          outfile << burnbuffer[idx + 2];
          outfile << burnbuffer[idx + 3];
          outfile << burnbuffer[idx + 0];
          outfile << burnbuffer[idx + 1];
          idx += 4;
      }
      outfile.close();
      LOG(INFO) << "VCOM code2bin finished!";
      if (burnbuffer != NULL) {
          free(burnbuffer);
          burnbuffer = NULL;
          LOG(INFO) << "free VCOM burnbuffer!";
      }
      FreeMallocInt(checksum_out);
      return 1;
  }
  else{
      LOG(INFO) << "VCOM code2bin fail,malloc fail!";
      return 0;
  }

}

int Oscar_FORMAT::GmToBin(GmCompMat* compMat, string& strSaveName) {

    LOG(INFO) << "Start Save VD format Gamma to:" << strSaveName.c_str();
    if (compMat == nullptr) {
        LOG(INFO) << "Gamma code2bin fail,compMat == nullptr!";
        return 0;
    }
    if (strSaveName.empty()) {
        LOG(INFO) << "Gamma code2bin fail,strSaveName empty!";
        return 0;
    }

    char* burnbuffer = (char*)malloc(sizeof(char*) * 6148);  // pow(2, compMat.GMInputBits) * 3 *compMat.GMOutputBits /8);
    if (burnbuffer) {
        switch (compMat->GmType) {
        case 1:
            compMat->lut_size = CreatVD60HzFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatVD60HzFromat!     lut_size:"<< compMat->lut_size;
            break;
        case 2:
            compMat->lut_size = CreatVD120HzFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatVD120HzFromat!    lut_size:" << compMat->lut_size;
            break;
        default:
            compMat->lut_size = 0;
            LOG(INFO) << "Creat gamma Fromat err!   lut_size:" << compMat->lut_size;
            return 0;
            break;
        }

        ofstream outfile;
        // outfile.open(strSaveName, ios::out | ios::binary);
        outfile.open(strSaveName, ios::_Nocreate | ios::binary);
        outfile.seekp(compMat->Address, ios::beg);
        outfile.write(burnbuffer, compMat->lut_size);
        outfile.close();

        LOG(INFO) << "Gamma code2bin finished!";
        if (burnbuffer != NULL) {
            free(burnbuffer);
            burnbuffer = NULL;
            LOG(INFO) << "free Gamma burnbuffer!";
        }


        return 1;
    }
    else {
        LOG(INFO) << "Gamma code2bin fail,malloc fail!";
        return 0;
    }

    
}

int Oscar_FORMAT::CreatVD60HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out=0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    i += 2;
  }
  int bank_size = 0x1800 - 0x200 * 3;
  for (int i = 0; i < bank_size + 2; i++) {
    ptr[j++] = 0x00;
  }
  
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  //FreeMallocInt(checksum_out);
}
int Oscar_FORMAT::CreatVD120HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out = 0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
      i += 2;
  }
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
    i += 2;
  }
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
    i += 2;
  }
  checksum_out += ptr[j++] = 0x00;
  checksum_out += ptr[j++] = 0x00;
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  // FreeMallocInt(checksum_out);
}


int Oscar_FORMAT::ToBinNoDll(CompMat *compMat, string &strSaveName) {
  m_pCompMat = compMat;

  // Step 0 – Demura setting
  DEMURA_SETTING setting;
  setting.total_tcon_count = 0;
  setting.table.h = compMat->burnWidth;
  setting.table.v = compMat->burnHeight;
  setting.scale_x = 8;
  setting.scale_y = 8;
  setting.node_num = compMat->channels * compMat->compNum;
  setting.plane_num = compMat->compNum;

  // Step 1 – Create source buffer
  DEMURA_SOURCE_BUFFER src;
  NoDll_CreateSrcBuffer(&src, compMat->channels * compMat->compNum,
                        compMat->burnWidth, compMat->burnHeight);
  // Step 2 – Read demura compensation data
  ReadDemuraCompensationData(src, *compMat);


  // Step 3 – Create table buffer
  DEMURA_TABLE_BUFFER tbls[2];
  NoDll_CreateTblBuffer(&tbls[0], setting, setting.total_tcon_count);

  // Step 4 – Convert demura table
  NoDll_TableGen(src, tbls[0], setting, setting.total_tcon_count);

  // Step 5 – Save demura table
  WriteBinFile(tbls, strSaveName);

  // Step 6 –Free source and table buffer

  if (tbls[0].buffer != NULL) {
	  free(tbls[0].buffer);
	  tbls[0].buffer = NULL;
      LOG(INFO) << "free demura burnbuffer!";
  }

  /*FreeMallocInt(tbls[0].checksum);
  FreeMallocInt(tbls[0].crc);*/
  Free3Darray(src.buffer);
  LOG(INFO) << "free demura databuffer!";
  return 1;
}

void Oscar_FORMAT::ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                          const CompMat &compMat) {
  int *ptr = &src.buffer[0][0][0];
  int nPlaneSize = compMat.burnHeight * compMat.burnWidth;
  //float R1, G1, B1, R2, G2, B2, R3, G3, B3;  //补偿值
  float RTemp[21];
  int TEMP;
  int GrayList[21] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  unsigned mag[21]= { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  float plane_max[21] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  float plane_min[21] = { 1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020,1020 };
  float plane_offset[21] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
  float plane1_max=0,plane1_min=1020,plane1_offset=0;
  float plane2_max=0,plane2_min=1020,plane2_offset=0;
  float plane3_max=0,plane3_min=1020,plane3_offset=0;

  for (int i = 0; i < compMat.channels; i++) {
      for (int j = 0; j < compMat.compNum; j++) {
          
      }
  }

  GrayList[0] = compMat.compGrayList[0];
  GrayList[1] = compMat.compGrayList[1];
  GrayList[2] = compMat.compGrayList[2];
  GrayList[3] = compMat.compGrayList[0];
  GrayList[4] = compMat.compGrayList[1];
  GrayList[5] = compMat.compGrayList[2];
  GrayList[6] = compMat.compGrayList[0];
  GrayList[7] = compMat.compGrayList[1];
  GrayList[8] = compMat.compGrayList[2];

  for (int i = 0; i < compMat.burnHeight; i++)
  for (int j = 0; j < compMat.burnWidth; j++) {
    // compMat: R1、R2、R3、G1、G2、G3、B1、B2、B3
      for(int k=0; k<compMat.channels;k++)
      {
          for (int h = 0; h < compMat.compNum; h++)
          {
              RTemp[h] = compMat.burnData[nPlaneSize * (k + h * 3) + compMat.burnWidth * i + j] - compMat.compGrayList[h];
              if (RTemp[h] > plane_max[h]) { plane_max[h] = RTemp[h]; }
              if (RTemp[h] < plane_min[h]) { plane_min[h] = RTemp[h]; }
              /*RTemp[k] = compMat.burnData[nPlaneSize * (k + 3) + compMat.burnWidth * i + j] - compMat.compGrayList[k];
              if (RTemp[k] > plane_max[k]) { plane_max[k] = RTemp[k]; }
              if (RTemp[k] < plane_min[k]) { plane_min[k] = RTemp[k]; }
              RTemp[k] = compMat.burnData[nPlaneSize * (k + 6) + compMat.burnWidth * i + j] - compMat.compGrayList[k];
              if (RTemp[k] > plane_max[k]) { plane_max[k] = RTemp[k]; }
              if (RTemp[k] < plane_min[k]) { plane_min[k] = RTemp[k]; }*/
          }
      }

  }
  for(int h=0; h< compMat.compNum; h++)
  {
        if(GrayList[h] != 0)
        {
            mag[h] = floor((plane_max[h] - plane_min[h]) / 62 );
            if (mag[h] >= 2)
                mag[h] = 2;
            plane_offset[h] = (plane_max[h] + plane_min[h])* pow(2, 2) / 2;
            m_pCompMat->plane_mag[h]=mag[h];
            m_pCompMat->plane_offset[h]=plane_offset[h];

        }
  }
//  mag[0]=(plane1_max-plane1_min)/62-1;
//  plane1_offset=(plane1_max+plane1_min)/2;
//  m_pCompMat->plane_mag[0]=mag[0];
//  m_pCompMat->plane_offset[0]=plane1_offset;
//  mag[1]=(plane2_max-plane2_min)/62-1;
//  plane2_offset=(plane2_max+plane2_min)/2;
//  m_pCompMat->plane_mag[1]=mag[1];
//  m_pCompMat->plane_offset[1]=plane2_offset;
//  mag[2]=(plane3_max-plane3_min)/62-1;
//  plane3_offset=(plane3_max+plane3_min)/2;
//  m_pCompMat->plane_mag[2]=mag[2];
//  m_pCompMat->plane_offset[2]=plane3_offset;
  int cnt = 0;
  for (int i = 0; i < compMat.burnHeight; i++)
  for (int j = 0; j < compMat.burnWidth; j++) {
    // NovaTek: R1、R2、R3、G1、G2、G3、B1、B2、B3
    // compMat: R1、G1、B1、R2、G2、B2、R3、G3、B3
        for(int k=0; k< compMat.channels; k++)
        {
            for (int h = 0; h < compMat.compNum; h++)
            {
                if (GrayList[h] != 0)
                {
                    RTemp[h] = compMat.burnData[nPlaneSize * (k + h * 3) + compMat.burnWidth * i + j] - compMat.compGrayList[h];
                    TEMP = round((RTemp[h] * pow(2, 2) - plane_offset[h]) / pow(2, mag[h]));
                    if (TEMP < -127)
                        ptr[cnt++] = -127;
                    else if (TEMP > 127)
                        ptr[cnt++] = 127;
                    else
                        ptr[cnt++] = TEMP;

                    /*RTemp[k] = compMat.burnData[nPlaneSize * (k + 3) + compMat.burnWidth * i + j] - compMat.compGrayList[k];
                    TEMP = round((RTemp[k] * pow(2, 2) - plane_offset[k]) / pow(2, mag[k]));
                    if (TEMP < -127)
                        ptr[cnt++] = -127;
                    else if (TEMP > 127)
                        ptr[cnt++] = 127;
                    else
                        ptr[cnt++] = TEMP;

                    RTemp[k] = compMat.burnData[nPlaneSize * (k + 6) + compMat.burnWidth * i + j] - compMat.compGrayList[k];
                    TEMP = round((RTemp[k] * pow(2, 2) - plane_offset[k]) / pow(2, mag[k]));
                    if (TEMP < -127)
                        ptr[cnt++] = -127;
                    else if (TEMP > 127)
                        ptr[cnt++] = 127;
                    else
                        ptr[cnt++] = TEMP;*/
                }
            }
        }

  }
  ptr[cnt++] = 0;
}




void Oscar_FORMAT::NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src,
                                     int tbl_num, int tbl_h, int tbl_v) {
  int real_tbl_h=tbl_h + 3;
  demura_src->buffer = Create3Darray(tbl_num, tbl_v, real_tbl_h);
  demura_src->tbl_h = real_tbl_h;
  demura_src->tbl_v = tbl_v;
  demura_src->tbl_num = tbl_num;
  demura_src->size = tbl_num * real_tbl_h * tbl_v;
}


void Oscar_FORMAT::NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                                     DEMURA_SETTING setting,
                                     unsigned char tcon_idx, bool shift_en,
                                     bool dummy_en) {
 /* Q_UNUSED(shift_en)
  Q_UNUSED(dummy_en)*/
  int Table_H;
  switch (tcon_idx) {
    case 0: {
      Table_H = setting.table.h;
      break;
    }
    default: {
      Table_H = setting.table.h / 2 + 1;
      break;
    }
  }

  // TCON demura table description.pdf相关公式
  int Table_V = setting.table.v;
  int nNodeNumber = setting.node_num;
  int Block_H = setting.scale_x;
  //double tmp = (Table_H + (Block_H / 8)) * Table_V * nNodeNumber * 12 / 256;
  int Table_SIZE = (Table_H) * Table_V * nNodeNumber;
  if (Table_SIZE % 2 != 0)
      Table_SIZE = Table_SIZE + 1;

  demura_tbl->buffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * Table_SIZE);
  demura_tbl->size = Table_SIZE;
}

void Oscar_FORMAT::NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                              DEMURA_TABLE_BUFFER &demura_tbl,
                              DEMURA_SETTING setting, unsigned char tcon_idx) {
  //demura_tbl.buffer =
  //    (unsigned char *)malloc(sizeof(unsigned char *) * demura_tbl.size);
  //demura_tbl.crc = (unsigned int *)malloc(sizeof(unsigned int *));
  //demura_tbl.checksum = (unsigned int *)malloc(sizeof(unsigned int *));

  switch (tcon_idx) {
    case 0: {
      int begin = 0;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 1: {
      int begin = 0;
      int end = setting.table.h / 2 + 1;//?
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 2: {
      int begin = setting.table.h / 2 ;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
  }

//  NvtDemura_CalCrcChecksum(demura_tbl.buffer, demura_tbl.size, demura_tbl.crc,
//                           demura_tbl.checksum);
}

void Oscar_FORMAT::TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                            DEMURA_TABLE_BUFFER &demura_tbl,
                            DEMURA_SETTING setting, const int colBegin,
                            const int colEnd) {
  //将两个12bits，存储在三个byte中，高8bits+（合并：低4bits+高4bits）+低8bits
  int cnt = 0;
  bool bEven = true;
  int nRow = setting.table.v;//?
  int nCol = setting.table.h;//?
  int nNodeNum = setting.node_num;
  int *p = &demura_src.buffer[0][0][0];
  int cnt2 = 0;
  int nPlaneSize = nRow * nCol * nNodeNum;
  if (nPlaneSize % 2 != 0)
      nPlaneSize = nPlaneSize + 1;
  for (int k = 0; k < nPlaneSize;) {
      demura_tbl.buffer[cnt] = p[k++];
      demura_tbl.buffer[cnt+1] = p[k++];
      demura_tbl.buffer[cnt+2] = p[k++];
      demura_tbl.buffer[cnt+3] = p[k++];
      //k=k+4;
      cnt=cnt+4;
  }


//  for (int k = 0; k < nRow; k++) {
//    for (int i = colBegin; i < colEnd; i++) {
//      for (int j = 0; j < 3; j++) {
//        cnt2++;
//        int tmp = p[k * nCol * 3 + i * 3 + j];  //补偿值
//        if (bEven) {
//          nFull8bits = (tmp >> 4) & 0xFF;
//          nMerge8bits = (tmp << 4) & 0xF0;
//          bEven = false;
//        } else {
//          nFull8bits = tmp & 0xFF;
//          nMerge8bits += (tmp >> 8) & 0x0F;
//          demura_tbl.buffer[cnt++] = nMerge8bits;  //存储合并的8bits
//          bEven = true;
//        }
//        demura_tbl.buffer[cnt++] = nFull8bits;
//      }
//    }
//  }
//  demura_tbl.buffer[cnt++] = 0;
//  demura_tbl.buffer[cnt++] = 0;
//  demura_tbl.buffer[cnt++] = 0;
//  demura_tbl.buffer[cnt++] = 0;
}

void Oscar_FORMAT::WriteBinFile(const DEMURA_TABLE_BUFFER tbls[],
                            string &strSaveName) {

    LOG(INFO)<<"Save Oscar format Demura to: "<<strSaveName.c_str();
  
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  if (checksum_out) {
      ofstream outfile;
      //outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(m_pCompMat->DemuraAddress, ios::beg);
      // write header
      Oscar_Header header = Init_Oscar_header();
      VD_Oscar_Checksum(tbls[0].size, tbls[0].buffer, checksum_out);
      //header.LUT_CHECKSUM = _byteswap_ushort(*checksum_out);
      m_pCompMat->lut_checksum = *checksum_out;
      LOG(INFO) << "Demura CRC/checksunm is: " << *checksum_out;

      SetHeader(header);
      //WriteHeader(qs, header);
      WriteHeader(outfile, header);

      // Write 1bin, 2table(2 TCONs)
      for (int nTCONindex = 0; nTCONindex < 1; nTCONindex++) {
          DEMURA_TABLE_BUFFER tbl = tbls[nTCONindex];

          for (int idx = 0; idx < tbl.size; idx++) {
              outfile << tbl.buffer[idx];
          }
      }

      outfile.close();
      LOG(INFO) << "Demura CSV2bin finished!";
      FreeMallocInt(checksum_out);
  }

}

void Oscar_FORMAT::SetHeader(Oscar_Header &header) {
  if (m_pCompMat == nullptr) {
    return;
  }
  int GrayList[8] = { 0,0,0,0,0,0,0,0 };
  //BLOCK_SIZE     5:8x8 6:8x16  10:16x16
  if (m_pCompMat->height / (m_pCompMat->burnHeight - 1) == 8 && m_pCompMat->width / (m_pCompMat->burnWidth - 1) == 8)
      header.BLOCK_SIZE = 5;
  else if (m_pCompMat->height / (m_pCompMat->burnHeight - 1) == 16 && m_pCompMat->width / (m_pCompMat->burnWidth - 1) == 16)
      header.BLOCK_SIZE = 10;
  else if (m_pCompMat->height / (m_pCompMat->burnHeight - 1) == 8 && m_pCompMat->width / (m_pCompMat->burnWidth - 1) == 16)
      header.BLOCK_SIZE = 6;

  if (m_pCompMat->channels == 3)
      header.RGB_MODE_ON = (1);
  else
      header.RGB_MODE_ON = (0);

  header.PLANE_NUM = (m_pCompMat->compNum * m_pCompMat->channels);
  header.LUT_SIZE = ((m_pCompMat->burnWidth) * m_pCompMat->burnHeight * m_pCompMat->compNum * m_pCompMat->channels);
  if (header.LUT_SIZE % 2 != 0)
      header.LUT_SIZE = header.LUT_SIZE + 1;
  header.LUT_CHECKSUM =(m_pCompMat->lut_checksum);

  header.LOW_LIMIT_R = (m_pCompMat->lower_Bound);
  header.LOW_LIMIT_G = (m_pCompMat->lower_Bound);
  header.LOW_LIMIT_B = (m_pCompMat->lower_Bound);

  header.HIGH_LIMIT_R = (m_pCompMat->high_Bound);
  header.HIGH_LIMIT_G = (m_pCompMat->high_Bound);
  header.HIGH_LIMIT_B = (m_pCompMat->high_Bound);
  for (int i = 0; i < 8; i++){
      if (m_pCompMat->compGrayList[i] != 0)
          GrayList[i] = m_pCompMat->compGrayList[i];
      else
          GrayList[i] = m_pCompMat->high_Bound;
  }
  header.R_PLANE0 = (GrayList[0]);
  header.R_PLANE1 = (GrayList[1]);
  header.R_PLANE2 = (GrayList[2]);
  header.R_PLANE3 = (GrayList[3]);
  header.R_PLANE4 = (GrayList[4]);
  header.R_PLANE5 = (GrayList[5]);
  header.R_PLANE6 = (GrayList[6]);
  header.R_PLANE7 = (GrayList[7]);

  header.G_PLANE0 = (GrayList[0]);
  header.G_PLANE1 = (GrayList[1]);
  header.G_PLANE2 = (GrayList[2]);
  header.G_PLANE3 = (GrayList[3]);
  header.G_PLANE4 = (GrayList[4]);
  header.G_PLANE5 = (GrayList[5]);
  header.G_PLANE6 = (GrayList[6]);
  header.G_PLANE7 = (GrayList[7]);

  header.B_PLANE0 = (GrayList[0]);
  header.B_PLANE1 = (GrayList[1]);
  header.B_PLANE2 = (GrayList[2]);
  header.B_PLANE3 = (GrayList[3]);
  header.B_PLANE4 = (GrayList[4]);
  header.B_PLANE5 = (GrayList[5]);
  header.B_PLANE6 = (GrayList[6]);
  header.B_PLANE7 = (GrayList[7]);

  header.PLANE_R_MAG = (m_pCompMat->plane_mag[0] + (m_pCompMat->plane_mag[1]<<4) +
          (m_pCompMat->plane_mag[2]<<8) + (m_pCompMat->plane_mag[3]<<12)+ (m_pCompMat->plane_mag[4]<<16) +
          (m_pCompMat->plane_mag[5]<<20) + (m_pCompMat->plane_mag[6]<<24)+ (m_pCompMat->plane_mag[7]<<28));

  header.PLANE_G_MAG = (m_pCompMat->plane_mag[0] + (m_pCompMat->plane_mag[1] << 4) +
      (m_pCompMat->plane_mag[2] << 8) + (m_pCompMat->plane_mag[3] << 12) + (m_pCompMat->plane_mag[4] << 16) +
      (m_pCompMat->plane_mag[5] << 20) + (m_pCompMat->plane_mag[6] << 24) + (m_pCompMat->plane_mag[7] << 28));

  header.PLANE_B_MAG = (m_pCompMat->plane_mag[0] + (m_pCompMat->plane_mag[1] << 4) +
      (m_pCompMat->plane_mag[2] << 8) + (m_pCompMat->plane_mag[3] << 12) + (m_pCompMat->plane_mag[4] << 16) +
      (m_pCompMat->plane_mag[5] << 20) + (m_pCompMat->plane_mag[6] << 24) + (m_pCompMat->plane_mag[7] << 28));

  header.PLANE0_R_OFFSET = (m_pCompMat->plane_offset[0]&0x0FFF);
  header.PLANE1_R_OFFSET = (m_pCompMat->plane_offset[1]&0x0FFF);
  header.PLANE2_R_OFFSET = (m_pCompMat->plane_offset[2]&0x0FFF);
  header.PLANE3_R_OFFSET = (m_pCompMat->plane_offset[3]&0x0FFF);
  header.PLANE4_R_OFFSET = (m_pCompMat->plane_offset[4]&0x0FFF);
  header.PLANE5_R_OFFSET = (m_pCompMat->plane_offset[5]&0x0FFF);
  header.PLANE6_R_OFFSET = (m_pCompMat->plane_offset[6]&0x0FFF);
  header.PLANE7_R_OFFSET = (m_pCompMat->plane_offset[7]&0x0FFF);

  header.PLANE0_G_OFFSET = (m_pCompMat->plane_offset[0] & 0x0FFF);
  header.PLANE1_G_OFFSET = (m_pCompMat->plane_offset[1] & 0x0FFF);
  header.PLANE2_G_OFFSET = (m_pCompMat->plane_offset[2] & 0x0FFF);
  header.PLANE3_G_OFFSET = (m_pCompMat->plane_offset[3] & 0x0FFF);
  header.PLANE4_G_OFFSET = (m_pCompMat->plane_offset[4] & 0x0FFF);
  header.PLANE5_G_OFFSET = (m_pCompMat->plane_offset[5] & 0x0FFF);
  header.PLANE6_G_OFFSET = (m_pCompMat->plane_offset[6] & 0x0FFF);
  header.PLANE7_G_OFFSET = (m_pCompMat->plane_offset[7] & 0x0FFF);

  header.PLANE0_B_OFFSET = (m_pCompMat->plane_offset[0] & 0x0FFF);
  header.PLANE1_B_OFFSET = (m_pCompMat->plane_offset[1] & 0x0FFF);
  header.PLANE2_B_OFFSET = (m_pCompMat->plane_offset[2] & 0x0FFF);
  header.PLANE3_B_OFFSET = (m_pCompMat->plane_offset[3] & 0x0FFF);
  header.PLANE4_B_OFFSET = (m_pCompMat->plane_offset[4] & 0x0FFF);
  header.PLANE5_B_OFFSET = (m_pCompMat->plane_offset[5] & 0x0FFF);
  header.PLANE6_B_OFFSET = (m_pCompMat->plane_offset[6] & 0x0FFF);
  header.PLANE7_B_OFFSET = (m_pCompMat->plane_offset[7] & 0x0FFF);

  unsigned char *ptr = (unsigned char *)&header + 32;
  unsigned int size = sizeof(header) - 32;
//  unsigned int *crc_out = (unsigned int *)malloc(sizeof(unsigned int *));
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  //NvtDemura_CalCrcChecksum(ptr, size, crc_out, checksum_out);
  VD_Oscar_Checksum(size,ptr,checksum_out);
  header.REG_CHECKSUM = (*checksum_out);

  ptr = (unsigned char*)&header;
  VD_Oscar_Checksum(16, ptr, checksum_out);
  header.HEADER_CHECKSUM = (*checksum_out);

  FreeMallocInt(checksum_out);
  //Log4Info(QString("VD_4k_Header, Checksum = 0x%1")
  //             .arg(QString::number(*checksum_out, 16)));
}

void Oscar_FORMAT::WriteHeader(ofstream &qs, Oscar_Header &header) {
  //QByteArray byte = StructToByte(header);
  //qs.writeRawData(byte, sizeof(VD_4k_Header));

  int length = sizeof(header);
  BYTE *p = new BYTE[length];
  memmove(p, &header, length);
  for (int idx = 0; idx < length; idx++) {
	  qs << p[idx];
  }
}

Oscar_Header Oscar_FORMAT::Init_Oscar_header() {
    Oscar_Header header;

  header.REG_SIZE= _byteswap_ulong(0x80000000);
  header.REG_CHECKSUM =_byteswap_ushort(0xCDCD);
  header.RESERVED00 = _byteswap_ushort(0x0000);

  header.LUT_SIZE = (0x00060114);   //484x271x3
  header.LUT_CHECKSUM = _byteswap_ushort(0xCDCD);
  header.RESERVED01 = 0;
  
  header.HEADER_CHECKSUM = _byteswap_ushort(0xCDCD);
  header.RESERVED02 = 0;
  header.RESERVED03 = 0;
  header.RESERVED04 = 0;
  header.RESERVED05 = 0;

  header.DEMURA_ENABLE = 1;  //
  header.BLOCK_SIZE = 5;  //5:8x8 6:8x16  10:16x16
  header.RGB_MODE_ON = (0);
  header.PLANE_NUM = 3;
  header.LOW_LIMIT_R = 12;
  header.LOW_LIMIT_G = 12;
  header.LOW_LIMIT_B = 12;
  header.R_PLANE0 = 100;
  header.R_PLANE1 = 240;
  header.R_PLANE2 = 900;
  header.R_PLANE3 = (0);
  header.R_PLANE4 = (0);
  header.R_PLANE5 = (0);
  header.R_PLANE6 = (0);
  header.R_PLANE7 = (0);

  header.G_PLANE0 = (0);
  header.G_PLANE1 = (0);
  header.G_PLANE2 = (0);
  header.G_PLANE3 = (0);
  header.G_PLANE4 = (0);
  header.G_PLANE5 = (0);
  header.G_PLANE6 = (0);
  header.G_PLANE7 = (0);

  header.B_PLANE0 = (0);
  header.B_PLANE1 = (0);
  header.B_PLANE2 = (0);
  header.B_PLANE3 = (0);
  header.B_PLANE4 = (0);
  header.B_PLANE5 = (0);
  header.B_PLANE6 = (0);
  header.B_PLANE7 = (0);

  header.HIGH_LIMIT_R = 1020;
  header.HIGH_LIMIT_G = 1020;
  header.HIGH_LIMIT_B = 1020;

  header.PLANE_R_MAG = (0);
  header.PLANE_G_MAG = (0);
  header.PLANE_B_MAG = (0);

  header.PLANE0_R_OFFSET = 0;
  header.PLANE1_R_OFFSET = 0;
  header.PLANE2_R_OFFSET = 0;
  header.PLANE3_R_OFFSET = 0;
  header.PLANE4_R_OFFSET = 0;
  header.PLANE5_R_OFFSET = 0;
  header.PLANE6_R_OFFSET = 0;
  header.PLANE7_R_OFFSET = 0;
  
  header.PLANE0_G_OFFSET = 0;
  header.PLANE1_G_OFFSET = 0;
  header.PLANE2_G_OFFSET = 0;
  header.PLANE3_G_OFFSET = 0;
  header.PLANE4_G_OFFSET = 0;
  header.PLANE5_G_OFFSET = 0;
  header.PLANE6_G_OFFSET = 0;
  header.PLANE7_G_OFFSET = 0;

  header.PLANE0_B_OFFSET = 0;
  header.PLANE1_B_OFFSET = 0;
  header.PLANE2_B_OFFSET = 0;
  header.PLANE3_B_OFFSET = 0;
  header.PLANE4_B_OFFSET = 0;
  header.PLANE5_B_OFFSET = 0;
  header.PLANE6_B_OFFSET = 0;
  header.PLANE7_B_OFFSET = 0;
  
  header.RESERVED07 = 0;
  header.RESERVED08 = 0;
  header.RESERVED09 = 0;
  header.RESERVED10 = 0;
  header.RESERVED11 = 0;
  header.RESERVED12 = 0;
  header.RESERVED13 = 0;
  header.RESERVED14 = 0;
  header.RESERVED15 = 0;
  header.RESERVED16 = 0;
  header.RESERVED17 = 0;
  header.RESERVED18 = 0;
  header.RESERVED19 = 0;
  header.RESERVED20 = 0;
  header.RESERVED21 = 0;
  header.RESERVED22 = 0;
  header.RESERVED23 = 0;
  header.RESERVED24 = 0;
  header.RESERVED25 = 0;
  header.RESERVED26 = 0;
  header.RESERVED27 = 0;
  header.RESERVED28 = 0;
  header.RESERVED29 = 0;
  header.RESERVED30 = 0;
  header.RESERVED31 = 0;
  header.RESERVED32 = 0;
  header.RESERVED33 = 0;
  header.RESERVED34 = 0;
  header.RESERVED35 = 0;
  header.RESERVED36 = 0;
  header.RESERVED37 = 0;
  header.RESERVED38 = 0;
 
  return header;
}
