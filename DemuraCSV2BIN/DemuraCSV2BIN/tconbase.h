#ifndef TCONBASE_H
#define TCONBASE_H
#include <string>

using namespace std;

template <typename T>
extern T  StructToByte(T  &ss) {
	T ba;
	ba.resize(sizeof(T));
	memset(ba.data(), 0, sizeof(T));
	memcpy(ba.data(), &ss, sizeof(T));
	return ba;
}


int ***Create3Darray(int m, int n, int t);
void Free3Darray(int ***p);
void FreeMallocInt(unsigned int *p);
void NvtCRC8(unsigned int size, const unsigned char *buf, unsigned int *crc_out,
	unsigned int *checksum_out);
void NvtCRC(unsigned int size, const unsigned char* buf, unsigned int* crc_out,
    unsigned int* checksum_out);
void VD_Checksum(unsigned int size, const unsigned char* buf,
    unsigned int* checksum_out);
void VD_Oscar_Checksum(unsigned int size, const unsigned char* buf,
    unsigned int* checksum_out);
void SDC_Checksum(unsigned int size, const unsigned char* buf,
    unsigned int* checksum_out);
void CSOTCRC16(unsigned int size, const unsigned char *buf,
unsigned int *checksum_out,unsigned int preCRC);

#define _DEMURA_MAX_TBL_NUM    12
#define _DEMURA_MAX_NODE       12
#define _DEMURA_MAX_NODE_MONO  12
#define _DEMURA_MAX_NODE_RGB    4
#define _DEMURA_MAX_NODE_RGBW   3
#define _DEMURA_MAX_STEPOFF_NUM 4

/**
* 对应目前已支持的TCON 型号；
*/
typedef enum {
    NT_71102,
    VD_8K_Format,
    NT_71267,
    CSOT_FORMAT,
    NT_71733B,
    NT_71265,
    CSOT_FORMAT_Dual,
    EU_3,
    Oscar_Single,
    Oscar_Double
} DemuraTCONtype;

typedef enum DATA_BITS
{
    DATA_8_BITS  = 0,
    DATA_10_BITS = 1,
    DATA_12_BITS = 2
}DATA_BITS;

typedef enum DEMURA_MODE
{
    DEMURA_MODE_MONO = 0,
    DEMURA_MODE_RGB  = 1,
    DEMURA_MODE_RGBW = 2
}DEMURA_MODE;

typedef enum SLPIT_DIR
{
    SPLIT_DIR_LR = 0,
    SPLIT_DIR_UD
}SLPIT_DIR;

//typedef enum DEMURA_GRID_POSITION1
//{
//    DEMURA_GRID_POS_00 = 0,
//    DEMURA_GRID_POS_MID
//}DEMURA_GRID_POSITION1;

typedef struct DEMURA_TABLE_BUFFER
{
    unsigned char* buffer;
    int            size;
    unsigned int   *crc;
    unsigned int   *checksum;
}DEMURA_TABLE_BUFFER;
//struct DEMURA_TABLE_BUFFER *pDEMURA_TABLE_BUFFER;
//typedef struct
//{
//    unsigned int   *crc;
//    unsigned int   *checksum;
//}DEMURA_SHARP_TABLE_BUFFER, *pDEMURA_SHARP_TABLE_BUFFER;

typedef struct DEMURA_SOURCE_BUFFER
{
    int*** buffer;
    int    tbl_num;
    int    tbl_h;
    int    tbl_v;
    int    size;
}DEMURA_SOURCE_BUFFER;

typedef struct DEMURA_SETTING
{
    DEMURA_MODE mode;
    int         node_num;
    int         plane_num;
    int         node_lv[_DEMURA_MAX_NODE];
    //DEMURA_FUNC function;

    struct
    {
        int h;
        int v;
        DATA_BITS bits;
    }table;
    struct
    {
        int h;
        int v;
    }src;

    SLPIT_DIR   split_dir;
    int scale_x;
    int scale_y;
    unsigned char total_tcon_count;

}DEMURA_SETTING;

//const DEMURA_SETTING DEMURA_DEFULT_SETTING =
//{
//    DEMURA_MODE_MONO,
//    3,    // node number
//    3,    // plane number
//    {0},  // demura node level (12 bits)

//    {
//        false, // dummy enable
//        false, // auo_mode_enable
//        false, // shift_mode_enable
//    },

//    {
//        481,
//        271,
//        DATA_12_BITS,
//    },
//    {
//        481,
//        271,
//    },
//    SPLIT_DIR_LR,
//    8,
//    8,
//    2,
//};

typedef struct CompMat {
  int compNum;            //< 待补偿的plane数量
  int compGrayList[10];   //< 待补偿的灰阶序列
  int channels;           //< 色彩通道数量
  int width;              //< 补偿图像宽，以像素为单位
  int height;             //< 补偿图像高，以像素为单位
  int tcon_num;
  //int nPixel;             //< 补偿图像像素个数
  //int imageSize;          //< 补偿图像数据大小，以字节为单位
  //float* compData;        //< 指向CPU内存的补偿数据区
  //float* compDeviceData;  //< 指向GPU内存的补偿数据区

  ////最多支持5个plane补偿
  //std::string levelStringList[5] = {"LEVEL 1", "LEVEL 2", "LEVEL 3", "LEVEL 4",
  //                                  "LEVEL 5"};
  ////格式按照plane1 - RGB, plane2 - RGB, ......
  //std::string colorStringList[3] = {"RED", "GREEN", "BLUE"};

  int burnWidth;      //< 烧录图像宽，以像素为单位
  int burnHeight;     //< 烧录图像高，以像素为单位
  int lower_Bound;    //demura 低灰限制
  int high_Bound;    //demura 高灰限制
  int nBurnPixel;     //< 烧录图像素个数
  int burnImageSize;  //< 烧录图像数据大小，以字节为单位
  float* burnData;    //< 指向CPU内存的图像数据区
  unsigned int lut_checksum;
  unsigned int lut_size;
  unsigned int plane_mag[8];
  unsigned int plane_offset[8];
  int DemuraAddress;


} CompMat;


typedef struct PMicCompMat {
  unsigned int lut_size;
  int VcomAddress;
  int DefaultVcom;
  int TconType;
  char *PMIC_CodeDir;
  char* CellInfo;
  int *burnData;  //< 指向CPU内存的数据区
  int burnsize;
  unsigned int lut_checksum;
} PMicCompMat;

typedef struct GmCompMat {
  int compNum;           //< 待补偿的plane数量
  int compGrayList[10];  //< 待补偿的灰阶序列
  int channels;          //< 色彩通道数量
  int *burnData_R;    //< 指向CPU内存的图像数据区
  int *burnData_G;       //< 指向CPU内存的数据区
  int *burnData_B;       //< 指向CPU内存的数据区
  unsigned int lut_checksum;
  unsigned int lut_size;
  int GmType;
  int Address;
  int GMInputBits;
  int GMOutputBits;
  int TconType;
} GmCompMat;

/***************************************************************
 *  @brief：Demura CompMat结构体构建函数
 *  @param[1]     int compNum        相机拍摄的有效亮度图的数量
 *  @param[2]     int *compGrayList  待补偿灰阶数组，最大可设置10个
 *  @param[3]     int channels       颜色通道数量
 *  @param[4]     int width          图像宽
 *  @param[5]     int height         图像高
 *  @param[6]     int burnWidth      烧录数据宽
 *  @param[7]     int burnHeight     烧录数据高
 *  @param[8]     int lowBound       demura补正的灰阶下限
 *  @param[9]     int highBound      demura补正的灰阶上限
 **************************************************************/
CompMat* CreateCompMat(int compNum, int* compGrayList,
	int channels, int width, int height,
	int burnWidth, int burnHeight, int lowBound, int highBound);

/***************************************************************
 *  @brief：Demura CompMat结构体构建函数
 *  @param[1]     int compNum        相机拍摄的有效亮度图的数量
 *  @param[2]     int *compGrayList  待补偿灰阶数组，最大可设置10个
 *  @param[3]     int channels       颜色通道数量
 *  @param[4]     int width          图像宽
 *  @param[5]     int height         图像高
 *  @param[6]     int burnWidth      烧录数据宽
 *  @param[7]     int burnHeight     烧录数据高
 *  @param[8]     int lowBound       demura补正的灰阶下限
 *  @param[9]     int highBound      demura补正的灰阶上限
 *  @param[10]    int DemuraAddress  数据的烧录地址
 **************************************************************/
CompMat *CreateAllCompMat(int compNum, int *compGrayList, int channels,
                          int width, int height, int burnWidth, int burnHeight,
                          int lowBound, int highBound, int DemuraAddress);

/***************************************************************
 *  @brief：PMIC CompMat结构体构建函数
 *  @param[1]     int VcomAddress    数据的烧录地址
 *  @param[2]     int DefaultVcom    预设VCOM值
 **************************************************************/
PMicCompMat *CreatePMicCompMat(int VcomAddress,int DefaultVcom);

/***************************************************************
 *  @brief：Gamma CompMat结构体构建函数
 *  @param[1]     int Address        数据的烧录地址
 *  @param[2]     int GMInputBits    ACC LUT 输入bit，通常8/10bit
 *  @param[3]     int GMOutputBits   ACC LUT 输出bit，通常10/11/12bit
 **************************************************************/
GmCompMat *CreateGmCompMat(int Address,int GMInputBits, int GMOutputBits);

/*************************************************************
 ******************       算法参数结构体      *******************
 ******************      变量名与CSV统一     *******************
 *************************************************************/
 //typedef struct DemuraParam {
 //  int Camera_Image_Size[2];
 //  int Correct_Image_Size[2];
 //  int Burn_Table_Size[2];
 //  int Color_Channels;
 //  int Camera_Gray_Num;
 //  int Camera_Gray_Level[10];
 //  int Compensation_Num;
 //  int Compensation_Plane[5];
 //  int Fixed_Point_Parameter[9];
 //  int Barcode_Param[5];
 //  int Color_Mode;
 //  int Save_Image_Flag;
 //  int Barcode_Process_Flag;
 //  int Particle_Process_Flag;
 //  int Filter_Process_Flag;
 //  int BlackEdge_Process_Flag;
 //  int Float_Version_Flag;
 //} DemuraParam;

 //extern DemuraParam DEMURA_PARAM;
#endif // TCONBASE_H
