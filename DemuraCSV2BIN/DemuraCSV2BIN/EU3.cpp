/**
 * @projectName   CSOT_EEAD_ALG2_DEMURA
 * @brief         Demura
 * @author        wxq/cxh
 * @date          2021-07-31 17:01
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#include "stdafx.h"
#include "EU3.h"
#include <WINDOWS.H>
#include <iostream>
#include <fstream>
#include <cmath>
#include "tconbase.h"
#include "tconfactor.h"
#include "easylogging++.h"
using namespace std;

EU3::EU3() {}
EU3::~EU3() {}

int EU3::ToBin(CompMat *compMat, string &strSaveName) {

  if (compMat == nullptr || strSaveName.empty()) {
    LOG(INFO) << "Demura compMat == nullptr or strSaveName empty";
    return 0;
  }
    return(ToBinNoDll(compMat, strSaveName)); 
    
}

int EU3::PMicToBin(PMicCompMat* compMat, string& strSaveName) {

  if (compMat == nullptr || strSaveName.empty()) {

    LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
    return 0;
  }
  

  LOG(INFO) << "Start Save  VD format VCOM to:" << strSaveName.c_str();

  compMat->burnsize = 8;
  unsigned char *burnbuffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * compMat->burnsize);
  unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
  if (burnbuffer && checksum_out) 
  {
      burnbuffer[0] = 0x00;
      burnbuffer[1] = 0x40;
      burnbuffer[2] = 0x00;
      burnbuffer[3] = 0x05;
      burnbuffer[4] = 0x00;
      int vcom = (compMat->burnData[0] << 1) + 1;
      burnbuffer[5] = vcom & 0xFF;
      burnbuffer[6] = 0x00;
      burnbuffer[7] = 0x00;

      VD_Checksum(compMat->burnsize - 2, burnbuffer, checksum_out);
      burnbuffer[7] = *checksum_out;

      ofstream outfile;
      // outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(compMat->VcomAddress, ios::beg);
      for (int idx = 0; idx < compMat->burnsize;) {
          outfile << burnbuffer[idx + 2];
          outfile << burnbuffer[idx + 3];
          outfile << burnbuffer[idx + 0];
          outfile << burnbuffer[idx + 1];
          idx += 4;
      }
      outfile.close();
      LOG(INFO) << "VCOM code2bin finished!";
      if (burnbuffer != NULL) {
          free(burnbuffer);
          burnbuffer = NULL;
          LOG(INFO) << "free VCOM burnbuffer!";
      }
      FreeMallocInt(checksum_out);
      return 1;
  }
  else{
      LOG(INFO) << "VCOM code2bin fail,malloc fail!";
      return 0;
  }

}

int EU3::GmToBin(GmCompMat* compMat, string& strSaveName) {

    LOG(INFO) << "Start Save VD format Gamma to:" << strSaveName.c_str();
    if (compMat == nullptr) {
        LOG(INFO) << "Gamma code2bin fail,compMat == nullptr!";
        return 0;
    }
    if (strSaveName.empty()) {
        LOG(INFO) << "Gamma code2bin fail,strSaveName empty!";
        return 0;
    }

    char* burnbuffer = (char*)malloc(sizeof(char*) * 6148);  // pow(2, compMat.GMInputBits) * 3 *compMat.GMOutputBits /8);
    if (burnbuffer) {
        switch (compMat->GmType) {
        case 1:
            compMat->lut_size = CreatVD60HzFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatVD60HzFromat!     lut_size:"<< compMat->lut_size;
            break;
        case 2:
            compMat->lut_size = CreatVD120HzFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatVD120HzFromat!    lut_size:" << compMat->lut_size;
            break;
        default:
            compMat->lut_size = 0;
            LOG(INFO) << "Creat gamma Fromat err!   lut_size:" << compMat->lut_size;
            return 0;
            break;
        }

        ofstream outfile;
        // outfile.open(strSaveName, ios::out | ios::binary);
        outfile.open(strSaveName, ios::_Nocreate | ios::binary);
        outfile.seekp(compMat->Address, ios::beg);
        outfile.write(burnbuffer, compMat->lut_size);
        outfile.close();

        LOG(INFO) << "Gamma code2bin finished!";
        if (burnbuffer != NULL) {
            free(burnbuffer);
            burnbuffer = NULL;
            LOG(INFO) << "free Gamma burnbuffer!";
        }

        return 1;
    }
    else {
        LOG(INFO) << "Gamma code2bin fail,malloc fail!";
        return 0;
    }

    
}

int EU3::CreatVD60HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out=0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    i += 2;
  }
  int bank_size = 0x1800 - 0x200 * 3;
  for (int i = 0; i < bank_size + 2; i++) {
    ptr[j++] = 0x00;
  }
  
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  //FreeMallocInt(checksum_out);
}
int EU3::CreatVD120HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out = 0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
      i += 2;
  }
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
    i += 2;
  }
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
    i += 2;
  }
  checksum_out += ptr[j++] = 0x00;
  checksum_out += ptr[j++] = 0x00;
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  // FreeMallocInt(checksum_out);
}

int EU3::ToBinNoDll(CompMat *compMat, string &strSaveName) {
  m_pCompMat = compMat;

  // Step 1 – Create source buffer
  DEMURA_SOURCE_BUFFER src;
  NoDll_CreateSrcBuffer(&src, compMat->channels * compMat->compNum,
                        compMat->burnWidth, compMat->burnHeight);
  // Step 2 – Read demura compensation data
  ReadDemuraCompensationData(src, *compMat);

  // Step 3 – Demura setting
  DEMURA_SETTING setting ;
  setting.total_tcon_count = 0;
  setting.table.h = compMat->burnWidth;
  setting.table.v = compMat->burnHeight;
  setting.scale_x = 8;
  setting.scale_y = 8;
  setting.node_num = compMat->channels * compMat->compNum;
  setting.plane_num = compMat->compNum;
  // Step 4 – Create table buffer
  DEMURA_TABLE_BUFFER tbls[2];
  NoDll_CreateTblBuffer(&tbls[0], setting, setting.total_tcon_count);

  // Step 5 – Convert demura table
  NoDll_TableGen(src, tbls[0], setting, setting.total_tcon_count);

  // Step 6 – Save demura table
  WriteBinFile(tbls, strSaveName);

  // Step 7 –Free source and table buffer

  if (tbls[0].buffer != NULL) {
	  free(tbls[0].buffer);
	  tbls[0].buffer = NULL;
      LOG(INFO) << "free demura burnbuffer!";
  }

  /*FreeMallocInt(tbls[0].checksum);
  FreeMallocInt(tbls[0].crc);*/
  Free3Darray(src.buffer);
  LOG(INFO) << "free demura databuffer!";
  return 1;
}

void EU3::ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                          const CompMat &compMat) {
  int *ptr = &src.buffer[0][0][0];
  int nPlaneSize = compMat.burnHeight * compMat.burnWidth;
  //float R1, G1, B1, R2, G2, B2, R3, G3, B3;  //补偿值
  float RTemp[8];
  unsigned mag[8];
  float plane_max[8]={0,0,0,0,0,0,0,0},plane_min[8]={1020,1020,1020,1020,1020,1020,1020,1020};
  float plane1_max=0,plane1_min=1020,plane1_offset=0;
  float plane2_max=0,plane2_min=1020,plane2_offset=0;
  float plane3_max=0,plane3_min=1020,plane3_offset=0;
  for (int i = 0; i < compMat.burnHeight; i++)
  for (int j = 0; j < compMat.burnWidth; j++) {
    // NovaTek: R1、R2、R3、G1、G2、G3、B1、B2、B3
    // compMat: R1、G1、B1、R2、G2、B2、R3、G3、B3
      for(int k=0; k<compMat.compNum;k++)
      {
            if(compMat.compGrayList[k] != 0)
            {
                RTemp[k] = compMat.burnData[nPlaneSize * k * 3 + compMat.burnWidth* i + j] - compMat.compGrayList[k];
                if(RTemp[k]>plane_max[k]){plane_max[k]=RTemp[k];}
                if(RTemp[k]<plane_min[k]){plane_min[k]=RTemp[k];}
            }
      }

  }
  for(int k=0; k<compMat.compNum;k++)
  {
        if(compMat.compGrayList[k] != 0)
        {
            mag[k] = floor((plane_max[k] - plane_min[k]) / 62 );
            if (mag[k] >= 2)
                mag[k] = 2;            
            m_pCompMat->plane_mag[k]=mag[k];
        }
  }

  for (int i = 0; i < compMat.burnHeight; i++)
  for (int j = 0; j < compMat.burnWidth; j++) {
    // EU3: R1、R2、R3、R4、R5、R6
    // compMat: R1、G1、B1、R2、G2、B2、R3、G3、B3、R4、G4、B4、R5、G5、B5、R6、G6、B6 
      for (int k = 0; k < compMat.compNum; k++)
      {
          if (compMat.compGrayList[k] != 0)
          {
              RTemp[k] = compMat.burnData[nPlaneSize * k * 3 + compMat.burnWidth * i + j] - compMat.compGrayList[k];
              ptr[(compMat.compNum * i + k) * (compMat.burnWidth) + j] = round((RTemp[k] * pow(2, 2)) / pow(2, mag[k]));
              if (ptr[(compMat.compNum * i + k) * (compMat.burnWidth) + j] < -127)
                  ptr[(compMat.compNum * i + k) * (compMat.burnWidth) + j] = -127;
              else if (ptr[(compMat.compNum * i + k) * (compMat.burnWidth) + j] > 127)
                  ptr[(compMat.compNum * i + k) * (compMat.burnWidth) + j] = 127;

          }
      }
  }

}




void EU3::NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src,
                                     int tbl_num, int tbl_h, int tbl_v) {
  int real_tbl_h=tbl_h + 3;
  demura_src->buffer = Create3Darray(tbl_num, tbl_v, real_tbl_h);
  demura_src->tbl_h = real_tbl_h;
  demura_src->tbl_v = tbl_v;
  demura_src->tbl_num = tbl_num;
  demura_src->size = tbl_num * real_tbl_h * tbl_v;
}


void EU3::NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                                     DEMURA_SETTING setting,
                                     unsigned char tcon_idx, bool shift_en,
                                     bool dummy_en) {
 /* Q_UNUSED(shift_en)
  Q_UNUSED(dummy_en)*/
  int Table_H;
  switch (tcon_idx) {
    case 0: {
      Table_H = setting.table.h;
      break;
    }
    default: {
      Table_H = setting.table.h / 2 + 1;
      break;
    }
  }

  // TCON demura table description.pdf相关公式
  int Table_V = setting.table.v;
  int nNodeNumber = setting.node_num;
  int Block_H = setting.scale_x;
  //double tmp = (Table_H + (Block_H / 8)) * Table_V * nNodeNumber * 12 / 256;
  int Table_SIZE = (Table_H) * Table_V * nNodeNumber;

  demura_tbl->buffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * Table_SIZE);
  demura_tbl->size = Table_SIZE;
}

void EU3::NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                              DEMURA_TABLE_BUFFER &demura_tbl,
                              DEMURA_SETTING setting, unsigned char tcon_idx) {
  //demura_tbl.buffer =
  //    (unsigned char *)malloc(sizeof(unsigned char *) * demura_tbl.size);
  //demura_tbl.crc = (unsigned int *)malloc(sizeof(unsigned int *));
  //demura_tbl.checksum = (unsigned int *)malloc(sizeof(unsigned int *));

  switch (tcon_idx) {
    case 0: {
      int begin = 0;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 1: {
      int begin = 0;
      int end = setting.table.h / 2 + 1;//?
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 2: {
      int begin = setting.table.h / 2 ;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
  }

//  NvtDemura_CalCrcChecksum(demura_tbl.buffer, demura_tbl.size, demura_tbl.crc,
//                           demura_tbl.checksum);
}

void EU3::TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                            DEMURA_TABLE_BUFFER &demura_tbl,
                            DEMURA_SETTING setting, const int colBegin,
                            const int colEnd) {
  //将两个12bits，存储在三个byte中，高8bits+（合并：低4bits+高4bits）+低8bits
  int cnt = 0;
  bool bEven = true;
  int nRow = setting.table.v;//?
  int nCol = setting.table.h;//?
  int nNodeNum = setting.node_num;
  int *p = &demura_src.buffer[0][0][0];
  int nPlaneSize = nRow * nCol * setting.plane_num;
  for (int k = 0; k < nPlaneSize;k++) {
      demura_tbl.buffer[cnt++] = p[k];
      
  }

}

void EU3::WriteBinFile(const DEMURA_TABLE_BUFFER tbls[],
                            string &strSaveName) {

    LOG(INFO)<<"Save EU3 format Demura to: "<<strSaveName.c_str();
  
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  if (checksum_out) {
      ofstream outfile;
      //outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      
      char* buffer = (char*)malloc(sizeof(char*) * 1057362);
      memset(buffer, 0x00, 1057362);
      outfile.write(buffer, 1057362);
      if (buffer != nullptr) {
          free(buffer);
          buffer = nullptr;
      }
      outfile.seekp(0x000000, ios::beg);
      outfile << char(0x01);
      outfile.seekp(0x000199, ios::beg);
      outfile << char(0xFE);
      outfile << char(0x01);
      outfile.seekp(0x0029EC, ios::beg);
      outfile << char(0xFE);
      outfile << char(0x01);
      outfile.seekp(0x003AE3, ios::beg);
      outfile << char(0xFE);

      outfile.seekp(m_pCompMat->DemuraAddress, ios::beg);
      // write header
      EU3_Header header = Init_EU3_header();
      SDC_Checksum(tbls[0].size, tbls[0].buffer, checksum_out);
      //header.LUT_CHECKSUM = _byteswap_ushort(*checksum_out);
      m_pCompMat->lut_checksum = *checksum_out;
      LOG(INFO) << "Demura CRC/checksunm is: " << *checksum_out;

      SetHeader(header);
      //WriteHeader(qs, header);
      WriteHeader(outfile, header);

      // Write 1bin, 2table(2 TCONs)
      for (int nTCONindex = 0; nTCONindex < 1; nTCONindex++) {
          DEMURA_TABLE_BUFFER tbl = tbls[nTCONindex];

          for (int idx = 0; idx < tbl.size; idx++) {
              outfile << tbl.buffer[idx];
          }
      }

      outfile.close();
      LOG(INFO) << "Demura CSV2bin finished!";
      FreeMallocInt(checksum_out);
  }

}

void EU3::SetHeader(EU3_Header &header) {
  if (m_pCompMat == nullptr) {
    return;
  }

  header.LUT_CHECKSUM =m_pCompMat->lut_checksum & 0xFF;

  header.PLANE0 = m_pCompMat->compGrayList[0]/4;
  header.PLANE1 = m_pCompMat->compGrayList[1]/4;
  header.PLANE2 = m_pCompMat->compGrayList[2]/4;
  header.PLANE3 = m_pCompMat->compGrayList[3]/4;
  header.PLANE4 = m_pCompMat->compGrayList[4]/4;
  header.PLANE5 = m_pCompMat->compGrayList[5]/4;

  header.PLANE_MAG = _byteswap_ushort(m_pCompMat->plane_mag[0] + (m_pCompMat->plane_mag[1] << 2) +
      (m_pCompMat->plane_mag[2] << 4) + (m_pCompMat->plane_mag[3] << 6) + (m_pCompMat->plane_mag[4] << 8) +
      (m_pCompMat->plane_mag[5] << 10) + (m_pCompMat->plane_mag[6] << 12));
}

void EU3::WriteHeader(ofstream &qs, EU3_Header &header) {
  //QByteArray byte = StructToByte(header);
  //qs.writeRawData(byte, sizeof(VD_4k_Header));

    int length = sizeof(header);
  BYTE *p = new BYTE[length];
  memmove(p, &header, length);
  for (int idx = 0; idx < length; idx++) {
	  qs << p[idx];
  }
}

EU3_Header EU3::Init_EU3_header() {
  EU3_Header header;
  header.LUT_CHECKSUM = 0X00;
  header.LUT_PARA00 = 0X00;   //fix 0x00
  header.PLANE_MAG = 0X00;
  header.PLANE0 = 16;
  header.PLANE1 = 24;
  header.PLANE2 = 32;
  header.PLANE3 = 64;
  header.PLANE4 = 128;
  header.PLANE5 = 192;
  header.LUT_PARA01 = _byteswap_ushort(0xFFFF);   //fix 0x0FFFF
  header.LUT_PARA02 = _byteswap_ushort(0xFFFF);   //fix 0x0FFFF
  header.RESERVED0 = _byteswap_ushort(0x0000);   //fix 0x00
  header.RESERVED1 = _byteswap_ushort(0x0000);   //fix 0x00
  header.RESERVED2 = _byteswap_ushort(0x0000);   //fix 0x00

  return header;
}
