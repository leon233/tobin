#include "stdafx.h"
#include "dm_binbase.h"

#include <fstream>
#include <sstream>  //
#include <vector>
#include <string>
//#include "tconbase.h"

Csv2bin::Csv2bin()
{
	m_pTCONnvt71102 = TCONfactor::Create(DemuraTCONtype::NT_71102);
	m_pTCONnvt71267 = TCONfactor::Create(DemuraTCONtype::NT_71267);
	m_pTCONcsot = TCONfactor::Create(DemuraTCONtype::CSOT_FORMAT);
	m_pTCONvd = TCONfactor::Create(DemuraTCONtype::VD_8K_Format);
	m_pTCONnvt71733B = TCONfactor::Create(DemuraTCONtype::NT_71733B);
	m_pTCONnvt71265 = TCONfactor::Create(DemuraTCONtype::NT_71265);
	m_pTCONcsot_dual = TCONfactor::Create(DemuraTCONtype::CSOT_FORMAT_Dual);
	m_pTCON_EU3 = TCONfactor::Create(DemuraTCONtype::EU_3);
	m_pTCON_Oscar = TCONfactor::Create(DemuraTCONtype::Oscar_Single);
	m_pTCON_Oscar_double = TCONfactor::Create(DemuraTCONtype::Oscar_Double);
	m_pTCONabstract = nullptr;
	m_bTCONSLected = false;
}

Csv2bin::~Csv2bin()
{
}

void Csv2bin::setPTCONabstract(const DemuraTCONtype index)
{
	m_bTCONSLected = true;

	switch (index) {

	case DemuraTCONtype::NT_71102: {
		m_pTCONabstract = m_pTCONnvt71102;
		break;
	}
	case DemuraTCONtype::NT_71267: {
		m_pTCONabstract = m_pTCONnvt71267;
		break;
	}
	case DemuraTCONtype::CSOT_FORMAT: {
		m_pTCONabstract = m_pTCONcsot;
		break;
	}
	case DemuraTCONtype::VD_8K_Format: {
		m_pTCONabstract = m_pTCONvd;
		break;
	}
	case DemuraTCONtype::NT_71733B: {
		m_pTCONabstract = m_pTCONnvt71733B;
		break;
	}
	case DemuraTCONtype::NT_71265: {
		m_pTCONabstract = m_pTCONnvt71265;
		break;
	}
	case DemuraTCONtype::CSOT_FORMAT_Dual: {
		m_pTCONabstract = m_pTCONcsot_dual;
		break;
	}
	case DemuraTCONtype::EU_3: {
		m_pTCONabstract = m_pTCON_EU3;
		break;
	}
	case DemuraTCONtype::Oscar_Single: {
		m_pTCONabstract = m_pTCON_Oscar;
		break;
	}
	case DemuraTCONtype::Oscar_Double: {
		m_pTCONabstract = m_pTCON_Oscar_double;
		break;
	}
	default: {
		m_pTCONabstract = nullptr;
		m_bTCONSLected = false;
		break;
	}
	}
}
int Csv2bin::run(string csvPath, CompMat *pCompMat){
    return(m_pTCONabstract->ToBin(pCompMat, csvPath));
}
int Csv2bin::run_PMIC(string csvPath, PMicCompMat* pPMicCompMat) {
	return(m_pTCONabstract->PMicToBin(pPMicCompMat, csvPath));
}
int Csv2bin::run_GM(string csvPath, GmCompMat* pGmCompMat) {
	return(m_pTCONabstract->GmToBin(pGmCompMat, csvPath));
}
bool splitString(char spCharacter, const string& objString, vector<string>& stringVector)
{
	if (objString.length() == 0)
	{
		return true;
	}

	size_t posBegin = 0;
	size_t posEnd = 0;
	bool lastObjStore = true;

	while (posEnd != string::npos)
	{
		posBegin = posEnd;
		posEnd = objString.find(spCharacter, posBegin);

		if (posBegin == posEnd)
		{
			posEnd += 1;
			continue;
		}

		if (posEnd == string::npos)
		{
			stringVector.push_back(objString.substr(posBegin, objString.length() - posBegin));
			break;
		}

		stringVector.push_back(objString.substr(posBegin, posEnd - posBegin));
		posEnd += 1;
	}
	return true;
}
bool Csv2bin::ReadCSV(const char * strPanelID, CompMat *pCompMat)
{
	int row_cnt = 0,data_cnt=0;
	string line;
	std::ifstream inFile(strPanelID, std::ios::in | std::ios::binary);
	if (!inFile.is_open()) {
		return false;//UN_VALID_FILE;
	}
	//检查是否开发Demura LUT 文件："Correction Data"开头
	getline(inFile, line); row_cnt++;
	if (strstr(line.c_str(), "Correction Data") == NULL) {
		inFile.close();
		return false;  //读取文件不是Demura LUT，或缺少"Correction Data"开头
	}
	else{
		getline(inFile, line); row_cnt++;
	}
		

	while (getline(inFile, line)){
		string field;
		istringstream sin(line);
		//检查非有效数据列： "Blocksize" "Level"
		if(strstr(line.c_str(), "size")==NULL && strstr(line.c_str(), "Level") == NULL && strstr(line.c_str(), "Size") == NULL && strstr(line.c_str(), "LEVEL") == NULL)
		while (getline(sin, field, ',')){
			pCompMat->burnData[data_cnt++] = atof(field.c_str());			
		}
		row_cnt++;
	}
	if (row_cnt != pCompMat->burnHeight*pCompMat->compNum * 3 + 2 + 4 * pCompMat->compNum) {
		return false;  //读取数据长度不正确
	}
	inFile.close();
	return true;
}
string Csv2bin::GenerateBinName(const char *& strPanelID)
{
	/*if (strPanelID.isEmpty()) {
		return nullptr;
	}*/

	//  QFileInfo fi = QFileInfo(strPanelID);
	//  QString strBinName = fi.fileName().replace(".csv", ".bin");

	//  QDir dir(OUTPUT_FOLDER + "/BinData");
	//  if (!dir.exists()) {
	//    dir.mkdir(dir.absolutePath());
	//  }

	//  strBinName = dir.absoluteFilePath(strBinName);
	string strBinName = strPanelID;
	strBinName.replace(strBinName.find(".csv"), 4, ".bin");
	
	return strBinName;
}
//CompMat* CreateCompMat(int compNum, int* compGrayList,
//	int channels, int width, int height,
//	int burnWidth, int burnHeight)
//{
//	//CompMat CM;
//	CompMat *pCM = new CompMat();
//	pCM->compNum = compNum;
//
//	for (int i = 0; i < compNum; i++) {
//		pCM->compGrayList[i] = *compGrayList++;
//	}
//	pCM->channels = channels;
//	pCM->width = width;
//	pCM->height = height;
//	pCM->burnWidth = burnWidth;
//	pCM->burnHeight = burnHeight;
//	pCM->burnData = (float*)malloc(sizeof(float)*burnWidth*burnHeight*compNum * 3);
//
//	return pCM;
//
//}
//CompMat *Csv2bin::GenerateCompMat(BinParam binParam) {
//	int channels = binParam.channel;
//	int width = binParam.imageH;
//	int height = binParam.imageV;
//	int compNum = binParam.PlaneNum;
//	int compGrayList[10];
//	for (int i = 0; i < binParam.PlaneNum; i++) {
//		compGrayList[i] = binParam.Plane[i] * 4;
//	}
//
//	int burnWidth = binParam.H_total;
//	int burnHeight = binParam.V_total;
//
//	return CreateCompMat(compNum, compGrayList, channels, width, height,
//		burnWidth, burnHeight);
//}
