/**
 * @projectName   CSOT_EEAD_ALG2_DEMURA
 * @brief         Demura
 * @author        wxq/cxh
 * @date          2021-07-31 17:01
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#include "stdafx.h"
#include "VDformat.h"
#include <WINDOWS.H>
#include <iostream>
#include <fstream>
#include <cmath>
#include "tconbase.h"
#include "tconfactor.h"
#include "easylogging++.h"
using namespace std;

VD_FORMAT::VD_FORMAT() {}
VD_FORMAT::~VD_FORMAT() {}

int VD_FORMAT::ToBin(CompMat *compMat, string &strSaveName) {

  if (compMat == nullptr || strSaveName.empty()) {
    LOG(INFO) << "Demura compMat == nullptr or strSaveName empty";
    return 0;
  }
    return(ToBinNoDll(compMat, strSaveName)); 
    
}

int VD_FORMAT::CellInforToBin(PMicCompMat* compMat, string& strSaveName) {
    if (compMat == nullptr || strSaveName.empty()) {
        // Log4Error("Tobin error");
        LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
        return 0;
    }
    LOG(INFO) << "Start Save  VD format Cellinfor to:" << strSaveName.c_str();
    
    unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
    char* CellInfobuffer = (char*)malloc(sizeof(char*) * 96);
    if (CellInfobuffer && checksum_out) {
        *checksum_out = 0;
        for (int i = 0; i < 64;) {
            *checksum_out += (compMat->CellInfo[i + 1] << 8) + compMat->CellInfo[i];
            i = i + 2;
        }
        compMat->CellInfo[64] = *checksum_out & 0xFF;
        compMat->CellInfo[65] = (*checksum_out >> 8) & 0xFF;

        for (int i = 0; i < 24; i++) {
            CellInfobuffer[4 * i] = compMat->CellInfo[4 * i + 3];
            CellInfobuffer[4 * i + 1] = compMat->CellInfo[4 * i + 2];
            CellInfobuffer[4 * i + 2] = compMat->CellInfo[4 * i + 1];
            CellInfobuffer[4 * i + 3] = compMat->CellInfo[4 * i];
        }
        ofstream outfile1;
        // outfile.open(strSaveName, ios::out | ios::binary);
        outfile1.open(strSaveName, ios::_Nocreate | ios::binary);
        outfile1.seekp(compMat->VcomAddress - 0x1000, ios::beg);
        outfile1.write(CellInfobuffer, 96);
        outfile1.close();
        LOG(INFO) << "CellInfo code2bin finished!";
        if (CellInfobuffer != NULL) {
            free(CellInfobuffer);
            CellInfobuffer = NULL;
            LOG(INFO) << "free CellInfobuffer!";
        }
        FreeMallocInt(checksum_out);
        return 1;
    }
    else {
        LOG(INFO) << "CellInfo to bin fail,malloc fail!";
        return 0;
    }

}

int VD_FORMAT::PMicToBin(PMicCompMat* compMat, string& strSaveName) {

  if (compMat == nullptr || strSaveName.empty()) {

    LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
    return 0;
  }
  if (CellInforToBin(compMat, strSaveName) == 0)
      return 0;

  LOG(INFO) << "Start Save  VD format VCOM to:" << strSaveName.c_str();

  compMat->burnsize = 8;
  unsigned char *burnbuffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * compMat->burnsize);
  unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
  if (burnbuffer && checksum_out) 
  {
      burnbuffer[0] = 0x00;
      burnbuffer[1] = 0x40;
      burnbuffer[2] = 0x00;
      burnbuffer[3] = 0x05;
      burnbuffer[4] = 0x00;
      int vcom = (compMat->burnData[0] << 1) + 1;
      burnbuffer[5] = vcom & 0xFF;
      burnbuffer[6] = 0x00;
      burnbuffer[7] = 0x00;

      VD_Checksum(compMat->burnsize - 2, burnbuffer, checksum_out);
      burnbuffer[7] = *checksum_out;

      ofstream outfile;
      // outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(compMat->VcomAddress, ios::beg);
      for (int idx = 0; idx < compMat->burnsize;) {
          outfile << burnbuffer[idx + 2];
          outfile << burnbuffer[idx + 3];
          outfile << burnbuffer[idx + 0];
          outfile << burnbuffer[idx + 1];
          idx += 4;
      }
      outfile.close();
      LOG(INFO) << "VCOM code2bin finished!";
      if (burnbuffer != NULL) {
          free(burnbuffer);
          burnbuffer = NULL;
          LOG(INFO) << "free VCOM burnbuffer!";
      }
      FreeMallocInt(checksum_out);
      return 1;
  }
  else{
      LOG(INFO) << "VCOM code2bin fail,malloc fail!";
      return 0;
  }

}

int VD_FORMAT::GmToBin(GmCompMat* compMat, string& strSaveName) {

    LOG(INFO) << "Start Save VD format Gamma to:" << strSaveName.c_str();
    if (compMat == nullptr) {
        LOG(INFO) << "Gamma code2bin fail,compMat == nullptr!";
        return 0;
    }
    if (strSaveName.empty()) {
        LOG(INFO) << "Gamma code2bin fail,strSaveName empty!";
        return 0;
    }

    char* burnbuffer = (char*)malloc(sizeof(char*) * 6148);  // pow(2, compMat.GMInputBits) * 3 *compMat.GMOutputBits /8);
    if (burnbuffer) {
        switch (compMat->GmType) {
        case 1:
            compMat->lut_size = CreatVD60HzFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatVD60HzFromat!     lut_size:"<< compMat->lut_size;
            break;
        case 2:
            compMat->lut_size = CreatVD120HzFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatVD120HzFromat!    lut_size:" << compMat->lut_size;
            break;
        case 3:
            compMat->lut_size = CreatIQCFromat(*compMat, *burnbuffer);
            LOG(INFO) << "CreatIQCFromat!    lut_size:" << compMat->lut_size;
            break;
        default:
            compMat->lut_size = 0;
            LOG(INFO) << "Creat gamma Fromat err!   lut_size:" << compMat->lut_size;
            return 0;
            break;
        }

        ofstream outfile;
        // outfile.open(strSaveName, ios::out | ios::binary);
        outfile.open(strSaveName, ios::_Nocreate | ios::binary);
        outfile.seekp(compMat->Address, ios::beg);
        outfile.write(burnbuffer, compMat->lut_size);
        outfile.close();

        LOG(INFO) << "Gamma code2bin finished!";
        if (burnbuffer != NULL) {
            free(burnbuffer);
            burnbuffer = NULL;
            LOG(INFO) << "free Gamma burnbuffer!";
        }

        if (IQCheaderToBin(compMat, strSaveName) == 0)
            return 0;

        return 1;
    }
    else {
        LOG(INFO) << "Gamma code2bin fail,malloc fail!";
        return 0;
    }

    
}

int VD_FORMAT::CreatVD60HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out=0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    i += 2;
  }
  int bank_size = 0x1800 - 0x200 * 3;
  for (int i = 0; i < bank_size + 2; i++) {
    ptr[j++] = 0x00;
  }
  
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  //FreeMallocInt(checksum_out);
}
int VD_FORMAT::CreatVD120HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out = 0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
      i += 2;
  }
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
    i += 2;
  }
  for (int i = 0; i < 1024;) {
    if (i < 256) {
      checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

      checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
      checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    } else {
      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;

      checksum_out += ptr[j++] = 0x00;
      checksum_out += ptr[j++] = 0x00;
    }
    i += 2;
  }
  checksum_out += ptr[j++] = 0x00;
  checksum_out += ptr[j++] = 0x00;
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  // FreeMallocInt(checksum_out);
}
int VD_FORMAT::CreatIQCFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int checksum_out = 0;
  unsigned int data_len = 0;
  unsigned int j = 0;
  //4 bytes dummy
  ptr[j++] = 0;
  ptr[j++] = 0;
  ptr[j++] = 0;
  ptr[j++] = 0;
  //R bolck
  for (int i = 0; i < 63;) {
       ptr[j++] = (compMat.burnData_R[4 * (i + 1)] >> 8 & 0x0F);
       ptr[j++] = (compMat.burnData_R[4 * (i + 1)] & 0xFF);
       checksum_out += compMat.burnData_R[4 * (i + 1)];

       ptr[j++] = (compMat.burnData_R[4 * i] >> 8 & 0x0F);
       ptr[j++] = (compMat.burnData_R[4 * i] & 0xFF);
       checksum_out += compMat.burnData_R[4 * i];
       i += 2;
  }
  ptr[j++] = (compMat.burnData_R[254] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_R[254] & 0xFF);
  checksum_out += compMat.burnData_R[254];

  ptr[j++] = (compMat.burnData_R[253] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_R[253] & 0xFF);
  checksum_out += compMat.burnData_R[253];

  ptr[j++] = 0x07;
  ptr[j++] = 0xFF;
  checksum_out += 0x7FF;

  ptr[j++] = (compMat.burnData_R[255] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_R[255] & 0xFF);
  checksum_out += compMat.burnData_R[255];
  //G bolck
  for (int i = 0; i < 63;) {
      ptr[j++] = (compMat.burnData_G[4 * (i + 1)] >> 8 & 0x0F);
      ptr[j++] = (compMat.burnData_G[4 * (i + 1)] & 0xFF);
      checksum_out += compMat.burnData_G[4 * (i + 1)];

      ptr[j++] = (compMat.burnData_G[4 * i] >> 8 & 0x0F);
      ptr[j++] = (compMat.burnData_G[4 * i] & 0xFF);
      checksum_out += compMat.burnData_G[4 * i];
      i += 2;
  }
  ptr[j++] = (compMat.burnData_G[254] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_G[254] & 0xFF);
  checksum_out += compMat.burnData_G[254];

  ptr[j++] = (compMat.burnData_G[253] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_G[253] & 0xFF);
  checksum_out += compMat.burnData_G[253];

  ptr[j++] = 0x07;
  ptr[j++] = 0xFF;
  checksum_out += 0x7FF;

  ptr[j++] = (compMat.burnData_G[255] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_G[255] & 0xFF);
  checksum_out += compMat.burnData_G[255];
  //B bolck
  for (int i = 0; i < 63;) {
      ptr[j++] = (compMat.burnData_B[4 * (i + 1)] >> 8 & 0x0F);
      ptr[j++] = (compMat.burnData_B[4 * (i + 1)] & 0xFF);
      checksum_out += compMat.burnData_B[4 * (i + 1)];

      ptr[j++] = (compMat.burnData_B[4 * i] >> 8 & 0x0F);
      ptr[j++] = (compMat.burnData_B[4 * i] & 0xFF);
      checksum_out += compMat.burnData_B[4 * i];
      i += 2;
  }
  ptr[j++] = (compMat.burnData_B[254] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_B[254] & 0xFF);
  checksum_out += compMat.burnData_B[254];

  ptr[j++] = (compMat.burnData_B[253] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_B[253] & 0xFF);
  checksum_out += compMat.burnData_B[253];

  ptr[j++] = 0x07;
  ptr[j++] = 0xFF;
  checksum_out += 0x7FF;

  ptr[j++] = (compMat.burnData_B[255] >> 8 & 0x0F);
  ptr[j++] = (compMat.burnData_B[255] & 0xFF);
  checksum_out += compMat.burnData_B[255];
  // 
  ptr[j++] = 0xFF;
  ptr[j++] = 0xFF;
  ptr[j++] = (checksum_out >> 8 & 0xFF);
  ptr[j++] = (checksum_out & 0xFF);
  return data_len = j;
  // FreeMallocInt(checksum_out);
}

int VD_FORMAT::IQCheaderToBin(GmCompMat* compMat, string& strSaveName) {

    LOG(INFO) << "Start Save IQC header to:" << strSaveName.c_str();
    if (compMat == nullptr) {
        LOG(INFO) << "IQC header to bin fail,compMat == nullptr!";
        return 0;
    }
    if (strSaveName.empty()) {
        LOG(INFO) << "IQC header to bin fail,strSaveName empty!";
        return 0;
    }
    VD_4k_IQCHeader header = Init_VD_4K_IQCheader();
    ofstream outfile;
    // outfile.open(strSaveName, ios::out | ios::binary);
    outfile.open(strSaveName, ios::_Nocreate | ios::binary);
    outfile.seekp(0x37B000, ios::beg);
    int length = sizeof(header);
    BYTE* p = new BYTE[length];
    memmove(p, &header, length);
    for (int idx = 0; idx < length; idx++) {
        outfile << p[idx];
    }

    char* buffer = (char*)malloc(sizeof(char*) * 0xB080);
    if (buffer) {
        memset(buffer, 0x00, 0xB080);
        outfile.seekp(0x37C000, ios::beg);
        outfile.write(buffer, 0xB080);

        outfile.seekp(0x37C003, ios::beg);
        outfile << char(0x05);
        outfile.seekp(0x37C01F, ios::beg);
        outfile << char(0x02);
        outfile.seekp(0x37C04D, ios::beg);
        outfile << char(0x01);
        outfile.seekp(0x37C055, ios::beg);
        outfile << char(0x01);
        outfile.seekp(0x37C05D, ios::beg);
        outfile << char(0x09);         //05+02+01+01=09

        outfile.seekp(0x37CB02, ios::beg);
        outfile << char(0xFF);
        outfile << char(0xFF);
        outfile.seekp(0x382500, ios::beg);
        outfile << char(0xFF);
        outfile << char(0xFF);
        outfile.seekp(0x385D9C, ios::beg);
        outfile << char(0xFF);
        outfile << char(0xFF);
        outfile.seekp(0x38707C, ios::beg);
        outfile << char(0xFF);
        outfile << char(0xFF);
        outfile.seekp(0x387082, ios::beg);
        outfile << char(0xFF);
        outfile << char(0xFC);

        outfile.seekp(0x389030, ios::beg);
        outfile << char(0xFF);
        outfile << char(0xE7);

        outfile.close();
        //if (buffer != nullptr) {
        //    free(buffer);
        //    buffer = nullptr;
        //}

        LOG(INFO) << "IQCHeader to bin finished!";
        if (buffer != NULL) {
            free(buffer);
            buffer = NULL;
            LOG(INFO) << "free IQCHeader burnbuffer!";
        }
        return 1;
    }
    else {
        LOG(INFO) << "IQCheader to bin fail,malloc fail!";
        return 0;
    }
        
}

int VD_FORMAT::ToBinNoDll(CompMat *compMat, string &strSaveName) {
  m_pCompMat = compMat;

  // Step 1 – Create source buffer
  DEMURA_SOURCE_BUFFER src;
  NoDll_CreateSrcBuffer(&src, compMat->channels * compMat->compNum,
                        compMat->burnWidth, compMat->burnHeight);
  // Step 2 – Read demura compensation data
  ReadDemuraCompensationData(src, *compMat);

  // Step 3 – Demura setting
  DEMURA_SETTING setting ;
  setting.total_tcon_count = 0;
  setting.table.h = compMat->burnWidth;
  setting.table.v = compMat->burnHeight;
  setting.scale_x = 8;
  setting.scale_y = 8;
  setting.node_num = compMat->channels * compMat->compNum;
  setting.plane_num = compMat->compNum;
  // Step 4 – Create table buffer
  DEMURA_TABLE_BUFFER tbls[2];
  NoDll_CreateTblBuffer(&tbls[0], setting, setting.total_tcon_count);

  // Step 5 – Convert demura table
  NoDll_TableGen(src, tbls[0], setting, setting.total_tcon_count);

  // Step 6 – Save demura table
  WriteBinFile(tbls, strSaveName);

  // Step 7 –Free source and table buffer

  if (tbls[0].buffer != NULL) {
	  free(tbls[0].buffer);
	  tbls[0].buffer = NULL;
      LOG(INFO) << "free demura burnbuffer!";
  }

  /*FreeMallocInt(tbls[0].checksum);
  FreeMallocInt(tbls[0].crc);*/
  Free3Darray(src.buffer);
  LOG(INFO) << "free demura databuffer!";
  return 1;
}

void VD_FORMAT::ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                          const CompMat &compMat) {
  int *ptr = &src.buffer[0][0][0];
  int nPlaneSize = compMat.burnHeight * compMat.burnWidth;
  //float R1, G1, B1, R2, G2, B2, R3, G3, B3;  //补偿值
  float RTemp[8];
  unsigned mag[8];
  float plane_max[8]={0,0,0,0,0,0,0,0},plane_min[8]={1020,1020,1020,1020,1020,1020,1020,1020},plane_offset[8]={0,0,0,0,0,0,0,0};
  float plane1_max=0,plane1_min=1020,plane1_offset=0;
  float plane2_max=0,plane2_min=1020,plane2_offset=0;
  float plane3_max=0,plane3_min=1020,plane3_offset=0;
  for (int i = 0; i < compMat.burnHeight; i++)
  for (int j = 0; j < compMat.burnWidth; j++) {
    // NovaTek: R1、R2、R3、G1、G2、G3、B1、B2、B3
    // compMat: R1、G1、B1、R2、G2、B2、R3、G3、B3
      for(int k=0; k<compMat.compNum;k++)
      {
            if(compMat.compGrayList[k] != 0)
            {
                RTemp[k] = compMat.burnData[nPlaneSize * k * 3 + compMat.burnWidth* i + j] - compMat.compGrayList[k];
                if(RTemp[k]>plane_max[k]){plane_max[k]=RTemp[k];}
                if(RTemp[k]<plane_min[k]){plane_min[k]=RTemp[k];}
            }
      }
//    R1 = compMat.burnData[nPlaneSize * 0 + compMat.burnWidth* i + j] - compMat.compGrayList[0];
//    if(R1>plane1_max){plane1_max=R1;}
//    if(R1<plane1_min){plane1_min=R1;}

//    R2 = compMat.burnData[nPlaneSize * 3 + compMat.burnWidth* i + j] - compMat.compGrayList[1];
//    if(R2>plane2_max){plane2_max=R2;}
//    if(R2<plane2_min){plane2_min=R2;}

//    R3 = compMat.burnData[nPlaneSize * 6 + compMat.burnWidth* i + j] - compMat.compGrayList[2];
//    if(R3>plane3_max){plane3_max=R3;}
//    if(R3<plane3_min){plane3_min=R3;}
  }
  for(int k=0; k<compMat.compNum;k++)
  {
        if(compMat.compGrayList[k] != 0)
        {
            mag[k] = floor((plane_max[k] - plane_min[k]) / 62 );
            if (mag[k] >= 2)
                mag[k] = 2;
            plane_offset[k] = (plane_max[k] + plane_min[k])* pow(2, 2) / 2;
            m_pCompMat->plane_mag[k]=mag[k];
            m_pCompMat->plane_offset[k]=plane_offset[k];

        }
  }
//  mag[0]=(plane1_max-plane1_min)/62-1;
//  plane1_offset=(plane1_max+plane1_min)/2;
//  m_pCompMat->plane_mag[0]=mag[0];
//  m_pCompMat->plane_offset[0]=plane1_offset;
//  mag[1]=(plane2_max-plane2_min)/62-1;
//  plane2_offset=(plane2_max+plane2_min)/2;
//  m_pCompMat->plane_mag[1]=mag[1];
//  m_pCompMat->plane_offset[1]=plane2_offset;
//  mag[2]=(plane3_max-plane3_min)/62-1;
//  plane3_offset=(plane3_max+plane3_min)/2;
//  m_pCompMat->plane_mag[2]=mag[2];
//  m_pCompMat->plane_offset[2]=plane3_offset;

  for (int i = 0; i < compMat.burnHeight; i++)
  for (int j = 0; j < compMat.burnWidth+3; j++) {
    // NovaTek: R1、R2、R3、G1、G2、G3、B1、B2、B3
    // compMat: R1、G1、B1、R2、G2、B2、R3、G3、B3
      if(j<compMat.burnWidth){
        for(int k=0; k<compMat.compNum;k++)
        {
            if(compMat.compGrayList[k] != 0)
            {
                RTemp[k] = compMat.burnData[nPlaneSize * k * 3 + compMat.burnWidth* i + j] - compMat.compGrayList[k];
                //ptr[(compMat.compNum*i+k) * (compMat.burnWidth+3) + j] = QString::asprintf("%.0f", RTemp[k] * pow(2,2-mag[0])-plane_offset[k]).toInt();
                ptr[(compMat.compNum * i + k) * (compMat.burnWidth + 3) + j] =round((RTemp[k] * pow(2,2) - plane_offset[k])/ pow(2,mag[k]));
                if (ptr[(compMat.compNum * i + k) * (compMat.burnWidth + 3) + j] < -127)
                    ptr[(compMat.compNum * i + k) * (compMat.burnWidth + 3) + j] = -127;
                else if(ptr[(compMat.compNum * i + k) * (compMat.burnWidth + 3) + j]>127)
                    ptr[(compMat.compNum * i + k) * (compMat.burnWidth + 3) + j] = 127;

            }
        }
      }
      else
      {
          for(int k=0; k<compMat.compNum;k++)
          {
              ptr[(compMat.compNum*i+k) * (compMat.burnWidth+3) + j] = 0;
          }
      }
//    if(j<compMat.burnWidth){
//    R1 = compMat.burnData[nPlaneSize * 0 + compMat.burnWidth* i + j] - compMat.compGrayList[0];
//    R2 = compMat.burnData[nPlaneSize * 3 + compMat.burnWidth* i + j] - compMat.compGrayList[1];
//    R3 = compMat.burnData[nPlaneSize * 6 + compMat.burnWidth* i + j] - compMat.compGrayList[2];
//    ptr[3*i * (compMat.burnWidth+3) + j] = QString::asprintf("%.0f", R1 * qPow(2,2-mag[0])-plane1_offset).toInt();  // for 四舍五入
//    ptr[(3*i+1) * (compMat.burnWidth+3) + j] = QString::asprintf("%.0f", R2 * qPow(2,2-mag[1])-plane2_offset).toInt();
//    ptr[(3*i+2) * (compMat.burnWidth+3) + j] = QString::asprintf("%.0f", R3 * qPow(2,2-mag[2])-plane3_offset).toInt();

//    }
//    else{
//        ptr[3*i * (compMat.burnWidth+3) + j] = 0;  // for 四舍五入
//        ptr[(3*i+1) * (compMat.burnWidth+3) + j] = 0;
//        ptr[(3*i+2) * (compMat.burnWidth+3) + j] = 0;
//    }

  }
}




void VD_FORMAT::NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src,
                                     int tbl_num, int tbl_h, int tbl_v) {
  int real_tbl_h=tbl_h + 3;
  demura_src->buffer = Create3Darray(tbl_num, tbl_v, real_tbl_h);
  demura_src->tbl_h = real_tbl_h;
  demura_src->tbl_v = tbl_v;
  demura_src->tbl_num = tbl_num;
  demura_src->size = tbl_num * real_tbl_h * tbl_v;
}


void VD_FORMAT::NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                                     DEMURA_SETTING setting,
                                     unsigned char tcon_idx, bool shift_en,
                                     bool dummy_en) {
 /* Q_UNUSED(shift_en)
  Q_UNUSED(dummy_en)*/
  int Table_H;
  switch (tcon_idx) {
    case 0: {
      Table_H = setting.table.h;
      break;
    }
    default: {
      Table_H = setting.table.h / 2 + 1;
      break;
    }
  }

  // TCON demura table description.pdf相关公式
  int Table_V = setting.table.v;
  int nNodeNumber = setting.node_num;
  int Block_H = setting.scale_x;
  //double tmp = (Table_H + (Block_H / 8)) * Table_V * nNodeNumber * 12 / 256;
  int Table_SIZE = (Table_H + 3) * Table_V * nNodeNumber;

  demura_tbl->buffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * Table_SIZE);
  demura_tbl->size = Table_SIZE;
}

void VD_FORMAT::NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                              DEMURA_TABLE_BUFFER &demura_tbl,
                              DEMURA_SETTING setting, unsigned char tcon_idx) {
  //demura_tbl.buffer =
  //    (unsigned char *)malloc(sizeof(unsigned char *) * demura_tbl.size);
  //demura_tbl.crc = (unsigned int *)malloc(sizeof(unsigned int *));
  //demura_tbl.checksum = (unsigned int *)malloc(sizeof(unsigned int *));

  switch (tcon_idx) {
    case 0: {
      int begin = 0;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 1: {
      int begin = 0;
      int end = setting.table.h / 2 + 1;//?
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
    case 2: {
      int begin = setting.table.h / 2 ;
      int end = setting.table.h;
      TableGenLoop(demura_src, demura_tbl, setting, begin, end);
      break;
    }
  }

//  NvtDemura_CalCrcChecksum(demura_tbl.buffer, demura_tbl.size, demura_tbl.crc,
//                           demura_tbl.checksum);
}

void VD_FORMAT::TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                            DEMURA_TABLE_BUFFER &demura_tbl,
                            DEMURA_SETTING setting, const int colBegin,
                            const int colEnd) {
  //将两个12bits，存储在三个byte中，高8bits+（合并：低4bits+高4bits）+低8bits
  int cnt = 0;
  bool bEven = true;
  int nRow = setting.table.v;//?
  int nCol = setting.table.h+3;//?
  int nNodeNum = setting.node_num;
  int *p = &demura_src.buffer[0][0][0];
  int cnt2 = 0;
  int nPlaneSize = nRow * nCol * setting.plane_num;
  for (int k = 0; k < nPlaneSize;) {
      demura_tbl.buffer[cnt+3] = p[k++];
      demura_tbl.buffer[cnt+2] = p[k++];
      demura_tbl.buffer[cnt+1] = p[k++];
      demura_tbl.buffer[cnt] = p[k++];
      //k=k+4;
      cnt=cnt+4;
  }


//  for (int k = 0; k < nRow; k++) {
//    for (int i = colBegin; i < colEnd; i++) {
//      for (int j = 0; j < 3; j++) {
//        cnt2++;
//        int tmp = p[k * nCol * 3 + i * 3 + j];  //补偿值
//        if (bEven) {
//          nFull8bits = (tmp >> 4) & 0xFF;
//          nMerge8bits = (tmp << 4) & 0xF0;
//          bEven = false;
//        } else {
//          nFull8bits = tmp & 0xFF;
//          nMerge8bits += (tmp >> 8) & 0x0F;
//          demura_tbl.buffer[cnt++] = nMerge8bits;  //存储合并的8bits
//          bEven = true;
//        }
//        demura_tbl.buffer[cnt++] = nFull8bits;
//      }
//    }
//  }
//  demura_tbl.buffer[cnt++] = 0;
//  demura_tbl.buffer[cnt++] = 0;
//  demura_tbl.buffer[cnt++] = 0;
//  demura_tbl.buffer[cnt++] = 0;
}

void VD_FORMAT::WriteBinFile(const DEMURA_TABLE_BUFFER tbls[],
                            string &strSaveName) {

    LOG(INFO)<<"Save VD format Demura to: "<<strSaveName.c_str();
  
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  if (checksum_out) {
      ofstream outfile;
      //outfile.open(strSaveName, ios::out | ios::binary);
      outfile.open(strSaveName, ios::_Nocreate | ios::binary);
      outfile.seekp(m_pCompMat->DemuraAddress, ios::beg);
      // write header
      VD_4k_Header header = Init_VD_4K_header();
      VD_Checksum(tbls[0].size, tbls[0].buffer, checksum_out);
      //header.LUT_CHECKSUM = _byteswap_ushort(*checksum_out);
      m_pCompMat->lut_checksum = *checksum_out;
      LOG(INFO) << "Demura CRC/checksunm is: " << *checksum_out;

      SetHeader(header);
      //WriteHeader(qs, header);
      WriteHeader(outfile, header);

      // Write 1bin, 2table(2 TCONs)
      for (int nTCONindex = 0; nTCONindex < 1; nTCONindex++) {
          DEMURA_TABLE_BUFFER tbl = tbls[nTCONindex];

          for (int idx = 0; idx < tbl.size; idx++) {
              outfile << tbl.buffer[idx];
          }
      }

      outfile.close();
      LOG(INFO) << "Demura CSV2bin finished!";
      FreeMallocInt(checksum_out);
  }

}

void VD_FORMAT::SetHeader(VD_4k_Header &header) {
  if (m_pCompMat == nullptr) {
    return;
  }

  header.PLANE_NUM = _byteswap_ushort(m_pCompMat->compNum);
  header.LUT_SIZE= _byteswap_ulong((m_pCompMat->burnWidth+3)*m_pCompMat->burnHeight*m_pCompMat->compNum);
  header.LUT_CHECKSUM =_byteswap_ushort(m_pCompMat->lut_checksum);

  header.LOW_LIMIT_R = _byteswap_ushort(m_pCompMat->lower_Bound);
  header.LOW_LIMIT_G = _byteswap_ushort(m_pCompMat->lower_Bound);
  header.LOW_LIMIT_B = _byteswap_ushort(m_pCompMat->lower_Bound);

  header.HIGH_LIMIT_R = _byteswap_ushort(m_pCompMat->high_Bound);
  header.HIGH_LIMIT_G = _byteswap_ushort(m_pCompMat->high_Bound);
  header.HIGH_LIMIT_B = _byteswap_ushort(m_pCompMat->high_Bound);

  header.R_PLANE0 = _byteswap_ushort(m_pCompMat->compGrayList[0]);
  header.R_PLANE1 = _byteswap_ushort(m_pCompMat->compGrayList[1]);
  header.R_PLANE2 = _byteswap_ushort(m_pCompMat->compGrayList[2]);
  header.R_PLANE3 = _byteswap_ushort(m_pCompMat->compGrayList[3]);
  header.R_PLANE4 = _byteswap_ushort(m_pCompMat->compGrayList[4]);
  header.R_PLANE5 = _byteswap_ushort(m_pCompMat->compGrayList[5]);
  header.R_PLANE6 = _byteswap_ushort(m_pCompMat->compGrayList[6]);
  header.R_PLANE7 = _byteswap_ushort(m_pCompMat->compGrayList[7]);

  header.PLANE_R_MAG = _byteswap_ulong(m_pCompMat->plane_mag[0] + (m_pCompMat->plane_mag[1]<<4) +
          (m_pCompMat->plane_mag[2]<<8) + (m_pCompMat->plane_mag[3]<<12)+ (m_pCompMat->plane_mag[4]<<16) +
          (m_pCompMat->plane_mag[5]<<20) + (m_pCompMat->plane_mag[6]<<24)+ (m_pCompMat->plane_mag[7]<<28));

  header.PLANE0_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[0]&0x0FFF);
  header.PLANE1_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[1]&0x0FFF);
  header.PLANE2_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[2]&0x0FFF);
  header.PLANE3_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[3]&0x0FFF);
  header.PLANE4_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[4]&0x0FFF);
  header.PLANE5_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[5]&0x0FFF);
  header.PLANE6_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[6]&0x0FFF);
  header.PLANE7_R_OFFSET = _byteswap_ushort(m_pCompMat->plane_offset[7]&0x0FFF);

  header.R_LIMIT_COEF=_byteswap_ushort(round(65536.0/(m_pCompMat->compGrayList[0]- m_pCompMat->lower_Bound)));
  unsigned int COEF[8]={0,0,0,0,0,0,0,0};
  for(int i=0;i<8;i++){
      if(m_pCompMat->compGrayList[i+1]!=0)
          COEF[i]=_byteswap_ushort(round(65536.0/(m_pCompMat->compGrayList[i+1]- m_pCompMat->compGrayList[i])));
      else
       {   COEF[i]=_byteswap_ushort(round(65536.0/(m_pCompMat->high_Bound - m_pCompMat->compGrayList[i])));
          break;}
    }
  header.PLANE_R0_COEF = COEF[0];
  header.PLANE_R1_COEF = COEF[1];
  header.PLANE_R2_COEF = COEF[2];
  header.PLANE_R3_COEF = COEF[3];
  header.PLANE_R4_COEF = COEF[4];
  header.PLANE_R5_COEF = COEF[5];
  header.PLANE_R6_COEF = COEF[6];
  header.PLANE_R7_COEF = COEF[7];


  unsigned char *ptr = (unsigned char *)&header + 32;
  unsigned int size = sizeof(header) - 32;
//  unsigned int *crc_out = (unsigned int *)malloc(sizeof(unsigned int *));
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
  //NvtDemura_CalCrcChecksum(ptr, size, crc_out, checksum_out);
  VD_Checksum(size,ptr,checksum_out);
  header.REG_CHECKSUM = _byteswap_ushort(*checksum_out);

  FreeMallocInt(checksum_out);
  //Log4Info(QString("VD_4k_Header, Checksum = 0x%1")
  //             .arg(QString::number(*checksum_out, 16)));
}

void VD_FORMAT::WriteHeader(ofstream &qs, VD_4k_Header &header) {
  //QByteArray byte = StructToByte(header);
  //qs.writeRawData(byte, sizeof(VD_4k_Header));

  int length = sizeof(header);
  BYTE *p = new BYTE[length];
  memmove(p, &header, length);
  for (int idx = 0; idx < length; idx++) {
	  qs << p[idx];
  }
}

VD_4k_Header VD_FORMAT::Init_VD_4K_header() {
  VD_4k_Header header;

  header.REG_SIZE= _byteswap_ulong(0x00000200);
  header.RESERVED00 = 0;
  header.REG_CHECKSUM =_byteswap_ushort(0xCDCD);
  header.LUT_SIZE = _byteswap_ulong(0x00060114);   //484x271x3
  header.RESERVED01 = 0;
  header.LUT_CHECKSUM =_byteswap_ushort(0xCDCD);

  header.RESERVED02 = 0;
  header.RESERVED03 = 0;
  header.RESERVED04 = 0;
  header.RESERVED05 = 0;

  header.FIX_DATA0 = _byteswap_ulong(0x00000B15);      //00 00 0B 15
  header.FIX_DATA1 = _byteswap_ulong(0x0000002F);      //00 00 00 2F

  header.RESERVED06 = 0;
  header.RESERVED07 = 0;
  header.RESERVED08 = 0;
  header.RESERVED09 = 0;
  header.RESERVED10 = 0;
  header.RESERVED11 = 0;
  header.RESERVED12 = 0;
  header.RESERVED13 = 0;
  header.RESERVED14 = 0;
  header.RESERVED15 = 0;
  header.RESERVED16 = 0;
  header.RESERVED17 = 0;
  header.RESERVED18 = 0;
  header.RESERVED19 = 0;


  header.BLOCK_SIZE = _byteswap_ushort(5);  //5:8x8 6:8x16  10:16x16
  header.DEMURA_ENABLE = _byteswap_ushort(1);
  header.PLANE_NUM = _byteswap_ushort(3);
  header.FIX_DATA2 = 0;  //00
  header.LOW_LIMIT_G = _byteswap_ushort(12);
  header.LOW_LIMIT_R = _byteswap_ushort(12);
  header.R_PLANE0 = _byteswap_ushort(100);
  header.LOW_LIMIT_B = _byteswap_ushort(12);
  header.R_PLANE2 = _byteswap_ushort(900);
  header.R_PLANE1 = _byteswap_ushort(0);
  header.R_PLANE4 = _byteswap_ushort(0);
  header.R_PLANE3 = _byteswap_ushort(0);
  header.R_PLANE6 = _byteswap_ushort(0);
  header.R_PLANE5 = _byteswap_ushort(0);
  header.G_PLANE0 = _byteswap_ushort(0);
  header.R_PLANE7 = _byteswap_ushort(0);

  header.G_PLANE2 = _byteswap_ushort(0);
  header.G_PLANE1 = _byteswap_ushort(0);
  header.G_PLANE4 = _byteswap_ushort(0);
  header.G_PLANE3 = _byteswap_ushort(0);
  header.G_PLANE6 = _byteswap_ushort(0);
  header.G_PLANE5 = _byteswap_ushort(0);
  header.B_PLANE0 = _byteswap_ushort(0);
  header.G_PLANE7 = _byteswap_ushort(0);

  header.B_PLANE2 = _byteswap_ushort(0);
  header.B_PLANE1 = _byteswap_ushort(0);
  header.B_PLANE4 = _byteswap_ushort(0);
  header.B_PLANE3 = _byteswap_ushort(0);
  header.B_PLANE6 = _byteswap_ushort(0);
  header.B_PLANE5 = _byteswap_ushort(0);
  header.HIGH_LIMIT_R = _byteswap_ushort(1020);
  header.B_PLANE7 = _byteswap_ushort(0);

  header.HIGH_LIMIT_B = _byteswap_ushort(1020);
  header.HIGH_LIMIT_G = _byteswap_ushort(1020);

  header.PLANE_R_MAG = _byteswap_ulong(0);
  header.PLANE_G_MAG = _byteswap_ulong(0);
  header.PLANE_B_MAG = _byteswap_ulong(0);

  header.PLANE1_R_OFFSET = 0;
  header.PLANE0_R_OFFSET = 0;
  header.PLANE3_R_OFFSET = 0;
  header.PLANE2_R_OFFSET = 0;
  header.PLANE5_R_OFFSET = 0;
  header.PLANE4_R_OFFSET = 0;
  header.PLANE7_R_OFFSET = 0;
  header.PLANE6_R_OFFSET = 0;

  header.PLANE1_G_OFFSET = 0;
  header.PLANE0_G_OFFSET = 0;
  header.PLANE3_G_OFFSET = 0;
  header.PLANE2_G_OFFSET = 0;
  header.PLANE5_G_OFFSET = 0;
  header.PLANE4_G_OFFSET = 0;
  header.PLANE7_G_OFFSET = 0;
  header.PLANE6_G_OFFSET = 0;

  header.PLANE1_B_OFFSET = 0;
  header.PLANE0_B_OFFSET = 0;
  header.PLANE3_B_OFFSET = 0;
  header.PLANE2_B_OFFSET = 0;
  header.PLANE5_B_OFFSET = 0;
  header.PLANE4_B_OFFSET = 0;
  header.PLANE7_B_OFFSET = 0;
  header.PLANE6_B_OFFSET = 0;

  header.G_LIMIT_COEF = 0;
  header.R_LIMIT_COEF = _byteswap_ushort(0x02E9);  //65536/(plane0- low limit)
  header.PLANE_R0_COEF = _byteswap_ushort(0x01D4);   //65536/(plane1- plane0)
  header.B_LIMIT_COEF = 0;

  header.PLANE_R2_COEF = _byteswap_ushort(0x0222);   //65536/(high limit- plane2)
  header.PLANE_R1_COEF = _byteswap_ushort(0x0063);   //65536/(plane2- plane1)
  header.PLANE_R4_COEF = 0;
  header.PLANE_R3_COEF = 0;
  header.PLANE_R6_COEF = 0;
  header.PLANE_R5_COEF = 0;
  header.PLANE_G0_COEF = 0;
  header.PLANE_R7_COEF = 0;

  header.PLANE_G2_COEF = 0;
  header.PLANE_G1_COEF = 0;
  header.PLANE_G4_COEF = 0;
  header.PLANE_G3_COEF = 0;
  header.PLANE_G6_COEF = 0;
  header.PLANE_G5_COEF = 0;
  header.PLANE_B0_COEF = 0;
  header.PLANE_G7_COEF = 0;

  header.PLANE_B2_COEF = 0;
  header.PLANE_B1_COEF = 0;
  header.PLANE_B4_COEF = 0;
  header.PLANE_B3_COEF = 0;
  header.PLANE_B6_COEF = 0;
  header.PLANE_B5_COEF = 0;
  header.RESERVED20 = 0;
  header.PLANE_B7_COEF = 0;

  header.RESERVED21 = 0;
  header.DEMURA_nRESET = _byteswap_ushort(0x0001);    //fix 0x00 01
  header.RESERVED22 = 0;
  header.H_ACTIVE = _byteswap_ushort(3840);
  header.DEMURA_3D_ON = _byteswap_ushort(0x0001);     //fix 0x00 01
  header.V_START_POSITION = _byteswap_ushort(0x0010);//fix 0x00 40
  header.V_ACTIVE = _byteswap_ushort(2160);

  header.RESERVED23 = 0;
  header.H_POSITION = _byteswap_ushort(3840);
  header.RESERVED24 = 0;
  header.V_POSITION = _byteswap_ushort(2160);
  header.RESERVED25 = 0;
  header.FIX_DATA3 = _byteswap_ulong(0x00B40051);  //00 B4 00 51
  header.RESERVED26 = 0;
  header.RESERVED27 = 0;

  header.FIX_DATA4 = _byteswap_ulong(0x18100000);  //18 10 00 00
  header.FIX_DATA5 = _byteswap_ulong(0x80604020);  //C0 80 40 20
  header.FIX_DATA6 = _byteswap_ulong(0xFFE0C0A0);  //00 00 00 00
  header.FIX_DATA7 = _byteswap_ulong(0x00000000);  //0F FF 00 00
  header.FIX_DATA8 = _byteswap_ulong(0x00000000);  //00 00 0F FF


  header.RESERVED28 = 0;
  header.RESERVED29 = 0;
  header.RESERVED30 = 0;
  header.RESERVED31 = 0;
  header.RESERVED32 = 0;
  header.RESERVED33 = 0;
  header.RESERVED34 = 0;
  header.RESERVED35 = 0;
  header.RESERVED36 = 0;
  header.RESERVED37 = 0;
  header.RESERVED38 = 0;
  header.RESERVED39 = 0;
  header.RESERVED40 = 0;
  header.RESERVED41 = 0;
  header.RESERVED42 = 0;
  header.RESERVED43 = 0;
  header.RESERVED44 = 0;
  header.RESERVED45 = 0;
  header.RESERVED46 = 0;
  header.RESERVED47 = 0;
  header.RESERVED48 = 0;
  header.RESERVED49 = 0;
  header.RESERVED50 = 0;
  header.RESERVED51 = 0;
  header.RESERVED52 = 0;
  header.RESERVED53 = 0;
  header.RESERVED54 = 0;
  header.RESERVED55 = 0;
  header.RESERVED56 = 0;
  header.RESERVED57 = 0;
  header.RESERVED58 = 0;
  header.RESERVED59 = 0;
  header.RESERVED60 = 0;
  header.RESERVED61 = 0;
  header.RESERVED62 = 0;
  header.RESERVED63 = 0;
  header.RESERVED64 = 0;
  header.RESERVED65 = 0;
  header.RESERVED66 = 0;
  header.RESERVED67 = 0;
  header.RESERVED68 = 0;
  header.RESERVED69 = 0;
  header.RESERVED70 = 0;
  header.RESERVED71 = 0;
  header.RESERVED72 = 0;
  header.RESERVED73 = 0;
  header.RESERVED74 = 0;
  header.RESERVED75 = 0;
  header.RESERVED76 = 0;
  header.RESERVED77 = 0;
  header.RESERVED78 = 0;

  return header;
}
VD_4k_IQCHeader VD_FORMAT::Init_VD_4K_IQCheader() {
    VD_4k_IQCHeader IQCheader;
    IQCheader.FIX_DATA0 = _byteswap_ulong(0x00001000);      
    IQCheader.FIX_DATA1 = _byteswap_ulong(0x00E68000);      
    IQCheader.FIX_DATA2 = _byteswap_ulong(0x0000002F);      
    IQCheader.FIX_DATA3 = _byteswap_ulong(0x00000000);      
    IQCheader.FIX_DATA4 = _byteswap_ulong(0x00001060);      
    IQCheader.FIX_DATA5 = _byteswap_ulong(0x00E68424);
    IQCheader.FIX_DATA6 = _byteswap_ulong(0x00005810);
    IQCheader.FIX_DATA7 = _byteswap_ulong(0x00000000);
    IQCheader.FIX_DATA8 = _byteswap_ulong(0x0000D000);
    IQCheader.FIX_DATA9 = _byteswap_ulong(0x00E680BC);
    IQCheader.FIX_DATA10 = _byteswap_ulong(0x00000001);
    IQCheader.FIX_DATA11 = _byteswap_ulong(0x00000000);
    IQCheader.FIX_DATA12 = _byteswap_ulong(0x0000D004);
    IQCheader.FIX_DATA13 = _byteswap_ulong(0x00E680F0);
    IQCheader.FIX_DATA14 = _byteswap_ulong(0x000000CC);
    IQCheader.FIX_DATA15 = _byteswap_ulong(0x00000000);

    return IQCheader;
}