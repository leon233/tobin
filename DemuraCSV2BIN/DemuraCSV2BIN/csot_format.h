#ifndef CSOT_FORMAT_H
#define CSOT_FORMAT_H


#include "tconfactor.h"
#include "tconbase.h"
typedef struct CSOTformat_Header {
  unsigned int header : 32;
  //QString name ;
  unsigned int name1: 8;
  unsigned int name2: 8;
  unsigned int name3: 8;
  unsigned int name4: 8;
  unsigned int name5: 8;
  unsigned int name6: 8;
  unsigned int name7: 8;
  unsigned int name8 : 8;
  unsigned int name9 : 8;
  unsigned int name10 : 8;
  unsigned int name11 : 8;
  unsigned int name12 : 8;
  unsigned int name13 : 8;
  unsigned int name14 : 8;

  unsigned int version : 16;
  unsigned int sectionNum : 16;
  unsigned int RESERVED00 : 16;
  unsigned int totalSize : 32;
  unsigned int FileTotalCRC : 16;
  unsigned int FileHeaderCRC : 16;

  unsigned int sectionTypeA : 32;
  unsigned int Paraoffset : 32;
  unsigned int Parasize : 32;
  unsigned int ParaSectionCRC : 16;
  unsigned int RESERVED01 : 16;

  unsigned int sectionTypeB : 32;
  unsigned int LUToffset : 32;
  unsigned int LUTsize : 32;
  unsigned int LUTSectionCRC : 16;
  unsigned int RESERVED02 : 16;

  unsigned int enDemuraMode : 8;
  unsigned int PlaneNUm : 8;
  unsigned int HblockSize : 8;
  unsigned int VblockSize : 8;
  unsigned int HLutNum : 16;
  unsigned int VLutNum : 16;
  unsigned int IntBitWidth : 8;
  unsigned int DecimalsBitWidth : 8;
  unsigned int TwoChipEN : 8;
  unsigned int CompressEN : 8;
  unsigned int ComValGainR : 16;
  unsigned int ComValGainG : 16;
  unsigned int ComValGainB : 16;
  unsigned int RESERVED03 : 16;
  unsigned int ComValOffsetR : 16;
  unsigned int ComValOffsetG : 16;
  unsigned int ComValOffsetB : 16;
  unsigned int RESERVED04 : 16;
  unsigned int BlackLimitR : 16;
  unsigned int BlackLimitG : 16;
  unsigned int BlackLimitB : 16;
  unsigned int RESERVED05 : 16;
  unsigned int WhiteLimitR : 16;
  unsigned int WhiteLimitG : 16;
  unsigned int WhiteLimitB : 16;
  unsigned int RESERVED06 : 16;
  unsigned int Plane1LevelR : 16;
  unsigned int Plane2LevelR : 16;
  unsigned int Plane3LevelR : 16;
  unsigned int Plane4LevelR : 16;
  unsigned int Plane5LevelR : 16;
  unsigned int Plane6LevelR : 16;
  unsigned int Plane7LevelR : 16;
  unsigned int Plane8LevelR : 16;
  unsigned int Plane9LevelR : 16;
  unsigned int Plane1LevelG : 16;
  unsigned int Plane2LevelG : 16;
  unsigned int Plane3LevelG : 16;
  unsigned int Plane4LevelG : 16;
  unsigned int Plane5LevelG : 16;
  unsigned int Plane6LevelG : 16;
  unsigned int Plane7LevelG : 16;
  unsigned int Plane8LevelG : 16;
  unsigned int Plane9LevelG : 16;
  unsigned int Plane1LevelB : 16;
  unsigned int Plane2LevelB : 16;
  unsigned int Plane3LevelB : 16;
  unsigned int Plane4LevelB : 16;
  unsigned int Plane5LevelB : 16;
  unsigned int Plane6LevelB : 16;
  unsigned int Plane7LevelB : 16;
  unsigned int Plane8LevelB : 16;
  unsigned int Plane9LevelB : 16;
  unsigned int RESERVED07 : 16;
  unsigned int RESERVED08 : 16;
  unsigned int RESERVED09 : 16;
  unsigned int RESERVED10 : 16;
  unsigned int RESERVED11 : 16;
  unsigned int RESERVED12 : 16;
  unsigned int RESERVED13 : 16;
  unsigned int RESERVED14 : 16;
  unsigned int RESERVED15 : 16;
  unsigned int PlaneB1SlopeR : 16;
  unsigned int Plane12SlopeR : 16;
  unsigned int Plane23SlopeR : 16;
  unsigned int Plane34SlopeR : 16;
  unsigned int Plane45SlopeR : 16;
  unsigned int Plane56SlopeR : 16;
  unsigned int Plane67SlopeR : 16;
  unsigned int Plane78SlopeR : 16;
  unsigned int Plane89SlopeR : 16;
  unsigned int Plane9WSlopeR : 16;

  unsigned int PlaneB1SlopeG : 16;
  unsigned int Plane12SlopeG : 16;
  unsigned int Plane23SlopeG : 16;
  unsigned int Plane34SlopeG : 16;
  unsigned int Plane45SlopeG : 16;
  unsigned int Plane56SlopeG : 16;
  unsigned int Plane67SlopeG : 16;
  unsigned int Plane78SlopeG : 16;
  unsigned int Plane89SlopeG : 16;
  unsigned int Plane9WSlopeG : 16;

  unsigned int PlaneB1SlopeB : 16;
  unsigned int Plane12SlopeB : 16;
  unsigned int Plane23SlopeB : 16;
  unsigned int Plane34SlopeB : 16;
  unsigned int Plane45SlopeB : 16;
  unsigned int Plane56SlopeB : 16;
  unsigned int Plane67SlopeB : 16;
  unsigned int Plane78SlopeB : 16;
  unsigned int Plane89SlopeB : 16;
  unsigned int Plane9WSlopeB : 16;

  unsigned int RESERVED16 : 16;
  unsigned int RESERVED17 : 16;
  unsigned int RESERVED18 : 16;
  unsigned int RESERVED19 : 16;
  unsigned int RESERVED20 : 16;
  unsigned int RESERVED21 : 16;
  unsigned int RESERVED22 : 16;
  unsigned int RESERVED23 : 16;
  unsigned int RESERVED24 : 16;
  unsigned int RESERVED25 : 16;
  unsigned int RESERVED26 : 16;
  unsigned int RESERVED27 : 16;
  unsigned int RESERVED28 : 16;
  unsigned int RESERVED29 : 16;
  unsigned int RESERVED30 : 16;
  unsigned int RESERVED31 : 16;


} CSOTformat_Header;

class CSOT_FORMAT : public TCONabstract {
 // Q_OBJECT
 public:
  explicit CSOT_FORMAT();
  ~CSOT_FORMAT() ;
  int ToBin(CompMat *compMat, string &strSaveName) override;
  int PMicToBin(PMicCompMat *compMat, string &strSaveName) override;
  int GmToBin(GmCompMat *compMat, string &strSaveName) override;

 private:
  int ToBinNoDll(CompMat *compMat, string &strSaveName);
  void WriteBinFile(const DEMURA_TABLE_BUFFER tbls[], string &strSaveName);
  void SetHeader(CSOTformat_Header &header);
  void WriteHeader(ofstream &qs, CSOTformat_Header &header);
  void NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src, int tbl_num,
                             int tbl_h, int tbl_v);
  void NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                             DEMURA_SETTING setting, unsigned char tcon_idx,
                             bool shift_en = false, bool dummy_en = false);
  void NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                      DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                      unsigned char tcon_idx);
  void TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                    DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                    const int begin, const int end);
  void ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                  const CompMat &compMat);
  int CreatCSOTFromat(const GmCompMat &compMat, char &burnbuffer);
  CSOTformat_Header Init_CSOTformat_Header();
 private:
  CompMat *m_pCompMat;
  //const int m_pLOWER_BOUND = 0;    //参考精测
  //const int m_pUPPER_BOUND = 1023;  //参考精测
};

#endif // CSOT_FORMAT_H
