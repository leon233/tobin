/**
 * @projectName   tobin
 * @brief         base
 * @author        cxh
 * @date          2021-08-05 19:54
 * Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
 * rights reserved.
 */
#include <string>

#ifndef TCONFACTOR_H
#define TCONFACTOR_H
using namespace std;

//class TCONabstract;

/***************************************************************
 *  @brief：AlgParam结构体初始化函数
 *  @param[1]     QString fileName      csv配置文件路径
 **************************************************************/

//typedef enum { NT71102, VD_65_8k_120HZ, NT71267, CSOT_UD_FORMAT, NT71733B, NT71265, CSOT_UD_FORMAT_DUAL, EU3, Oscar,Oscar_dual } TCONtype;
typedef enum { NO_DLL, USE_DLL } ToBinMethod;

struct CompMat;
struct PMicCompMat;
struct GmCompMat;

class TCONabstract   {
 
 public:
  ~TCONabstract();
  virtual int ToBin(CompMat *info, string &strSaveName) = 0;
  virtual int PMicToBin(PMicCompMat *info, string &strSaveName) = 0;
  virtual int GmToBin(GmCompMat *info, string &strSaveName) = 0;
  void setBUseDll(bool bUseDll);

 //signals:
 // void sigToBinMax(const int &);
 // void sigToBinValue(const int &);

 protected:
  bool m_bUseDll = false;
};

class TCONfactor {
 public:
  static TCONabstract* Create(const int nIndex);
};
#endif // TCONFACTOR_H
