#ifndef VDFORMAT_H
#define VDFORMAT_H
#include "tconfactor.h"
#include "tconbase.h"

typedef struct VD_4k_Header {
  unsigned int REG_SIZE : 32;
  unsigned int RESERVED00 : 16;
  unsigned int REG_CHECKSUM : 16;
  unsigned int LUT_SIZE : 32;
  unsigned int RESERVED01 : 16;
  unsigned int LUT_CHECKSUM : 16;

  unsigned int RESERVED02 : 32;
  unsigned int RESERVED03 : 32;
  unsigned int RESERVED04 : 32;
  unsigned int RESERVED05 : 32;

  unsigned int FIX_DATA0 : 32;      //00 00 0B 15
  unsigned int FIX_DATA1 : 32;      //00 00 00 2F

  unsigned int RESERVED06 : 32;
  unsigned int RESERVED07 : 32;
  unsigned int RESERVED08 : 32;
  unsigned int RESERVED09 : 32;
  unsigned int RESERVED10 : 32;
  unsigned int RESERVED11 : 32;
  unsigned int RESERVED12 : 32;
  unsigned int RESERVED13 : 32;
  unsigned int RESERVED14 : 32;
  unsigned int RESERVED15 : 32;
  unsigned int RESERVED16 : 32;
  unsigned int RESERVED17 : 32;
  unsigned int RESERVED18 : 32;
  unsigned int RESERVED19 : 32;


  unsigned int BLOCK_SIZE : 16;
  unsigned int DEMURA_ENABLE : 16;
  unsigned int PLANE_NUM : 16;
  unsigned int FIX_DATA2 : 16;  //00
  unsigned int LOW_LIMIT_G : 16;
  unsigned int LOW_LIMIT_R : 16;
  unsigned int R_PLANE0 : 16;
  unsigned int LOW_LIMIT_B : 16;
  unsigned int R_PLANE2 : 16;
  unsigned int R_PLANE1 : 16;
  unsigned int R_PLANE4 : 16;
  unsigned int R_PLANE3 : 16;
  unsigned int R_PLANE6 : 16;
  unsigned int R_PLANE5 : 16;
  unsigned int G_PLANE0 : 16;
  unsigned int R_PLANE7 : 16;

  unsigned int G_PLANE2 : 16;
  unsigned int G_PLANE1 : 16;
  unsigned int G_PLANE4 : 16;
  unsigned int G_PLANE3 : 16;
  unsigned int G_PLANE6 : 16;
  unsigned int G_PLANE5 : 16;
  unsigned int B_PLANE0 : 16;
  unsigned int G_PLANE7 : 16;

  unsigned int B_PLANE2 : 16;
  unsigned int B_PLANE1 : 16;
  unsigned int B_PLANE4 : 16;
  unsigned int B_PLANE3 : 16;
  unsigned int B_PLANE6 : 16;
  unsigned int B_PLANE5 : 16;
  unsigned int HIGH_LIMIT_R : 16;
  unsigned int B_PLANE7 : 16;

  unsigned int HIGH_LIMIT_B : 16;
  unsigned int HIGH_LIMIT_G : 16;

  unsigned int PLANE_R_MAG : 32;
  unsigned int PLANE_G_MAG : 32;
  unsigned int PLANE_B_MAG : 32;

  unsigned int PLANE1_R_OFFSET : 16;
  unsigned int PLANE0_R_OFFSET : 16;
  unsigned int PLANE3_R_OFFSET : 16;
  unsigned int PLANE2_R_OFFSET : 16;
  unsigned int PLANE5_R_OFFSET : 16;
  unsigned int PLANE4_R_OFFSET : 16;
  unsigned int PLANE7_R_OFFSET : 16;
  unsigned int PLANE6_R_OFFSET : 16;

  unsigned int PLANE1_G_OFFSET : 16;
  unsigned int PLANE0_G_OFFSET : 16;
  unsigned int PLANE3_G_OFFSET : 16;
  unsigned int PLANE2_G_OFFSET : 16;
  unsigned int PLANE5_G_OFFSET : 16;
  unsigned int PLANE4_G_OFFSET : 16;
  unsigned int PLANE7_G_OFFSET : 16;
  unsigned int PLANE6_G_OFFSET : 16;

  unsigned int PLANE1_B_OFFSET : 16;
  unsigned int PLANE0_B_OFFSET : 16;
  unsigned int PLANE3_B_OFFSET : 16;
  unsigned int PLANE2_B_OFFSET : 16;
  unsigned int PLANE5_B_OFFSET : 16;
  unsigned int PLANE4_B_OFFSET : 16;
  unsigned int PLANE7_B_OFFSET : 16;
  unsigned int PLANE6_B_OFFSET : 16;

  unsigned int G_LIMIT_COEF : 16;
  unsigned int R_LIMIT_COEF : 16;
  unsigned int PLANE_R0_COEF : 16;
  unsigned int B_LIMIT_COEF : 16;

  unsigned int PLANE_R2_COEF : 16;
  unsigned int PLANE_R1_COEF : 16;
  unsigned int PLANE_R4_COEF : 16;
  unsigned int PLANE_R3_COEF : 16;
  unsigned int PLANE_R6_COEF : 16;
  unsigned int PLANE_R5_COEF : 16;
  unsigned int PLANE_G0_COEF : 16;
  unsigned int PLANE_R7_COEF : 16;

  unsigned int PLANE_G2_COEF : 16;
  unsigned int PLANE_G1_COEF : 16;
  unsigned int PLANE_G4_COEF : 16;
  unsigned int PLANE_G3_COEF : 16;
  unsigned int PLANE_G6_COEF : 16;
  unsigned int PLANE_G5_COEF : 16;
  unsigned int PLANE_B0_COEF : 16;
  unsigned int PLANE_G7_COEF : 16;

  unsigned int PLANE_B2_COEF : 16;
  unsigned int PLANE_B1_COEF : 16;
  unsigned int PLANE_B4_COEF : 16;
  unsigned int PLANE_B3_COEF : 16;
  unsigned int PLANE_B6_COEF : 16;
  unsigned int PLANE_B5_COEF : 16;
  unsigned int RESERVED20 : 16;
  unsigned int PLANE_B7_COEF : 16;

  unsigned int RESERVED21 :32;
  unsigned int DEMURA_nRESET :16;    //fix 0x00 01
  unsigned int RESERVED22 : 16;
  unsigned int H_ACTIVE : 16;
  unsigned int DEMURA_3D_ON :16;     //fix 0x00 01
  unsigned int V_START_POSITION : 16;//fix 0x00 40
  unsigned int V_ACTIVE : 16;

  unsigned int RESERVED23 :32;
  unsigned int H_POSITION : 16;
  unsigned int RESERVED24 :16;
  unsigned int V_POSITION : 16;
  unsigned int RESERVED25 :16;
  unsigned int FIX_DATA3 : 32;  //00 B4 00 51
  unsigned int RESERVED26 :32;
  unsigned int RESERVED27 :32;

  unsigned int FIX_DATA4 : 32;  //18 10 00 00
  unsigned int FIX_DATA5 : 32;  //C0 80 40 20
  unsigned int FIX_DATA6 : 32;  //00 00 00 00
  unsigned int FIX_DATA7 : 32;  //0F FF 00 00
  unsigned int FIX_DATA8 : 32;  //00 00 0F FF


  unsigned int RESERVED28 :32;
  unsigned int RESERVED29 :32;
  unsigned int RESERVED30 :32;
  unsigned int RESERVED31 :32;
  unsigned int RESERVED32 :32;
  unsigned int RESERVED33 :32;
  unsigned int RESERVED34 :32;
  unsigned int RESERVED35 :32;
  unsigned int RESERVED36 :32;
  unsigned int RESERVED37 :32;
  unsigned int RESERVED38 :32;
  unsigned int RESERVED39 :32;
  unsigned int RESERVED40 :32;
  unsigned int RESERVED41 :32;
  unsigned int RESERVED42 :32;
  unsigned int RESERVED43 :32;
  unsigned int RESERVED44 :32;
  unsigned int RESERVED45 :32;
  unsigned int RESERVED46 :32;
  unsigned int RESERVED47 :32;
  unsigned int RESERVED48 :32;
  unsigned int RESERVED49 :32;
  unsigned int RESERVED50 :32;
  unsigned int RESERVED51 :32;
  unsigned int RESERVED52 :32;
  unsigned int RESERVED53 :32;
  unsigned int RESERVED54 :32;
  unsigned int RESERVED55 :32;
  unsigned int RESERVED56 :32;
  unsigned int RESERVED57 :32;
  unsigned int RESERVED58 :32;
  unsigned int RESERVED59 :32;
  unsigned int RESERVED60 :32;
  unsigned int RESERVED61 :32;
  unsigned int RESERVED62 :32;
  unsigned int RESERVED63 :32;
  unsigned int RESERVED64 :32;
  unsigned int RESERVED65 :32;
  unsigned int RESERVED66 :32;
  unsigned int RESERVED67 :32;
  unsigned int RESERVED68 :32;
  unsigned int RESERVED69 :32;
  unsigned int RESERVED70 :32;
  unsigned int RESERVED71 :32;
  unsigned int RESERVED72 :32;
  unsigned int RESERVED73 :32;
  unsigned int RESERVED74 :32;
  unsigned int RESERVED75 :32;
  unsigned int RESERVED76 :32;
  unsigned int RESERVED77 :32;
  unsigned int RESERVED78 :32;

} VD_4k_Header;

typedef struct VD_4k_IQCHeader {

    unsigned int FIX_DATA0 : 32;      //00 00 10 00
    unsigned int FIX_DATA1 : 32;      //00 E6 80 00
    unsigned int FIX_DATA2 : 32;      //00 00 00 2F
    unsigned int FIX_DATA3 : 32;      //00 00 00 00
    unsigned int FIX_DATA4 : 32;      //00 00 10 60
    unsigned int FIX_DATA5 : 32;      //00 E6 84 24
    unsigned int FIX_DATA6 : 32;      //00 00 58 10
    unsigned int FIX_DATA7 : 32;      //00 00 00 00
    unsigned int FIX_DATA8 : 32;      //00 00 D0 00 
    unsigned int FIX_DATA9 : 32;      //00 E6 80 BC
    unsigned int FIX_DATA10 : 32;      //00 00 00 01
    unsigned int FIX_DATA11 : 32;      //00 00 00 00
    unsigned int FIX_DATA12 : 32;      //00 00 D0 04
    unsigned int FIX_DATA13 : 32;      //00 E6 80 F0
    unsigned int FIX_DATA14 : 32;      //00 00 00 CC
    unsigned int FIX_DATA15 : 32;      //00 00 00 00
} VD_4k_IQCHeader;

class VD_FORMAT : public TCONabstract {

 public:
  explicit VD_FORMAT();
  ~VD_FORMAT();
  int ToBin(CompMat *compMat, string &strSaveName) override;
  int PMicToBin(PMicCompMat *compMat, string &strSaveName) override;
  int GmToBin(GmCompMat *compMat, string &strSaveName) override;

private:

  int ToBinNoDll(CompMat *compMat, string &strSaveName);
  void WriteBinFile(const DEMURA_TABLE_BUFFER tbls[], string &strSaveName);
  void SetHeader(VD_4k_Header &header);
  void WriteHeader(ofstream &qs, VD_4k_Header &header);
  void NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src, int tbl_num,
                             int tbl_h, int tbl_v);
  void NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                             DEMURA_SETTING setting, unsigned char tcon_idx,
                             bool shift_en = false, bool dummy_en = false);
  void NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                      DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                      unsigned char tcon_idx);
  void TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                    DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                    const int begin, const int end);
  void ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                  const CompMat &compMat);
  int CreatVD60HzFromat(const GmCompMat &compMat,  char &burnbuffer);
  int CreatVD120HzFromat(const GmCompMat &compMat, char &burnbuffer);
  int CreatIQCFromat(const GmCompMat &compMat, char &burnbuffer);
  VD_4k_Header Init_VD_4K_header();
  VD_4k_IQCHeader Init_VD_4K_IQCheader();
  int IQCheaderToBin(GmCompMat* compMat, string& strSaveName);
  int CellInforToBin(PMicCompMat* compMat, string& strSaveName);

 private:
  CompMat *m_pCompMat;
  //const int m_pLOWER_BOUND = 12;    //参考精测
  //const int m_pUPPER_BOUND = 1020;  //参考精测

};
#endif // VDFORMAT_H
