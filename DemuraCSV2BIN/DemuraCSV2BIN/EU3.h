#ifndef EU3_H
#define EU3_H
#include "tconfactor.h"
#include "tconbase.h"

typedef struct EU3_Header {
    unsigned int LUT_CHECKSUM : 8;   
    unsigned int LUT_PARA00 : 8;   //fix 0x00
    unsigned int PLANE_MAG : 16;
    unsigned int PLANE0 : 8;
    unsigned int PLANE1 : 8;
    unsigned int PLANE2 : 8;
    unsigned int PLANE3 : 8;
    unsigned int PLANE4 : 8;
    unsigned int PLANE5 : 8;
    unsigned int LUT_PARA01 : 16;   //fix 0x0FFFF
    unsigned int LUT_PARA02 : 16;   //fix 0x0FFFF
    unsigned int RESERVED0 : 16;   //fix 0x00
    unsigned int RESERVED1 : 16;   //fix 0x00
    unsigned int RESERVED2 : 16;   //fix 0x00


} EU3_Header;

class EU3 : public TCONabstract {

 public:
  explicit EU3();
  ~EU3();
  int ToBin(CompMat *compMat, string &strSaveName) override;
  int PMicToBin(PMicCompMat *compMat, string &strSaveName) override;
  int GmToBin(GmCompMat *compMat, string &strSaveName) override;

private:

  int ToBinNoDll(CompMat *compMat, string &strSaveName);
  void WriteBinFile(const DEMURA_TABLE_BUFFER tbls[], string &strSaveName);
  void SetHeader(EU3_Header &header);
  void WriteHeader(ofstream &qs, EU3_Header &header);
  void NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src, int tbl_num,
                             int tbl_h, int tbl_v);
  void NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                             DEMURA_SETTING setting, unsigned char tcon_idx,
                             bool shift_en = false, bool dummy_en = false);
  void NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                      DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                      unsigned char tcon_idx);
  void TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                    DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                    const int begin, const int end);
  void ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                  const CompMat &compMat);
  int CreatVD60HzFromat(const GmCompMat &compMat,  char &burnbuffer);
  int CreatVD120HzFromat(const GmCompMat &compMat, char &burnbuffer);
  EU3_Header Init_EU3_header();

 private:
  CompMat *m_pCompMat;
  //const int m_pLOWER_BOUND = 12;    //参考精测
  //const int m_pUPPER_BOUND = 1020;  //参考精测

};
#endif // VDFORMAT_H
