/**
* @projectName   CSOT_EEAD_ALG2_DEMURA
* @brief         Demura
* @author        wxq/cxh
* @date          2021-07-31 17:01
* Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
* rights reserved.
*/
#include "stdafx.h"
#include "nvttcon71265.h"
#include <WINDOWS.H>
#include <iostream>
#include <io.h>
#include <cstdio>
#include <fstream>
#include <cmath>
#include "tconbase.h"
#include "tconfactor.h"
#include "easylogging++.h"
using namespace std;

NVT71265::NVT71265() {}
NVT71265::~NVT71265() {}

int NVT71265::ToBin(CompMat *compMat, string &strSaveName) {
	if (compMat == nullptr || strSaveName.empty()) {
		LOG(INFO) << "Demura compMat == nullptr or strSaveName empty";
		return 0;
	}
	return(ToBinNoDll(compMat, strSaveName));
}
int NVT71265::PMicToBin(PMicCompMat *compMat, string &strSaveName) {
	if (compMat == nullptr || strSaveName.empty()) {
		LOG(INFO) << "PMIC compMat == nullptr or strSaveName empty";
		return 0;
	}
	LOG(INFO) << "Start Save  CSOT format PMIC to:" << strSaveName.c_str();
  compMat->burnsize = 8;
  unsigned char *srcbuffer =
      (unsigned char *)malloc(sizeof(unsigned char *) * compMat->burnsize);
  unsigned int* checksum_out = (unsigned int*)malloc(sizeof(unsigned int*));
  if (srcbuffer && checksum_out) {
	  srcbuffer[0] = 0x40;
	  srcbuffer[1] = 0x00;
	  srcbuffer[2] = 0x05;
	  srcbuffer[3] = 0x00;
	  srcbuffer[4] = compMat->DefaultVcom - 64 + compMat->burnData[0];
	  srcbuffer[5] = 0x00;
	  srcbuffer[6] = 0x00;
	  srcbuffer[7] = 0x00;
	  VD_Checksum(compMat->burnsize - 2, srcbuffer, checksum_out);
	  srcbuffer[6] = *checksum_out;

	  ofstream outfile;
	  // outfile.open(strSaveName, ios::out | ios::binary);
	  outfile.open(strSaveName, ios::_Nocreate | ios::binary);
	  outfile.seekp(compMat->VcomAddress, ios::beg);
	  for (int idx = 0; idx < compMat->burnsize; idx++) {
		  outfile << srcbuffer[idx + 3];
		  outfile << srcbuffer[idx + 2];
		  outfile << srcbuffer[idx + 1];
		  outfile << srcbuffer[idx + 0];
		  idx += 4;
	  }
	  outfile.close();
	  LOG(INFO) << "PMIC2bin finished";

	  FreeMallocInt(checksum_out);
	  if (srcbuffer != NULL) {
		  free(srcbuffer);
		  srcbuffer = NULL;
		  LOG(INFO) << "free PMIC burnbuffer!";
	  }
	  return 1;
  }
  else {
	  LOG(INFO) << "PMIC code2bin fail,malloc fail!";
	  return 0;
  }
}
int NVT71265::GmToBin(GmCompMat *compMat, string &strSaveName) {
	LOG(INFO) << "Start Save Gamma to:" << strSaveName.c_str();
	if (compMat == nullptr) {
		LOG(INFO) << "Gamma code2bin fail,compMat == nullptr!";
		return 0;
	}
	if (strSaveName.empty()) {
		LOG(INFO) << "Gamma code2bin fail,strSaveName empty!";
		return 0;
	}

  char *burnbuffer = (char *)malloc(
      sizeof(char *) *
      8194);  // pow(2, compMat.GMInputBits) * 3 *compMat.GMOutputBits /8);
  if (burnbuffer) {
	  CreatVD60HzFromat(*compMat, *burnbuffer);

	  ofstream outfile;
	  // outfile.open(strSaveName, ios::out | ios::binary);
	  outfile.open(strSaveName, ios::_Nocreate | ios::binary);
	  outfile.seekp(compMat->Address, ios::beg);
	  outfile.write(burnbuffer, 8194);
	  outfile.close();

	  if (burnbuffer != NULL) {
		  free(burnbuffer);
		  burnbuffer = NULL;
		  LOG(INFO) << "free Gamma burnbuffer!";
	  }
	  return 1;
  }
  else {
	  LOG(INFO) << "Gamma code2bin fail,malloc fail!";
	  return 0;
  }
}

void NVT71265::CreatVD60HzFromat(const GmCompMat &compMat, char &burnbuffer) {
  char *ptr = &burnbuffer;
  unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));

  unsigned int j = 0;
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_R[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_R[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_G[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_G[i] & 0xFF);
    i += 2;
  }
  for (int i = 0; i < 256;) {
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i + 1] & 0xFF);

    checksum_out += ptr[j++] = (compMat.burnData_B[i] >> 8 & 0x0F);
    checksum_out += ptr[j++] = (compMat.burnData_B[i] & 0xFF);
    i += 2;
  }
  unsigned int bank_size = 0x1800 - 0x200 * 3;
  for (int i = 0; i < bank_size + 2; i++) {
    ptr[j++] = 0x00;
  }
  ptr[j++] = (*checksum_out >> 8 & 0xFF);
  ptr[j++] = (*checksum_out & 0xFF);

  FreeMallocInt(checksum_out);
}
int NVT71265::ToBinNoDll(CompMat *compMat, string &strSaveName) {
	m_pCompMat = compMat;

	// Step 1 – Create source buffer
	DEMURA_SOURCE_BUFFER src;
	NoDll_CreateSrcBuffer(&src, compMat->channels * compMat->compNum,
		compMat->burnWidth, compMat->burnHeight);
	// Step 2 – Read demura compensation data
	ReadDemuraCompensationData(src, *compMat);

	// Step 3 – Demura setting
	DEMURA_SETTING setting;
	//setting.total_tcon_count = 2;
	setting.total_tcon_count = 0;
	setting.table.h = compMat->burnWidth;
	setting.table.v = compMat->burnHeight;
	setting.scale_x = 8;
	setting.scale_y = 8;
	setting.node_num = compMat->channels * compMat->compNum;

	// Step 4 – Create table buffer
	DEMURA_TABLE_BUFFER tbls[2];
	NoDll_CreateTblBuffer(&tbls[0], setting, setting.total_tcon_count);
	//NoDll_CreateTblBuffer(&tbls[1], setting, 2);

	// Step 5 – Convert demura table
	NoDll_TableGen(src, tbls[0], setting, setting.total_tcon_count);
	//NoDll_TableGen(src, tbls[1], setting, 2);

	// Step 6 – Save demura table
	WriteBinFile(tbls, strSaveName);

	// Step 7 –Free source and table buffer
	if (tbls[0].buffer != NULL) {
		free(tbls[0].buffer);
		tbls[0].buffer = NULL;
		LOG(INFO) << "free demura burnbuffer!";
	}

	/*FreeMallocInt(tbls[0].checksum);
	FreeMallocInt(tbls[0].crc);*/
	Free3Darray(src.buffer);
	LOG(INFO) << "free demura databuffer!";
	return 1;
}

void NVT71265::ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
	const CompMat &compMat) {
	int *ptr = &src.buffer[0][0][0];
	int nPlaneSize = compMat.burnHeight * compMat.burnWidth;
	float R1, G1, B1, R2, G2, B2, R3, G3, B3;  //补偿值
	for (int i = 0; i < nPlaneSize; i++) {
		// NovaTek: R1、R2、R3、G1、G2、G3、B1、B2、B3
		// compMat: R1、G1、B1、R2、G2、B2、R3、G3、B3
		R1 = compMat.burnData[nPlaneSize * 0 + i] - compMat.compGrayList[0];
		G1 = compMat.burnData[nPlaneSize * 1 + i] - compMat.compGrayList[0];
		B1 = compMat.burnData[nPlaneSize * 2 + i] - compMat.compGrayList[0];
		R2 = compMat.burnData[nPlaneSize * 3 + i] - compMat.compGrayList[1];
		G2 = compMat.burnData[nPlaneSize * 4 + i] - compMat.compGrayList[1];
		B2 = compMat.burnData[nPlaneSize * 5 + i] - compMat.compGrayList[1];
		R3 = compMat.burnData[nPlaneSize * 6 + i] - compMat.compGrayList[2];
		G3 = compMat.burnData[nPlaneSize * 7 + i] - compMat.compGrayList[2];
		B3 = compMat.burnData[nPlaneSize * 8 + i] - compMat.compGrayList[2];

		//ptr[i * 3 + 0] = QString::asprintf("%.0f", R1 * 4).toInt();  // for 四舍五入
		//ptr[i * 3 + 1] = QString::asprintf("%.0f", R2 * 4).toInt();
		//ptr[i * 3 + 2] = QString::asprintf("%.0f", R3 * 4).toInt();
		ptr[i * 3 + 0] = round(R1 * 4);  // for 四舍五入
		ptr[i * 3 + 1] = round(R2 * 4);
		ptr[i * 3 + 2] = round(R3 * 4);
		//    ptr[i * 9 + 3] = QString::asprintf("%.0f", G1 * 4).toInt();
		//    ptr[i * 9 + 4] = QString::asprintf("%.0f", G2 * 4).toInt();
		//    ptr[i * 9 + 5] = QString::asprintf("%.0f", G3 * 4).toInt();
		//    ptr[i * 9 + 6] = QString::asprintf("%.0f", B1 * 4).toInt();
		//    ptr[i * 9 + 7] = QString::asprintf("%.0f", B2 * 4).toInt();
		//    ptr[i * 9 + 8] = QString::asprintf("%.0f", B3 * 4).toInt();
	}
}




void NVT71265::NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src,
	int tbl_num, int tbl_h, int tbl_v) {
	demura_src->buffer = Create3Darray(tbl_num, tbl_v, tbl_h);
	demura_src->tbl_h = tbl_h;
	demura_src->tbl_v = tbl_v;
	demura_src->tbl_num = tbl_num;
	demura_src->size = tbl_num * tbl_h * tbl_v;
}


void NVT71265::NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
	DEMURA_SETTING setting,
	unsigned char tcon_idx, bool shift_en,
	bool dummy_en) {
	/* Q_UNUSED(shift_en)
	Q_UNUSED(dummy_en)*/
	int Table_H;
	switch (tcon_idx) {
	case 0: {
		Table_H = setting.table.h;
		break;
	}
	default: {
		Table_H = setting.table.h / 2 + 1;
		break;
	}
	}

	// NT71267_2_TCON demura table description.pdf相关公式
	int Table_V = setting.table.v;
	int nNodeNumber = setting.node_num;
	int Block_H = setting.scale_x;
	//double tmp = (Table_H + (Block_H / 8)) * Table_V * nNodeNumber * 12 / 256;
	//double tmp = (Table_H + (Block_H / 8)) * Table_V * nNodeNumber / 3 * 12 / 256;
	//int Table_SIZE = ceil(tmp + 0.5) * 32;  //向上取整
	//Table_SIZE = 241 * 136 * 3 * 1.5 + 4;
	int Table_SIZE = setting.table.h * setting.table.v * setting.node_num * 1.5;

	demura_tbl->buffer =
		(unsigned char *)malloc(sizeof(unsigned char *) * Table_SIZE);
	demura_tbl->size = Table_SIZE;
	//demura_tbl->crc = (unsigned int *)malloc(sizeof(unsigned int *));
}

void NVT71265::NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
	DEMURA_TABLE_BUFFER &demura_tbl,
	DEMURA_SETTING setting, unsigned char tcon_idx) {
	/*demura_tbl.buffer =
		(unsigned char *)malloc(sizeof(unsigned char *) * demura_tbl.size);
	demura_tbl.crc = (unsigned int *)malloc(sizeof(unsigned int *));
	demura_tbl.checksum = (unsigned int *)malloc(sizeof(unsigned int *));*/

	switch (tcon_idx) {
	case 0: {
		int begin = 0;
		int end = setting.table.h;
		TableGenLoop(demura_src, demura_tbl, setting, begin, end);
		break;
	}
	case 1: {
		int begin = 0;
		int end = setting.table.h / 2 + 1;//?
		TableGenLoop(demura_src, demura_tbl, setting, begin, end);
		break;
	}
	case 2: {
		int begin = setting.table.h / 2;
		int end = setting.table.h;
		TableGenLoop(demura_src, demura_tbl, setting, begin, end);
		break;
	}
	}

	//  NvtDemura_CalCrcChecksum(demura_tbl.buffer, demura_tbl.size, demura_tbl.crc,
	//                           demura_tbl.checksum);
}

void NVT71265::TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
	DEMURA_TABLE_BUFFER &demura_tbl,
	DEMURA_SETTING setting, const int colBegin,
	const int colEnd) {
	//将两个12bits，存储在三个byte中，高8bits+（合并：低4bits+高4bits）+低8bits
	int cnt = 0;
	bool bEven = true;
	int nRow = setting.table.v;//?
	int nCol = setting.table.h;//?
	int nNodeNum = setting.node_num;
	int nFull8bits, nMerge8bits;
	int *p = &demura_src.buffer[0][0][0];
	int cnt2 = 0;
	for (int k = 0; k < nRow; k++) {
		for (int i = colBegin; i < colEnd; i++) {
			for (int j = 0; j < 3; j++) {
				cnt2++;
				int tmp = p[k * nCol * 3 + i * 3 + j];  //补偿值
				if (bEven) {
					nFull8bits = (tmp >> 4) & 0xFF;
					nMerge8bits = (tmp << 4) & 0xF0;
					bEven = false;
				}
				else {
					nFull8bits = tmp & 0xFF;
					nMerge8bits += (tmp >> 8) & 0x0F;
					demura_tbl.buffer[cnt++] = nMerge8bits;  //存储合并的8bits
					bEven = true;
				}
				demura_tbl.buffer[cnt++] = nFull8bits;
			}
		}
	}
	demura_tbl.buffer[cnt++] = 0;
	demura_tbl.buffer[cnt++] = 0;
	demura_tbl.buffer[cnt++] = 0;
	demura_tbl.buffer[cnt++] = 0;
}

void NVT71265::WriteBinFile(const DEMURA_TABLE_BUFFER tbls[],
	string &strSaveName) {

	LOG(INFO) << "Save  NT71265 Demura to: " << strSaveName.c_str();

	unsigned int Write_byte_div;
	unsigned int Read_byte_2x;

	unsigned int *crc_out = (unsigned int *)malloc(sizeof(unsigned int *));
	unsigned int *checksum_out = (unsigned int *)malloc(sizeof(unsigned int *));
	if (checksum_out && crc_out) {
		ofstream outfile;
		//outfile.open(strSaveName, ios::out | ios::binary);
		outfile.open(strSaveName, ios::_Nocreate | ios::binary);
		outfile.seekp(m_pCompMat->DemuraAddress, ios::beg);

		NT71265_Header header = Init_NT71265_header();
		SetHeader(header);
		//WriteHeader(outfile, header);

		for (int nTCONindex = 0; nTCONindex < 1; nTCONindex++) {
			DEMURA_TABLE_BUFFER tbl = tbls[nTCONindex];

			NvtCRC8(tbl.size, tbl.buffer, crc_out, checksum_out);
			LOG(INFO) << "Demura CRC/checksunm is: " << *crc_out;

			header.DEMURA_CRC = _byteswap_ushort(*crc_out);

			//Write_byte_div = ceil(float(m_pCompMat->burnWidth * m_pCompMat->burnHeight * 12 * m_pCompMat->compNum / 128.0)) + 16;
			////header.Write_byte_div = _byteswap_ushort(Write_byte_div);
			//
			//Read_byte_2x = m_pCompMat->burnWidth * m_pCompMat->compNum * 3;
			//header.Read_byte_2x = _byteswap_ushort(Read_byte_2x);

			WriteHeader(outfile, header);

			for (int idx = 0; idx < 152; idx++) {
				outfile << char(0x00);
			}

			for (int idx = 0; idx < tbl.size; idx++) {
				outfile << tbl.buffer[idx];
			}
		}

		outfile.close();
		LOG(INFO) << "Demura CSV2bin finished!";
		//string strNewSaveName = strSaveName;
		//strNewSaveName.replace(".bin", "1.bin");
		//strNewSaveName.replace(strNewSaveName.find(".csv"), 1, "1.bin");
		//if (QFile::copy(strSaveName, strNewSaveName)) {  //将文件复制到新的文件路径下
		// // Log4Info(strNewSaveName + " finished!");
		//}
		FreeMallocInt(crc_out);
		FreeMallocInt(checksum_out);
	}
}

void NVT71265::SetHeader(NT71265_Header &header) {
	if (m_pCompMat == nullptr) {
		return;
	}
	//header.Read_byte_2X = _byteswap_ushort((m_pCompMat->compNum < 12) + m_pCompMat->burnWidth * m_pCompMat->compNum * 3);

	header.DEMURA_TBL_H_H = (m_pCompMat->burnWidth)>>8 & 0xFF;
	header.DEMURA_TBL_H_L = (m_pCompMat->burnWidth) & 0xFF;
	header.DEMURA_TBL_V = (m_pCompMat->burnHeight);
	header.LOWER_BOUND = _byteswap_ushort(m_pCompMat->lower_Bound);
	header.UPPER_BOUND = _byteswap_ushort(m_pCompMat->high_Bound);
	header.PLANE00_LV = _byteswap_ushort(m_pCompMat->compGrayList[0]);
	header.PLANE01_LV = _byteswap_ushort(m_pCompMat->compGrayList[1]);
	header.PLANE02_LV = _byteswap_ushort(m_pCompMat->compGrayList[2]);
	header.PLANE03_LV = _byteswap_ushort(m_pCompMat->compGrayList[2]);   //假设plane 个数只有3，plane 3,4的值等于plane2
	header.PLANE04_LV = _byteswap_ushort(m_pCompMat->compGrayList[2]);	 //假设plane 个数只有3，plane 3,4的值等于plane2	
	header.PlaneB0Slope = _byteswap_ulong((1024 << 20) / ((m_pCompMat->compGrayList[0] - m_pCompMat->lower_Bound) << 4));
	header.Plane2WSlope = _byteswap_ulong((1024 << 20) / ((m_pCompMat->high_Bound - m_pCompMat->compGrayList[2]) << 4));
	header.Plane01Slope = _byteswap_ulong((1024 << 20) / ((m_pCompMat->compGrayList[1] - m_pCompMat->compGrayList[0]) << 4));
	header.Plane12Slope = _byteswap_ulong((1024 << 20) / ((m_pCompMat->compGrayList[2] - m_pCompMat->compGrayList[1]) << 4));
	header.Demura_Block_HV = 0; //fix0: 8x8  ,1: 16x16
	
	header.Write_byte_div_H = int((ceil(float(m_pCompMat->burnWidth * m_pCompMat->burnHeight * 12 * m_pCompMat->compNum / 128.0)) + 16)) >> 8 & 0xFF;   // size
	header.Write_byte_div_L = int((ceil(float(m_pCompMat->burnWidth * m_pCompMat->burnHeight * 12 * m_pCompMat->compNum / 128.0)) + 16)) & 0xFF;   // size

	header.Read_byte_2x = _byteswap_ushort(m_pCompMat->burnWidth*m_pCompMat->compNum * 3);
							 

							 /* Log4Info(QString("NVTTCON  Header, CRC = 0x%1, Checksum = 0x%2")
							 .arg(QString::number(*crc_out, 16))
							 .arg(QString::number(*checksum_out, 16)));*/
}

void NVT71265::WriteHeader(ofstream &qs, NT71265_Header &header) {
	//QByteArray byte = StructToByte(header);
	//qs.writeRawData(byte, sizeof(NT71267_Header));

	int length = sizeof(header);
	BYTE *p = new BYTE[length];
	memmove(p, &header, length);
	for (int idx = 0; idx < length; idx++) {
		qs << p[idx];
	}
}

NT71265_Header NVT71265::Init_NT71265_header() {
	NT71265_Header header;
	header.header = _byteswap_ulong(0x20222220);
	header.RESERVED00 = 0;
	header.RESERVED01 = 0;
	header.RESERVED02 = 0;
	header.RESERVED03 = 0;
	header.RESERVED04 = 0;
	header.RESERVED05 = 0;
	header.RESERVED06 = 0;
	header.RESERVED07 = 0;
	header.RESERVED08 = 0;
	header.RESERVED09 = 0;

	header.LOWER_BOUND = _byteswap_ushort(0);    //
	header.UPPER_BOUND = _byteswap_ushort(1023);  //

	header.PLANE00_LV = _byteswap_ushort(100);
	header.PLANE01_LV = _byteswap_ushort(240);
	header.PLANE02_LV = _byteswap_ushort(900);
	header.PLANE03_LV = _byteswap_ushort(900);   //假设plane 个数只有3，plane 3,4的值等于plane2
	header.PLANE04_LV = _byteswap_ushort(900);	 //假设plane 个数只有3，plane 3,4的值等于plane2	
	header.PlaneB0Slope = _byteswap_ulong((1024 << 20) / ((100 - 0) << 4));
	header.Plane2WSlope = _byteswap_ulong((1024 << 20) / ((1023 - 900) << 4));
	header.Plane01Slope = _byteswap_ulong((1024 << 20) / ((240 - 100) << 4));
	header.Plane12Slope = _byteswap_ulong((1024 << 20) / ((900 - 240) << 4));

	header.RESERVED10 = 0;
	header.RESERVED11 = 0;
	header.RESERVED12 = 0;
	header.RESERVED13 = 0;
	header.RESERVED14 = 0;
	header.RESERVED15 = 0;
	header.RESERVED16 = 0;
	header.RESERVED17 = 0;

	header.Dither_setting0 = 0x10;   //data 0X40
	header.Dither_setting1 = 0x41;
	header.Dither_setting2 = 0x08;
	header.Dither_setting3 = 0x92;
	header.Dither_setting4 = 0xF5;
	header.Dither_setting5 = 0x3C;
	header.Dither_setting6 = 0xA6;
	header.Dither_setting7 = 0xED;
	header.Dither_setting8 = 0x7B;
	header.Dither_setting9 = 0x00;
	header.Dither_setting10 = 0x00;


	header.Demura_Plane_Num = 0x03;   //plane num
	header.Fix_data0 = 0x80;    //fix 0x80
	header.Fix_data1 = 0;    //fix 0x00
	header.Demura_Block_HV = 0;
	header.DEMURA_TBL_H_H = (241) >> 8 & 0xFF;
	header.DEMURA_TBL_H_L = (241) & 0xFF;
	header.Fix_data2 = 0;
	header.DEMURA_TBL_V = 136;  //1080/8+1

	header.RESERVED20 = 0;   //0X47
	header.RESERVED21 = 0;
	header.RESERVED22 = 0;
	header.RESERVED23 = 0;
	header.RESERVED24 = 0;
	header.RESERVED25 = 0;
	header.RESERVED26 = 0;
	header.RESERVED27 = 0;
	header.RESERVED28 = 0;
	header.RESERVED29 = 0;
	header.RESERVED30 = 0;
	header.RESERVED31 = 0;
	header.RESERVED32 = 0;
	header.RESERVED33 = 0;
	header.RESERVED34 = 0;
	header.RESERVED35 = 0;
	header.RESERVED36 = 0;
	header.RESERVED37 = 0;
	header.RESERVED38 = 0;
	header.DEMURA_CRC = 0;
	header.Fix_data3 = 0x70;  //fix 0x70
	header.Fix_data4 = 0x00;	  //fix 0x00
	header.Fix_data5 = 0x00;   //fix 0x00
	header.Write_byte_div_H = 0x24;   // size
	header.Write_byte_div_L = 0x13;   // size
	header.Fix_data6 = 0x11;   //fix 0x11
	header.Fix_data7 = 0x00;   //fix 0x00
	header.Fix_data8 = 0x00;   //fix 0x00
	header.Read_byte_2x = _byteswap_ushort(0x0879);
	header.RESERVED39 = 0x00;
	header.RESERVED40 = 0x00;


	return header;
}
