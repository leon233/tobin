#pragma once
#ifndef DM_BINBASE_H
#define DM_BINBASE_H
#include <string>
#include <iostream>
#include "vector"
#include "tconfactor.h"
#include "tconbase.h"
///**
//* 对应目前已支持的TCON 型号；
//*/
//typedef enum {
//	NT_71102,
//	VD_8K_Format,
//	NT_71267,
//	CSOT_FORMAT,
//	NT_71733B,
//	NT_71265,
//	CSOT_FORMAT_Dual,
//	EU_3,
//	Oscar_Single,
//	Oscar_Double
//} DemuraTCONtype;

class Csv2bin {

public:
	//struct DM_INPUT
	//{
	//	unsigned int *PlaneTbl[5];
	//};

	//unsigned char header[64] = { 0 };
private:
	TCONabstract *m_pTCONabstract;
	TCONabstract *m_pTCONnvt71102;
	TCONabstract *m_pTCONnvt71267;
	TCONabstract *m_pTCONcsot;
	TCONabstract *m_pTCONvd;
	TCONabstract *m_pTCONnvt71733B;
	TCONabstract *m_pTCONnvt71265;
	TCONabstract *m_pTCONcsot_dual;
	TCONabstract *m_pTCON_EU3;
	TCONabstract *m_pTCON_Oscar;
	TCONabstract *m_pTCON_Oscar_double;
	bool m_bTCONSLected;
public:
	explicit Csv2bin();
	~Csv2bin();
	
	void setPTCONabstract(const DemuraTCONtype index);
	int run(string csvPath, CompMat *pCompMat);
    int run_PMIC(string csvPath, PMicCompMat *pPMicCompMat);
    int run_GM(string csvPath, GmCompMat *pGmCompMat);
	static bool ReadCSV(const char* strPanelID, CompMat *pCompMat);
	string GenerateBinName(const char* &strPanelID);
	//char* binpath;

};



#endif  // DM_BINBASE_H