#pragma once
/**
* @projectName   CSOT_EEAD_ALG2_DEMURA
* @brief         Demura
* @author        wxq/cxh
* @date          2021-07-31 17:01
* Copyright(c) 2021 TCL China Star Optoelectronics Technology Co., Ltd. All
* rights reserved.
*/
#ifndef NVTTCON71733B_H
#define NVTTCON71733B_H
//#include "NvtDemuraCodeGen.h"
#include "tconfactor.h"
#include "tconbase.h"

typedef struct NT71733B_Header {
	unsigned int header : 32;
	unsigned int RESERVED00 : 8;
	unsigned int RESERVED01 : 8;
	unsigned int RESERVED02 : 8;
	unsigned int RESERVED03 : 8;
	unsigned int RESERVED04 : 8;
	unsigned int RESERVED05 : 8;
	unsigned int RESERVED06 : 8;
	unsigned int RESERVED07 : 8;
	unsigned int RESERVED08 : 8;
	unsigned int RESERVED09 : 8;
	unsigned int LOWER_BOUND : 16;
	unsigned int UPPER_BOUND : 16;
	unsigned int PLANE00_LV : 16;
	unsigned int PLANE01_LV : 16;
	unsigned int PLANE02_LV : 16;
	unsigned int PLANE03_LV : 16;
	unsigned int PLANE04_LV : 16;
	unsigned int PlaneB0Slope : 32;
	unsigned int Plane2WSlope : 32;
	unsigned int Plane01Slope : 32;
	unsigned int Plane12Slope : 32;

	unsigned int RESERVED10 : 8;   //0X2C
	unsigned int RESERVED11 : 8;    
	unsigned int RESERVED12 : 8;
	unsigned int RESERVED13 : 8;
	unsigned int RESERVED14 : 8;
	unsigned int RESERVED15 : 8;
	unsigned int RESERVED16 : 8;
	unsigned int RESERVED17 : 8;

	unsigned int Dither_setting0 : 8;   //data 0X40
	unsigned int Dither_setting1 : 8;   
	unsigned int Dither_setting2 : 8;   
	unsigned int Dither_setting3 : 8;   
	unsigned int Dither_setting4 : 8;   
	unsigned int Dither_setting5 : 8;   
	unsigned int Dither_setting6 : 8;   
	unsigned int Dither_setting7 : 8;   
	unsigned int Dither_setting8 : 8;   
	unsigned int Dither_setting9 : 8;   
	unsigned int Dither_setting10 : 8;   
	unsigned int Read_byte_2X1 : 8;   //plane num & size
	unsigned int Read_byte_2X2 : 8;   //plane num & size
	unsigned int Fix_data0 : 8;    //fix 0x80
	unsigned int Fix_data1 : 8;    //fix 0x00
	unsigned int Demura_Block : 8;   //block size H & V 
	unsigned int DEMURA_TBL_H : 8;
	unsigned int DEMURA_TBL_V : 8;

	unsigned int RESERVED20 : 8;   //0X46
	unsigned int RESERVED21 : 8;
	unsigned int RESERVED22 : 8;
	unsigned int RESERVED23 : 8;
	unsigned int RESERVED24 : 8;
	unsigned int RESERVED25 : 8;
	unsigned int RESERVED26 : 8;
	unsigned int RESERVED27 : 8;
	unsigned int RESERVED28 : 8;
	unsigned int RESERVED29 : 8;
	unsigned int RESERVED30 : 8;
	unsigned int RESERVED31 : 8;
	unsigned int RESERVED32 : 8;
	unsigned int RESERVED33 : 8;
	unsigned int RESERVED34 : 8;
	unsigned int RESERVED35 : 8;
	unsigned int RESERVED36 : 8;
	unsigned int RESERVED37 : 8;

	unsigned int DEMURA_CRC : 16;
	unsigned int Fix_data2 : 8;   //fix 0x30
	unsigned int Write_byte_div_H : 8;   //plane num & size
	unsigned int Write_byte_div_L : 8;   //plane num & size
	unsigned int Fix_data3 : 8;   //fix 0x11
	unsigned int RESERVED38 : 8;
	unsigned int RESERVED39 : 8;

} NT71733B_Header;

class NVT71733B : public TCONabstract {

public:
	explicit NVT71733B();
	~NVT71733B();
	int ToBin(CompMat *compMat, string &strSaveName) override;
	int PMicToBin(PMicCompMat *compMat, string &strSaveName) override;
	int GmToBin(GmCompMat *compMat, string &strSaveName) override;

private:

	int ToBinNoDll(CompMat *compMat, string &strSaveName);
	void WriteBinFile(const DEMURA_TABLE_BUFFER tbls[], string &strSaveName);
	void SetHeader(NT71733B_Header &header);
	void WriteHeader(ofstream &qs, NT71733B_Header &header);
	void NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src, int tbl_num,
		int tbl_h, int tbl_v);
	void NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
		DEMURA_SETTING setting, unsigned char tcon_idx,
		bool shift_en = false, bool dummy_en = false);
	void NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
		DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
		unsigned char tcon_idx);
	void TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
		DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
		const int begin, const int end);
	void ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
		const CompMat &compMat);
   void CreatVD60HzFromat(const GmCompMat &compMat, char &burnbuffer);
	NT71733B_Header Init_NT71733B_header();

private:
	CompMat *m_pCompMat;
	//const int m_pLOWER_BOUND = 0;    //�ο�����
	//const int m_pUPPER_BOUND = 1023;  //�ο�����

};

#endif  // NVTTCON71733B_H
