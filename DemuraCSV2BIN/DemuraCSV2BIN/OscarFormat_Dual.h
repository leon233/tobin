#ifndef OscarFORMAT_DUAL_H
#define OscarFORMAT_DUAL_H
#include "tconfactor.h"
#include "tconbase.h"

typedef struct Oscar_Dual_Header {
  unsigned int REG_SIZE : 32;
  unsigned int REG_CHECKSUM : 16;
  unsigned int RESERVED00 : 16;  //MS_FLASH

  unsigned int LUT_SIZE : 32;
  unsigned int LUT_CHECKSUM : 16;
  unsigned int RESERVED01 : 16;

  unsigned int HEADER_CHECKSUM : 16;
  unsigned int RESERVED02 : 16;
  unsigned int RESERVED03 : 32;
  unsigned int RESERVED04 : 32;
  unsigned int RESERVED05 : 32;

  unsigned int DEMURA_ENABLE : 16;
  unsigned int BLOCK_SIZE : 16;
  unsigned int RGB_MODE_ON : 16;
  unsigned int PLANE_NUM : 16;   //(RGB:3)*plane
  
  unsigned int LOW_LIMIT_R : 16;
  unsigned int LOW_LIMIT_G : 16;
  unsigned int LOW_LIMIT_B : 16;

  unsigned int R_PLANE0 : 16;
  unsigned int R_PLANE1 : 16;
  unsigned int R_PLANE2 : 16;
  unsigned int R_PLANE3 : 16;
  unsigned int R_PLANE4 : 16;
  unsigned int R_PLANE5 : 16;
  unsigned int R_PLANE6 : 16;
  unsigned int R_PLANE7 : 16;

  unsigned int G_PLANE0 : 16;
  unsigned int G_PLANE1 : 16;
  unsigned int G_PLANE2 : 16;
  unsigned int G_PLANE3 : 16;
  unsigned int G_PLANE4 : 16;
  unsigned int G_PLANE5 : 16;
  unsigned int G_PLANE6 : 16;
  unsigned int G_PLANE7 : 16;
  unsigned int B_PLANE0 : 16;
  unsigned int B_PLANE1 : 16;

  unsigned int B_PLANE2 : 16;
  unsigned int B_PLANE3 : 16;
  unsigned int B_PLANE4 : 16;
  unsigned int B_PLANE5 : 16;
  unsigned int B_PLANE6 : 16;
  unsigned int B_PLANE7 : 16;
  unsigned int HIGH_LIMIT_R : 16;
  unsigned int HIGH_LIMIT_G : 16;
  unsigned int HIGH_LIMIT_B : 16;
  

  unsigned int PLANE_R_MAG : 32;
  unsigned int PLANE_G_MAG : 32;
  unsigned int PLANE_B_MAG : 32;

  unsigned int PLANE0_R_OFFSET : 16;
  unsigned int PLANE1_R_OFFSET : 16;
  unsigned int PLANE2_R_OFFSET : 16;
  unsigned int PLANE3_R_OFFSET : 16;
  unsigned int PLANE4_R_OFFSET : 16;
  unsigned int PLANE5_R_OFFSET : 16;
  unsigned int PLANE6_R_OFFSET : 16;
  unsigned int PLANE7_R_OFFSET : 16;
  
  unsigned int PLANE0_G_OFFSET : 16;
  unsigned int PLANE1_G_OFFSET : 16;
  unsigned int PLANE2_G_OFFSET : 16;
  unsigned int PLANE3_G_OFFSET : 16;
  unsigned int PLANE4_G_OFFSET : 16;
  unsigned int PLANE5_G_OFFSET : 16;
  unsigned int PLANE6_G_OFFSET : 16;
  unsigned int PLANE7_G_OFFSET : 16;
  
  unsigned int PLANE0_B_OFFSET : 16;
  unsigned int PLANE1_B_OFFSET : 16;
  unsigned int PLANE2_B_OFFSET : 16;
  unsigned int PLANE3_B_OFFSET : 16;
  unsigned int PLANE4_B_OFFSET : 16;
  unsigned int PLANE5_B_OFFSET : 16;
  unsigned int PLANE6_B_OFFSET : 16;
  unsigned int PLANE7_B_OFFSET : 16;
  
  unsigned int RESERVED07 :32;
  unsigned int RESERVED08 :32;
  unsigned int RESERVED09 :32;
  unsigned int RESERVED10 :32;
  unsigned int RESERVED11 :32;
  unsigned int RESERVED12 :32;
  unsigned int RESERVED13 :32;
  unsigned int RESERVED14 :32;
  unsigned int RESERVED15 :32;
  unsigned int RESERVED16 :32;
  unsigned int RESERVED17 :32;
  unsigned int RESERVED18 :32;
  unsigned int RESERVED19 :32;
  unsigned int RESERVED20 :32;
  unsigned int RESERVED21 :32;
  unsigned int RESERVED22 :32;
  unsigned int RESERVED23 :32;
  unsigned int RESERVED24 :32;
  unsigned int RESERVED25 :32;
  unsigned int RESERVED26 :32;
  unsigned int RESERVED27 :32;
  unsigned int RESERVED28 :32;
  unsigned int RESERVED29 :32;
  unsigned int RESERVED30 :32;
  unsigned int RESERVED31 :32;
  unsigned int RESERVED32 :32;
  unsigned int RESERVED33 :32;
  unsigned int RESERVED34 :32;
  unsigned int RESERVED35 :32;
  unsigned int RESERVED36 : 32;
  unsigned int RESERVED37 :32;
  unsigned int RESERVED38 :32;
  

} Oscar_Dual_Header;

class Oscar_FORMAT_DUAL : public TCONabstract {

 public:
  explicit Oscar_FORMAT_DUAL();
  ~Oscar_FORMAT_DUAL();
  int ToBin(CompMat *compMat, string &strSaveName) override;
  int PMicToBin(PMicCompMat *compMat, string &strSaveName) override;
  int GmToBin(GmCompMat *compMat, string &strSaveName) override;

private:

  int ToBinNoDll(CompMat *compMat, string &strSaveName);
  void WriteBinFile(const DEMURA_TABLE_BUFFER tbls[], string &strSaveName);
  void SetHeader(Oscar_Dual_Header &header);
  void WriteHeader(ofstream &qs, Oscar_Dual_Header &header);
  void NoDll_CreateSrcBuffer(DEMURA_SOURCE_BUFFER *demura_src, int tbl_num,
                             int tbl_h, int tbl_v);
  void NoDll_CreateTblBuffer(DEMURA_TABLE_BUFFER *demura_tbl,
                             DEMURA_SETTING setting, unsigned char tcon_idx,
                             bool shift_en = false, bool dummy_en = false);
  void NoDll_TableGen(const DEMURA_SOURCE_BUFFER &demura_src,
                      DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                      unsigned char tcon_idx);
  void TableGenLoop(const DEMURA_SOURCE_BUFFER &demura_src,
                    DEMURA_TABLE_BUFFER &demura_tbl, DEMURA_SETTING setting,
                    const int begin, const int end);
  void ReadDemuraCompensationData(DEMURA_SOURCE_BUFFER &src,
                                  const CompMat &compMat);
  int CreatVD60HzFromat(const GmCompMat &compMat,  char &burnbuffer);
  int CreatVD120HzFromat(const GmCompMat &compMat, char &burnbuffer);
  Oscar_Dual_Header Init_Oscar_header();
  int CellInforToBin(PMicCompMat* compMat, string& strSaveName);

 private:
  CompMat *m_pCompMat;

};
#endif // OscarFORMAT_DUAL_H
