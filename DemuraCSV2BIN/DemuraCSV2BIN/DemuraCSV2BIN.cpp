﻿// DemuraCSV2BIN.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "DemuraCSV2BIN.h"
#include <stdexcept>
#include <exception>
#include <fstream>
#include <sstream>  //
#include <vector>
#include <String>
#include <ctime>
#include "dm_binbase.h"
#include "tconbase.h"
#include "json.h"
#include <direct.h>
#include "easylogging++.h"

INITIALIZE_EASYLOGGINGPP;

using namespace std;

DLLAPI_EXPORT char* __stdcall GetDLLVersion() {
	return "version3.35";

}

DLLAPI_EXPORT int __stdcall BinCreatApi(
	uTCONtype tconType,
	BinParam binParam, const char* csvPath) {
	FILE* fp;
	time_t rawtime;
	struct tm timeinfo;
	
	
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		fprintf(fp, "#%d/%d/%d  %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
		fprintf(fp, "      Enter BinCreatApi Mode\n");
		fclose(fp);
	}
	if (csvPath == nullptr) {
		
		if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
			fprintf(fp, "csvPath==null，未指定正确CSV路径!\n");
			fclose(fp);
		}
		return 0;// INPUT_NULL_PONTER;
	}

	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		fprintf(fp, "csvPath:%s\n", csvPath);
		fprintf(fp, "DemuraTCONtype index:%d\n", tconType);
		fprintf(fp, "binParam setting:\nPlaneNum=%d  channel=%d   H_total=%d   V_total=%d\n", binParam.PlaneNum, binParam.channel, binParam.H_total, binParam.V_total);
		fprintf(fp, "imageH=%d  imageV=%d   BlockSizeH=%c   BlockSizeV=%c\n", binParam.imageH, binParam.imageV, binParam.BlockSizeH, binParam.BlockSizeV);
		fprintf(fp, "LowBound=%d  HighBound=%d\n", binParam.imageH, binParam.imageV);
		fprintf(fp, "Planes = [% d] - [% d] - [% d] - [% d] - [% d]\n", binParam.Plane[0], binParam.Plane[1], binParam.Plane[2], binParam.Plane[3], binParam.Plane[4]);
		fclose(fp);
	}
	//CompMat *pCompMat = Csv2bin::GenerateCompMat(binParam);
	CompMat *pCompMat = CreateCompMat(binParam.PlaneNum, binParam.Plane,
		binParam.channel, binParam.imageV, binParam.imageH,
		binParam.H_total, binParam.V_total, binParam.LowBound, binParam.HighBound);

	Csv2bin c2b;
	//DemuraTCONtype index;
	switch (tconType)
	{
	case uTCONtype::uCSOT_FORMAT:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT);
			break;
	case uTCONtype::uNT_71102:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71102);
		break;
	case uTCONtype::uNT_71267:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71267);
		break;
	case uTCONtype::uVD_Format:
		c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
		break;
	case uTCONtype::uNT_71733B:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71733B);
		break;
	case uTCONtype::uNT_71265:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71265);
		break;
	case uTCONtype::uCSOT_FORMAT_Dual:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT_Dual);
		break;
	default:
		break;
	}
	if (c2b.ReadCSV(csvPath, pCompMat)) {
		string strBinName = c2b.GenerateBinName(csvPath);
		c2b.run(strBinName,pCompMat);
	}
	else
	{
		if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
			fprintf(fp, "Raed CSV fail!\n");
			fclose(fp);
		}
		free(pCompMat->burnData);
		pCompMat->burnData = NULL;
		return 0;
	}
	//std::ifstream inFile(paramPath, std::ios::in | std::ios::binary);
	//if (!inFile.is_open()) {
	//	return false;//UN_VALID_FILE;
	//}
	//int paramSize = imageRow * imageCol;
	//int count = 0;
	//float* temp = (float*)malloc(paramSize * sizeof(float));
	//while (inFile.read((char*)temp, paramSize * sizeof(float))) {
	//	count++;
	//}
	//inFile.close();
	//for (int i = 0; i < imageRow; ++i) {
	//	for (int j = 0; j < imageCol; ++j) {
	//		param[i * imageCol + j] = temp[j * imageRow + i];
	//	}
	//}
	//free(temp);
	free(pCompMat->burnData);
	pCompMat->burnData = NULL;
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		fprintf(fp, "#%d/%d/%d  %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
		fprintf(fp, "      Exit BinCreatApi Mode\n\n");
		fclose(fp);
	}
	return 1;
}

DLLAPI_EXPORT int __stdcall BinCreatFromMat(
	uTCONtype tconType,
	BinParam binParam, const char* csvPath) {
	FILE* fp;
	time_t rawtime;
	struct tm timeinfo;
       

	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		fprintf(fp, "+%d/%d/%d  %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
		fprintf(fp, "      Enter BinCreatFromMat Mode\n");
		fclose(fp);
	}
	if (csvPath == nullptr) {
		if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
			fprintf(fp, "csvPath==null，未指定正确bin 保存路径!\n");
			fclose(fp);
		}
		return 0;// INPUT_NULL_PONTER;
	}

	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		fprintf(fp, "csvPath:%s\n", csvPath);
		fprintf(fp, "DemuraTCONtype index:%d\n", tconType);
		fprintf(fp, "binParam setting:\nPlaneNum=%d  channel=%d   H_total=%d   V_total=%d\n", binParam.PlaneNum, binParam.channel, binParam.H_total, binParam.V_total);
		fprintf(fp, "imageH=%d  imageV=%d   BlockSizeH=%c   BlockSizeV=%c\n", binParam.imageH, binParam.imageV, binParam.BlockSizeH, binParam.BlockSizeV);
		fprintf(fp, "LowBound=%d  HighBound=%d\n", binParam.imageH, binParam.imageV);
		fprintf(fp, "Planes = [% d] - [% d] - [% d] - [% d] - [% d]\n", binParam.Plane[0], binParam.Plane[1], binParam.Plane[2], binParam.Plane[3], binParam.Plane[4]);
		fprintf(fp, "DemuraTCONtype index:%d\n", tconType);
		fclose(fp);
	}
	//CompMat *pCompMat = Csv2bin::GenerateCompMat(binParam);
	CompMat *pCompMat = CreateCompMat(binParam.PlaneNum, binParam.Plane,
		binParam.channel, binParam.imageV, binParam.imageH,
		binParam.H_total, binParam.V_total, binParam.LowBound, binParam.HighBound);
	Csv2bin c2b;

	switch (tconType)
	{
	case uTCONtype::uCSOT_FORMAT:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT);
		break;
	case uTCONtype::uNT_71102:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71102);
		break;
	case uTCONtype::uNT_71267:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71267);
		break;
	case uTCONtype::uVD_Format:
		c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
		break;
	case uTCONtype::uNT_71733B:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71733B);
		break;
	case uTCONtype::uNT_71265:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71265);
		break;
	case uTCONtype::uCSOT_FORMAT_Dual:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT_Dual);
		break;
	default:
		break;
	}
	pCompMat->burnData = binParam.burnDataIn;	

	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		fprintf(fp, "the first comp data is :%f\n", *pCompMat->burnData);
		fclose(fp);
	}
	string strBinName = c2b.GenerateBinName(csvPath);
	c2b.run(strBinName, pCompMat);
	
	free(pCompMat->burnData);
	pCompMat->burnData = NULL;
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		fprintf(fp, "+%d/%d/%d  %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
		fprintf(fp, "      Exit BinCreatFromMat Mode\n\n");
		fclose(fp);
	}
	return 1;
}

DLLAPI_EXPORT int __stdcall BinCreatFromMatAddress(
	uTCONtype tconType,
	BinParam binParam, const char* csvPath) {
	FILE* fp;
	time_t rawtime;
	struct tm timeinfo;
	
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		fprintf(fp, "-%d/%d/%d  %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
		fprintf(fp, "      Enter BinCreatFromMatAddress Mode\n");
		fclose(fp);
	}

	if (csvPath == nullptr) {
		
		if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
			fprintf(fp, "csvPath==null!\n");
			fclose(fp);
		}
		return 0;// INPUT_NULL_PONTER;
	}
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		fprintf(fp, "csvPath:%s\n", csvPath);
		fprintf(fp, "binParam setting:\nPlaneNum=%d  channel=%d   H_total=%d   V_total=%d\n", binParam.PlaneNum, binParam.channel, binParam.H_total, binParam.V_total);
		fprintf(fp, "imageH=%d  imageV=%d   BlockSizeH=%c   BlockSizeV=%c\n", binParam.imageH, binParam.imageV, binParam.BlockSizeH, binParam.BlockSizeV);
		fprintf(fp, "LowBound=%d  HighBound=%d\n", binParam.imageH, binParam.imageV);
		fprintf(fp, "Planes = [% d] - [% d] - [% d] - [% d] - [% d]\n", binParam.Plane[0], binParam.Plane[1], binParam.Plane[2], binParam.Plane[3], binParam.Plane[4]);
		fprintf(fp, "the comp Data Address is:0x%016I64x\n", binParam.burnDataAddress);
		fprintf(fp, "DemuraTCONtype index:%d\n", tconType);
		fclose(fp);
	}
	
	//CompMat *pCompMat = Csv2bin::GenerateCompMat(binParam);
	CompMat *pCompMat = CreateCompMat(binParam.PlaneNum, binParam.Plane,
		binParam.channel, binParam.imageV, binParam.imageH,
		binParam.H_total, binParam.V_total, binParam.LowBound, binParam.HighBound);

	Csv2bin c2b;
	switch (tconType)
	{
	case uTCONtype::uCSOT_FORMAT:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT);
		break;
	case uTCONtype::uNT_71102:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71102);
		break;
	case uTCONtype::uNT_71267:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71267);
		break;
	case uTCONtype::uVD_Format:
		c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
		break;
	case uTCONtype::uNT_71733B:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71733B);
		break;
	case uTCONtype::uNT_71265:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71265);
		break;
	case uTCONtype::uCSOT_FORMAT_Dual:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT_Dual);
		break;
	default:
		break;
	}
	if (pCompMat->burnData != nullptr) {
		free(pCompMat->burnData);
		pCompMat->burnData = nullptr;
	}
	pCompMat->burnData = (float*)(binParam.burnDataAddress);
	
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		fprintf(fp, "the first comp data is :%f\n", *pCompMat->burnData);
		fclose(fp);
	}

	string strBinName = c2b.GenerateBinName(csvPath);
	c2b.run(strBinName, pCompMat);
	
	/*if (pCompMat->burnData != nullptr) {
		free(pCompMat->burnData);
		pCompMat->burnData = nullptr;
	}*/
	//std::ifstream inFile(paramPath, std::ios::in | std::ios::binary);
	//if (!inFile.is_open()) {
	//	return false;//UN_VALID_FILE;
	//}
	//int paramSize = imageRow * imageCol;
	//int count = 0;
	//float* temp = (float*)malloc(paramSize * sizeof(float));
	//while (inFile.read((char*)temp, paramSize * sizeof(float))) {
	//	count++;
	//}
	//inFile.close();
	//for (int i = 0; i < imageRow; ++i) {
	//	for (int j = 0; j < imageCol; ++j) {
	//		param[i * imageCol + j] = temp[j * imageRow + i];
	//	}
	//}
	//free(temp);
	if (fopen_s(&fp, "csv2bin_log.txt", "at+") == 0) {
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		fprintf(fp, "-%d/%d/%d  %d:%d:%d", timeinfo.tm_year + 1900, timeinfo.tm_mon + 1, timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
		fprintf(fp, "      Exit BinCreatFromMatAddress Mode\n\n");
		fclose(fp);
	}
	return 1;
}

DLLAPI_EXPORT int __stdcall BinCreatFromMatAddressAndJS(
    const char* JsonString,
    const char* csvPath,
	const char* Plane_SN,
    BinData burnBinData) {
 
  string temp;
  time_t rawtime;
  struct tm timeinfo;//
  Json::Reader reader;
  Json::Value root;
  BinParam binParam;
  char timebuf[64];
  int strlen=0;
  //// Load configuration from file
  el::Configurations conf("log.conf");
  //// Actually reconfigure all loggers instead
  el::Loggers::reconfigureAllLoggers(conf);

  time(&rawtime);
  localtime_s(&timeinfo, &rawtime);

  LOG(INFO) << "<<   Enter BinCreatFromMatAddressAndJS Mode";
  LOG(INFO) << GetDLLVersion();
  mktime(&timeinfo);
  //strlen = strftime(timebuf, sizeof(timebuf), "%y%W", &timeinfo);
  strlen = strftime(timebuf, sizeof(timebuf), "%g%V", &timeinfo);   //%g%V：按ISO8601 获取当前年and周次
  if (strlen != 0) {
	  binParam.Week = (char*)malloc(strlen * sizeof(char));
	  strlen += 1;
	  memcpy(binParam.Week, timebuf, strlen);
	LOG(INFO) << "Week: "<< binParam.Week;
  }

  if (csvPath == nullptr) {
	LOG(INFO) << "csvPath ==null!";
    return 0;
  }

  if (ReadBinParamFromJson(JsonString, binParam) == 0) {
	  LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode,JSON string err!\n";
	  return 0;
  }
 
  // CompMat *pCompMat = Csv2bin::GenerateCompMat(binParam);
  
  Csv2bin c2b;
  switch (binParam.TconType) {
    case uTCONtype::uCSOT_FORMAT:
      c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT);
      break;
    case uTCONtype::uNT_71102:
      c2b.setPTCONabstract(DemuraTCONtype::NT_71102);
      break;
    case uTCONtype::uNT_71267:
      c2b.setPTCONabstract(DemuraTCONtype::NT_71267);
      break;
    case uTCONtype::uVD_Format:
      c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
      break;
    case uTCONtype::uNT_71733B:
      c2b.setPTCONabstract(DemuraTCONtype::NT_71733B);
      break;
    case uTCONtype::uNT_71265:
      c2b.setPTCONabstract(DemuraTCONtype::NT_71265);
      break;
    case uTCONtype::uCSOT_FORMAT_Dual:
      c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT_Dual);
      break;
	case uTCONtype::uEU3:
		c2b.setPTCONabstract(DemuraTCONtype::EU_3);
		break;
	case uTCONtype::uOscar:
		c2b.setPTCONabstract(DemuraTCONtype::Oscar_Single);
		break;
	case uTCONtype::uOscar_Dual:
		c2b.setPTCONabstract(DemuraTCONtype::Oscar_Double);
		break;
	case uTCONtype::uVD_Y21_8K:
		c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
		break;
    default:
      break;
  }
  // Step0:
  // creat the bin file path and file name
  //string strBinName = c2b.GenerateBinName(csvPath);
  string strBinName = csvPath;
  strBinName.append(Plane_SN);
  strBinName.append(binParam.BinName);
  

  // Step1:
  // ceater empty bin file
  ofstream outfile;
  outfile.open(strBinName, ios::out | ios::binary);
  char* buffer = (char*)malloc(sizeof(char*) * binParam.BinSize);
  memset(buffer, 0xFF, binParam.BinSize);
  outfile.write(buffer, binParam.BinSize);
  outfile.close();
  if (buffer != nullptr) {
	  free(buffer);
	  buffer = nullptr;
  }


  // Step2:
  // Demura data
  if (binParam.DemuraProcFlag == 1) {
    CompMat* pCompMat =CreateAllCompMat(binParam.PlaneNum, binParam.Plane, binParam.channel,
                      binParam.imageV, binParam.imageH, binParam.H_total, binParam.V_total, binParam.LowBound,
        binParam.HighBound, binParam.DemuraAddress);
    if (pCompMat->burnData != nullptr) {
      free(pCompMat->burnData);
      pCompMat->burnData = nullptr;
    }
    pCompMat->burnData = (float*)(burnBinData.DM_burnDataMemoryADD);
	LOG(INFO) << "the first Demura comp data is :"<< * pCompMat->burnData;

	if (c2b.run(strBinName, pCompMat) == 0) {
		LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode,Demura to bin fail!\n";
		return 0;
	}
  }
 
  // Step3: 
  // VCOM data
  if (binParam.VcomProcFlag == 1 || binParam.CellInforFlag == 1) {
	  PMicCompMat* pPMicCompMat =
		  CreatePMicCompMat(binParam.VcomAddress, binParam.DefaultVcom);
	  if (burnBinData.VcomData == -1 || binParam.VcomProcFlag == 0)
		  pPMicCompMat->burnData[0] = binParam.DefaultVcom;
	  else
		  pPMicCompMat->burnData[0] = burnBinData.VcomData;
	  pPMicCompMat->PMIC_CodeDir = binParam.PMicCodeDir;
	  int size = 96 + 1;
	  pPMicCompMat->CellInfo = (char*)malloc(96 * sizeof(char));
	  if (pPMicCompMat->CellInfo) {
		  for (int i = 0; i < 96; i++) {
			  pPMicCompMat->CellInfo[i] = 0;
		  }
		  int start = 0;
		  for (int i = 0; i < 10; i++) {
			  pPMicCompMat->CellInfo[start + i] = binParam.CellCode[i];
			  if (pPMicCompMat->CellInfo[start + i] == '\0')
				  break;
		  }
		  start = 16;
		  for (int i = 0; i < 3; i++) {
			  pPMicCompMat->CellInfo[start + i] = binParam.Revision[i];
			  if (pPMicCompMat->CellInfo[start + i] == '\0')
				  break;
		  }
		  start = 24;
		  for (int i = 0; i < 4; i++) {
			  pPMicCompMat->CellInfo[start + i] = binParam.Week[i];
			  if (pPMicCompMat->CellInfo[start + i] == '\0')
				  break;
		  }
		  start = 32;
		  for (int i = 0; i < 12; i++) {
			  pPMicCompMat->CellInfo[start + i] = Plane_SN[i];
			  if (pPMicCompMat->CellInfo[start + i] == '\0')
				  break;
		  }
		  start = 80;
		  for (int i = 0; i < 10; i++) {
			  pPMicCompMat->CellInfo[start + i] = binParam.ModelCode[i];
			  if (pPMicCompMat->CellInfo[start + i] == '\0')
				  break;
		  }
		  if (c2b.run_PMIC(strBinName, pPMicCompMat) == 0) {
			  if (pPMicCompMat->CellInfo != nullptr) {
				  free(pPMicCompMat->CellInfo);
				  pPMicCompMat->CellInfo = nullptr;
			  }
			  LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode,PMIC2bin fail!\n";
			  return 0;
		  }
		  else
		  {
			  if (pPMicCompMat->CellInfo != nullptr) {
				  free(pPMicCompMat->CellInfo);
				  pPMicCompMat->CellInfo = nullptr;
			  }
		  }
	  }
	  else {
		  LOG(INFO) << "PMIC to bin fail,malloc fail!";
		  LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode,PMIC2bin fail!\n";
		  return 0;
	  }
	  
  }
  // Step4: 
  // ACC data 
  //GmProcFlag=1：VD 60Hz
  //GmProcFlag=2：VD120Hz
  if (binParam.GmProcFlag == 1|| binParam.GmProcFlag == 2) {

    GmCompMat* pGmCompMat=CreateGmCompMat(
        binParam.AccAddress, binParam.GMInputBits,
                    binParam.GMOutputBits);
    pGmCompMat->burnData_R = (int*)(burnBinData.GM_R_burnDataMemoryADD);
    pGmCompMat->burnData_G = (int*)(burnBinData.GM_G_burnDataMemoryADD);
    pGmCompMat->burnData_B = (int*)(burnBinData.GM_B_burnDataMemoryADD);
	pGmCompMat->GmType = binParam.GmProcFlag;
	LOG(INFO) << "the first gamma_R comp data is :" << *pGmCompMat->burnData_R;
	LOG(INFO) << "the first gamma_G comp data is :" << *pGmCompMat->burnData_G;
	LOG(INFO) << "the first gamma_B comp data is :" << *pGmCompMat->burnData_B;
	if (c2b.run_GM(strBinName, pGmCompMat) == 0) {
		LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode,GM2bin fail!\n";
		return 0;
	}
  }
  // Step5: 
  // IQC data
  //GmProcFlag=3：VD IQC
  if (binParam.GmProcFlag == 3) {
    GmCompMat* pGmCompMat = CreateGmCompMat(
        binParam.IqcAddress, binParam.GMInputBits,
                    binParam.GMOutputBits);
    pGmCompMat->burnData_R = (int*)(burnBinData.GM_R_burnDataMemoryADD);
    pGmCompMat->burnData_G = (int*)(burnBinData.GM_G_burnDataMemoryADD);
    pGmCompMat->burnData_B = (int*)(burnBinData.GM_B_burnDataMemoryADD);
	pGmCompMat->GmType = binParam.GmProcFlag;
	LOG(INFO) << "the first gamma_R comp data is :" << *pGmCompMat->burnData_R;
	LOG(INFO) << "the first gamma_G comp data is :" << *pGmCompMat->burnData_G;
	LOG(INFO) << "the first gamma_B comp data is :" << *pGmCompMat->burnData_B;
	if (c2b.run_GM(strBinName, pGmCompMat) == 0) {
		LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode,GM2bin fail!\n";
		return 0;
	}
  }

  LOG(INFO) << ">>   Exit BinCreatFromMatAddressAndJS Mode\n";
  return 1;
}

DLLAPI_EXPORT int __stdcall BinCreat_JSON(
	const char* JsonString,
	const char* saveDir,
	const char* Plane_SN,
	BinData burnBinData,
	char** p_binDir){
	string temp;
	time_t rawtime;
	struct tm timeinfo;//
	Json::Reader reader;
	Json::Value root;
	BinParam binParam;
	char timebuf[64];
	int strlen = 0;
	//// Load configuration from file
	el::Configurations conf("log.conf");
	//// Actually reconfigure all loggers instead
	el::Loggers::reconfigureAllLoggers(conf);

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	LOG(INFO) << "<<   Enter BinCreat_JSON Mode";
	LOG(INFO) << GetDLLVersion();
	mktime(&timeinfo);
	//strlen = strftime(timebuf, sizeof(timebuf), "%y%W", &timeinfo);
	strlen = strftime(timebuf, sizeof(timebuf), "%g%V", &timeinfo);   //%g%V：按ISO8601 获取当前年and周次
	if (strlen != 0) {
		binParam.Week = (char*)malloc(strlen * sizeof(char));
		strlen += 1;
		memcpy(binParam.Week, timebuf, strlen);
		LOG(INFO) << "Week: " << binParam.Week;
	}

	if (saveDir == nullptr) {
		LOG(INFO) << "csvPath ==null!";
		return 0;
	}
	
	if (ReadBinParamFromLiteJson(JsonString, binParam) == 0) {
		LOG(INFO) << ">>   Exit BinCreat_JSON Mode,JSON string err!\n";
		return 0;
	}
	searchFileJson(binParam.TconConfigDir, binParam.TconIndex, binParam);
	Csv2bin c2b;
	switch (binParam.TconType) {
	case uTCONtype::uCSOT_FORMAT:
		if (binParam.GmProcFlag == 1) binParam.BinSize = 0x100000;
		else binParam.BinSize = 0x1000;
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT);
		break;
	case uTCONtype::uNT_71102:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71102);
		break;
	case uTCONtype::uNT_71267:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71267);
		break;
	case uTCONtype::uVD_Format:
		c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
		break;
	case uTCONtype::uNT_71733B:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71733B);
		break;
	case uTCONtype::uNT_71265:
		c2b.setPTCONabstract(DemuraTCONtype::NT_71265);
		break;
	case uTCONtype::uCSOT_FORMAT_Dual:
		c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT_Dual);
		break;
	case uTCONtype::uEU3:
		c2b.setPTCONabstract(DemuraTCONtype::EU_3);
		break;
	case uTCONtype::uOscar:
		c2b.setPTCONabstract(DemuraTCONtype::Oscar_Single);
		break;
	case uTCONtype::uOscar_Dual:
		c2b.setPTCONabstract(DemuraTCONtype::Oscar_Double);
		break;
	case uTCONtype::uVD_Y21_8K:
		c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
		break;
	default:
		break;
	}
	// Step0:
	// creat the bin file path and file name
	//string strBinName = c2b.GenerateBinName(csvPath);
	binParam.BinName = (char*)malloc(34 * sizeof(char));
	sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d%s",
					  binParam.BlockSizeH, binParam.BlockSizeV, binParam.FileLastName);
	
	LOG(INFO) << "BinName:  " << binParam.BinName;
	if (binParam.BinName == "" || strstr(binParam.BinName, ".bin") == NULL)
	{
		LOG(INFO) << "JasonSring BinName err!";

		return 0;  // INPUT_NULL_PONTER;
	}
	string strBinName = saveDir;
	strBinName.append(Plane_SN);
	strBinName.append(binParam.BinName);


	// Step1:
	// ceater empty bin file
	ofstream outfile;
	outfile.open(strBinName, ios::out | ios::binary);
	char* buffer = (char*)malloc(sizeof(char*) * binParam.BinSize);
	memset(buffer, 0xFF, binParam.BinSize);
	outfile.write(buffer, binParam.BinSize);
	outfile.close();
	if (buffer != nullptr) {
		free(buffer);
		buffer = nullptr;
	}


	// Step2:
	// Demura data
	if (binParam.DemuraProcFlag == 1) {
		CompMat* pCompMat = CreateAllCompMat(binParam.PlaneNum, binParam.Plane, binParam.channel,
			binParam.imageV, binParam.imageH, binParam.H_total, binParam.V_total, binParam.LowBound,
			binParam.HighBound, binParam.DemuraAddress);
		if (pCompMat->burnData != nullptr) {
			free(pCompMat->burnData);
			pCompMat->burnData = nullptr;
		}
		pCompMat->burnData = (float*)(burnBinData.DM_burnDataMemoryADD);
		LOG(INFO) << "the first Demura comp data is :" << *pCompMat->burnData;

		if (c2b.run(strBinName, pCompMat) == 0) {
			LOG(INFO) << ">>   Exit BinCreat_JSON Mode,Demura to bin fail!\n";
			return 0;
		}
	}

	// Step3: 
	// VCOM data
	if (binParam.VcomProcFlag == 1 || binParam.CellInforFlag == 1) {
		PMicCompMat* pPMicCompMat =
			CreatePMicCompMat(binParam.VcomAddress, binParam.DefaultVcom);
		if (burnBinData.VcomData == -1 || binParam.VcomProcFlag == 0)
			pPMicCompMat->burnData[0] = binParam.DefaultVcom;
		else
			pPMicCompMat->burnData[0] = burnBinData.VcomData;
		pPMicCompMat->PMIC_CodeDir = binParam.PMicCodeDir;
		int size = 96 + 1;
		pPMicCompMat->CellInfo = (char*)malloc(96 * sizeof(char));
		if (pPMicCompMat->CellInfo) {
			for (int i = 0; i < 96; i++) {
				pPMicCompMat->CellInfo[i] = 0;
			}
			int start = 0;
			for (int i = 0; i < 10; i++) {
				pPMicCompMat->CellInfo[start + i] = binParam.CellCode[i];
				if (pPMicCompMat->CellInfo[start + i] == '\0')
					break;
			}
			start = 16;
			for (int i = 0; i < 3; i++) {
				pPMicCompMat->CellInfo[start + i] = binParam.Revision[i];
				if (pPMicCompMat->CellInfo[start + i] == '\0')
					break;
			}
			start = 24;
			for (int i = 0; i < 4; i++) {
				pPMicCompMat->CellInfo[start + i] = binParam.Week[i];
				if (pPMicCompMat->CellInfo[start + i] == '\0')
					break;
			}
			start = 32;
			for (int i = 0; i < 12; i++) {
				pPMicCompMat->CellInfo[start + i] = Plane_SN[i];
				if (pPMicCompMat->CellInfo[start + i] == '\0')
					break;
			}
			start = 80;
			for (int i = 0; i < 10; i++) {
				pPMicCompMat->CellInfo[start + i] = binParam.ModelCode[i];
				if (pPMicCompMat->CellInfo[start + i] == '\0')
					break;
			}
			if (c2b.run_PMIC(strBinName, pPMicCompMat) == 0) {
				if (pPMicCompMat->CellInfo != nullptr) {
					free(pPMicCompMat->CellInfo);
					pPMicCompMat->CellInfo = nullptr;
				}
				LOG(INFO) << ">>   Exit BinCreat_JSON Mode,PMIC2bin fail!\n";
				return 0;
			}
			else
			{
				if (pPMicCompMat->CellInfo != nullptr) {
					free(pPMicCompMat->CellInfo);
					pPMicCompMat->CellInfo = nullptr;
				}
			}
		}
		else {
			LOG(INFO) << "PMIC to bin fail,malloc fail!";
			LOG(INFO) << ">>   Exit BinCreat_JSON Mode,PMIC2bin fail!\n";
			return 0;
		}

	}
	// Step4: 
	// ACC data 
	//GmProcFlag=1：VD 60Hz
	//GmProcFlag=2：VD120Hz
	if (binParam.GmProcFlag == 1 || binParam.GmProcFlag == 2) {

		GmCompMat* pGmCompMat = CreateGmCompMat(
			binParam.AccAddress, binParam.GMInputBits,
			binParam.GMOutputBits);
		pGmCompMat->burnData_R = (int*)(burnBinData.GM_R_burnDataMemoryADD);
		pGmCompMat->burnData_G = (int*)(burnBinData.GM_G_burnDataMemoryADD);
		pGmCompMat->burnData_B = (int*)(burnBinData.GM_B_burnDataMemoryADD);
		pGmCompMat->GmType = binParam.GmProcFlag;
		LOG(INFO) << "the first gamma_R comp data is :" << *pGmCompMat->burnData_R;
		LOG(INFO) << "the first gamma_G comp data is :" << *pGmCompMat->burnData_G;
		LOG(INFO) << "the first gamma_B comp data is :" << *pGmCompMat->burnData_B;
		if (c2b.run_GM(strBinName, pGmCompMat) == 0) {
			LOG(INFO) << ">>   Exit BinCreat_JSON Mode,GM2bin fail!\n";
			return 0;
		}
	}
	// Step5: 
	// IQC data
	//GmProcFlag=3：VD IQC
	if (binParam.GmProcFlag == 3) {
		GmCompMat* pGmCompMat = CreateGmCompMat(
			binParam.IqcAddress, binParam.GMInputBits,
			binParam.GMOutputBits);
		pGmCompMat->burnData_R = (int*)(burnBinData.GM_R_burnDataMemoryADD);
		pGmCompMat->burnData_G = (int*)(burnBinData.GM_G_burnDataMemoryADD);
		pGmCompMat->burnData_B = (int*)(burnBinData.GM_B_burnDataMemoryADD);
		pGmCompMat->GmType = binParam.GmProcFlag;
		LOG(INFO) << "the first gamma_R comp data is :" << *pGmCompMat->burnData_R;
		LOG(INFO) << "the first gamma_G comp data is :" << *pGmCompMat->burnData_G;
		LOG(INFO) << "the first gamma_B comp data is :" << *pGmCompMat->burnData_B;
		if (c2b.run_GM(strBinName, pGmCompMat) == 0) {
			LOG(INFO) << ">>   Exit BinCreat_JSON Mode,GM2bin fail!\n";
			return 0;
		}
	}
	if (binParam.TconNum != 2) {
		string SaveName = strBinName;
		size_t size = SaveName.size() + 1;
		memcpy(p_binDir[0], SaveName.c_str(), size);
		LOG(INFO) << ">>   Set binDir:" << p_binDir[0];
	}
	else {
		string SaveName = strBinName;
		SaveName.replace(SaveName.find(".bin"), 4, "_1.bin");
		size_t size = SaveName.size() + 1;
		memcpy(p_binDir[0], SaveName.c_str(), size);
		LOG(INFO) << ">>   Set binDir1:" << p_binDir[0];
		SaveName = strBinName;
		SaveName.replace(SaveName.find(".bin"), 4, "_2.bin");
		size = SaveName.size() + 1;
		memcpy(p_binDir[1], SaveName.c_str(), size);
		LOG(INFO) << ">>   Set binDir2:" << p_binDir[1];
	}
	

	LOG(INFO) << ">>   Exit BinCreat_JSON Mode\n";
	return 1;

}

DLLAPI_EXPORT int __stdcall BinCreatJSON(
	const char* JsonString,
	const char* saveDir,
	const char* Plane_SN,
	BinData burnBinData,
	char* p_binDir) {
	string temp;
	time_t rawtime;
	struct tm timeinfo;//
	Json::Reader reader;
	Json::Value root;
	BinParam binParam;
	char timebuf[64];
	int strlen = 0;
	//// Load configuration from file
	el::Configurations conf("log.conf");
	//// Actually reconfigure all loggers instead
	el::Loggers::reconfigureAllLoggers(conf);

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	LOG(INFO) << "<<   Enter BinCreat_JSON Mode";
	LOG(INFO) << GetDLLVersion();
	mktime(&timeinfo);
	//strlen = strftime(timebuf, sizeof(timebuf), "%y%W", &timeinfo);
	strlen = strftime(timebuf, sizeof(timebuf), "%g%V", &timeinfo);   //%g%V：按ISO8601 获取当前年and周次
	if (strlen != 0) {
		binParam.Week = (char*)malloc(strlen * sizeof(char));
		strlen += 1;
		memcpy(binParam.Week, timebuf, strlen);
		LOG(INFO) << "Week: " << binParam.Week;
	}

	if (saveDir == nullptr) {
		LOG(INFO) << "csvPath ==null!";
		return 0;
	}
	try {
		if (ReadBinParamFromLiteJson(JsonString, binParam) == 0) {
			LOG(INFO) << ">>   Exit BinCreat_JSON Mode,JSON string err!\n";
			return 0;
		}
		if (searchFileJson(binParam.TconConfigDir, binParam.TconIndex, binParam) == 0) {
			LOG(INFO) << ">>   Exit BinCreat_JSON Mode,Tcon_configJSON.json string err!\n";
			return 0;
		}
		Csv2bin c2b;
		switch (binParam.TconType) {
		case uTCONtype::uCSOT_FORMAT:
			if (binParam.GmProcFlag == 1) binParam.BinSize = 0x100000;
			else binParam.BinSize = 0x1000;
			c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT);
			break;
		case uTCONtype::uNT_71102:
			c2b.setPTCONabstract(DemuraTCONtype::NT_71102);
			break;
		case uTCONtype::uNT_71267:
			c2b.setPTCONabstract(DemuraTCONtype::NT_71267);
			break;
		case uTCONtype::uVD_Format:
			c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
			break;
		case uTCONtype::uNT_71733B:
			c2b.setPTCONabstract(DemuraTCONtype::NT_71733B);
			break;
		case uTCONtype::uNT_71265:
			c2b.setPTCONabstract(DemuraTCONtype::NT_71265);
			break;
		case uTCONtype::uCSOT_FORMAT_Dual:
			c2b.setPTCONabstract(DemuraTCONtype::CSOT_FORMAT_Dual);
			break;
		case uTCONtype::uEU3:
			c2b.setPTCONabstract(DemuraTCONtype::EU_3);
			break;
		case uTCONtype::uOscar:
			c2b.setPTCONabstract(DemuraTCONtype::Oscar_Single);
			break;
		case uTCONtype::uOscar_Dual:
			c2b.setPTCONabstract(DemuraTCONtype::Oscar_Double);
			break;
		case uTCONtype::uVD_Y21_8K:
			c2b.setPTCONabstract(DemuraTCONtype::VD_8K_Format);
			break;
		default:
			break;
		}
		// Step0:
		// creat the bin file path and file name
		//string strBinName = c2b.GenerateBinName(csvPath);
		binParam.BinName = (char*)malloc(34 * sizeof(char));
		sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d%s",
			binParam.BlockSizeH, binParam.BlockSizeV, binParam.FileLastName);

		LOG(INFO) << "BinName:  " << binParam.BinName;
		if (binParam.BinName == "" || strstr(binParam.BinName, ".bin") == NULL)
		{
			LOG(INFO) << "JasonSring BinName err!";

			return 0;  // INPUT_NULL_PONTER;
		}
		string strBinName = saveDir;
		strBinName.append(Plane_SN);
		strBinName.append(binParam.BinName);


		// Step1:
		// ceater empty bin file
		ofstream outfile;
		outfile.open(strBinName, ios::out | ios::binary);
		char* buffer = (char*)malloc(sizeof(char*) * binParam.BinSize);
		memset(buffer, 0xFF, binParam.BinSize);
		outfile.write(buffer, binParam.BinSize);
		outfile.close();
		if (buffer != nullptr) {
			free(buffer);
			buffer = nullptr;
		}


		// Step2:
		// Demura data
		if (binParam.DemuraProcFlag == 1) {
			CompMat* pCompMat = CreateAllCompMat(binParam.PlaneNum, binParam.Plane, binParam.channel,
				binParam.imageV, binParam.imageH, binParam.H_total, binParam.V_total, binParam.LowBound,
				binParam.HighBound, binParam.DemuraAddress);
			if (pCompMat->burnData != nullptr) {
				free(pCompMat->burnData);
				pCompMat->burnData = nullptr;
			}
			pCompMat->burnData = (float*)(burnBinData.DM_burnDataMemoryADD);
			LOG(INFO) << "the first Demura comp data is :" << *pCompMat->burnData;

			if (c2b.run(strBinName, pCompMat) == 0) {
				LOG(INFO) << ">>   Exit BinCreat_JSON Mode,Demura to bin fail!\n";
				return 0;
			}
		}

		// Step3: 
		// VCOM data
		if (binParam.VcomProcFlag == 1 || binParam.CellInforFlag == 1) {
			PMicCompMat* pPMicCompMat =
				CreatePMicCompMat(binParam.VcomAddress, binParam.DefaultVcom);
			if (burnBinData.VcomData == -1 || binParam.VcomProcFlag == 0)
				pPMicCompMat->burnData[0] = binParam.DefaultVcom;
			else
				pPMicCompMat->burnData[0] = burnBinData.VcomData;
			pPMicCompMat->PMIC_CodeDir = binParam.PMicCodeDir;
			int size = 96 + 1;
			pPMicCompMat->CellInfo = (char*)malloc(96 * sizeof(char));
			if (pPMicCompMat->CellInfo) {
				for (int i = 0; i < 96; i++) {
					pPMicCompMat->CellInfo[i] = 0;
				}
				int start = 0;
				for (int i = 0; i < 10; i++) {
					pPMicCompMat->CellInfo[start + i] = binParam.CellCode[i];
					if (pPMicCompMat->CellInfo[start + i] == '\0')
						break;
				}
				start = 16;
				for (int i = 0; i < 3; i++) {
					pPMicCompMat->CellInfo[start + i] = binParam.Revision[i];
					if (pPMicCompMat->CellInfo[start + i] == '\0')
						break;
				}
				start = 24;
				for (int i = 0; i < 4; i++) {
					pPMicCompMat->CellInfo[start + i] = binParam.Week[i];
					if (pPMicCompMat->CellInfo[start + i] == '\0')
						break;
				}
				start = 32;
				for (int i = 0; i < 12; i++) {
					pPMicCompMat->CellInfo[start + i] = Plane_SN[i];
					if (pPMicCompMat->CellInfo[start + i] == '\0')
						break;
				}
				start = 80;
				for (int i = 0; i < 10; i++) {
					pPMicCompMat->CellInfo[start + i] = binParam.ModelCode[i];
					if (pPMicCompMat->CellInfo[start + i] == '\0')
						break;
				}
				if (c2b.run_PMIC(strBinName, pPMicCompMat) == 0) {
					if (pPMicCompMat->CellInfo != nullptr) {
						free(pPMicCompMat->CellInfo);
						pPMicCompMat->CellInfo = nullptr;
					}
					LOG(INFO) << ">>   Exit BinCreat_JSON Mode,PMIC2bin fail!\n";
					return 0;
				}
				else
				{
					if (pPMicCompMat->CellInfo != nullptr) {
						free(pPMicCompMat->CellInfo);
						pPMicCompMat->CellInfo = nullptr;
					}
				}
			}
			else {
				LOG(INFO) << "PMIC to bin fail,malloc fail!";
				LOG(INFO) << ">>   Exit BinCreat_JSON Mode,PMIC2bin fail!\n";
				return 0;
			}

		}
		// Step4: 
		// ACC data 
		//GmProcFlag=1：VD 60Hz
		//GmProcFlag=2：VD120Hz
		if (binParam.GmProcFlag == 1 || binParam.GmProcFlag == 2) {

			GmCompMat* pGmCompMat = CreateGmCompMat(
				binParam.AccAddress, binParam.GMInputBits,
				binParam.GMOutputBits);
			pGmCompMat->burnData_R = (int*)(burnBinData.GM_R_burnDataMemoryADD);
			pGmCompMat->burnData_G = (int*)(burnBinData.GM_G_burnDataMemoryADD);
			pGmCompMat->burnData_B = (int*)(burnBinData.GM_B_burnDataMemoryADD);
			pGmCompMat->GmType = binParam.GmProcFlag;
			LOG(INFO) << "the first gamma_R comp data is :" << *pGmCompMat->burnData_R;
			LOG(INFO) << "the first gamma_G comp data is :" << *pGmCompMat->burnData_G;
			LOG(INFO) << "the first gamma_B comp data is :" << *pGmCompMat->burnData_B;
			if (c2b.run_GM(strBinName, pGmCompMat) == 0) {
				LOG(INFO) << ">>   Exit BinCreat_JSON Mode,GM2bin fail!\n";
				return 0;
			}
		}
		// Step5: 
		// IQC data
		//GmProcFlag=3：VD IQC
		if (binParam.GmProcFlag == 3) {
			GmCompMat* pGmCompMat = CreateGmCompMat(
				binParam.IqcAddress, binParam.GMInputBits,
				binParam.GMOutputBits);
			pGmCompMat->burnData_R = (int*)(burnBinData.GM_R_burnDataMemoryADD);
			pGmCompMat->burnData_G = (int*)(burnBinData.GM_G_burnDataMemoryADD);
			pGmCompMat->burnData_B = (int*)(burnBinData.GM_B_burnDataMemoryADD);
			pGmCompMat->GmType = binParam.GmProcFlag;
			LOG(INFO) << "the first gamma_R comp data is :" << *pGmCompMat->burnData_R;
			LOG(INFO) << "the first gamma_G comp data is :" << *pGmCompMat->burnData_G;
			LOG(INFO) << "the first gamma_B comp data is :" << *pGmCompMat->burnData_B;
			if (c2b.run_GM(strBinName, pGmCompMat) == 0) {
				LOG(INFO) << ">>   Exit BinCreat_JSON Mode,GM2bin fail!\n";
				return 0;
			}
		}

		if (binParam.TconNum != 2) {
			string SaveName = strBinName;
			size_t size = SaveName.size() + 1;
			//memcpy(p_binDir, SaveName.c_str(), size);
			sprintf_s(p_binDir, size + 100, "{\"binAdd\":[\"%s\"]}", SaveName.c_str());
			LOG(INFO) << ">>   Set binDir:" << p_binDir;
		}
		else {
			string SaveName1 = strBinName;
			SaveName1.replace(SaveName1.find(".bin"), 4, "_1.bin");
			size_t size1 = SaveName1.size() + 1;
			string SaveName2 = strBinName;
			SaveName2.replace(SaveName2.find(".bin"), 4, "_2.bin");
			size_t size2 = SaveName2.size() + 1;
			sprintf_s(p_binDir, size1 + size2 + 100, "{\"binAdd\":[\"%s\",\"%s\"]}", SaveName1.c_str(), SaveName2.c_str());
			LOG(INFO) << ">>   Set binDir:" << p_binDir;

		}

		LOG(INFO) << ">>   Exit BinCreat_JSON Mode\n";

		return 1;
	}
	catch (...)
	{
		LOG(INFO) << "CSV to bin fail!";
		LOG(INFO) << ">>   Exit BinCreat_JSON Mode\n";
	}

}
int ReadBinParamFromJson(const char* JsonSring, BinParam& binParam) {
	
	Json::Reader reader;
	Json::Value root;
	char buff[250];
	_getcwd(buff, 250);
	std::string current_working_directory(buff);

	LOG(INFO) << "current_working_directory:" << current_working_directory.c_str();

	if (JsonSring == nullptr) {
          LOG(INFO) << "JsonSring == nullptr!";
          return 0;  // INPUT_NULL_PONTER;
        }
	//从Jason字符串中读取数据
	if (reader.parse(JsonSring, root)) {
		binParam.imageH = root["$PANEL$PanelResulotionWidth"].asInt();
		LOG(INFO) << "imageH:" << binParam.imageH;

		binParam.imageV = root["$PANEL$PanelResulotionHeight"].asInt();
		LOG(INFO) << "imageV:" << binParam.imageV;

		if (root["$DEMURA$compMode"].asInt() == 1) {
                  binParam.channel = 3;
                  LOG(INFO)
                      << "Demura_compMode:" << root["$DEMURA$compMode"].asInt();
                }
        else {
                  binParam.channel = 1;
                  LOG(INFO) << "Demura_compMode:" << root["$DEMURA$compMode"].asInt();
		}
		LOG(INFO) << "Demura_channel:" << binParam.channel;

		binParam.HighBound = root["$DEMURA$upperLimit"].asInt() * 4 + 4;
		if (binParam.HighBound > 1023)
			binParam.HighBound = 1023;
		LOG(INFO) << "Demura_HighBound:" << binParam.HighBound;

		binParam.LowBound = root["$DEMURA$lowerLimit"].asInt() * 4;
		LOG(INFO) << "Demura_LowBound:" << binParam.LowBound;

		binParam.BlockSizeH = root["$DEMURA$blockSizeWidth"].asInt();
        LOG(INFO) << "Demura_BlockSizeH:" << binParam.BlockSizeH;

        binParam.BlockSizeV = root["$DEMURA$blockSizeHeight"].asInt();
        LOG(INFO) << "Demura_BlockSizeV:" << binParam.BlockSizeV;

		binParam.H_total = binParam.imageH / binParam.BlockSizeH + 1;
		LOG(INFO) << "Demura_H_total:" << binParam.H_total;

		binParam.V_total = binParam.imageV / binParam.BlockSizeV + 1;
		LOG(INFO) << "Demura_V_total:" << binParam.V_total;

		binParam.PlaneNum = root["$DEMURA$compNum"].asInt();
		LOG(INFO) << "Demura_PlaneNum:" << binParam.PlaneNum;

		for (unsigned int i = 0; i < 5; i++) {
			binParam.Plane[i] = 0;
		}
		for (unsigned int i = 0; i < root["$DEMURA$compGrayList"].size(); i++) {
			binParam.Plane[i] = root["$DEMURA$compGrayList"][i].asInt();
		}
		LOG(INFO) << "Planes = " << binParam.Plane[0]<< " " << binParam.Plane[1] << " " << binParam.Plane[2] << " " << binParam.Plane[3] << " " << binParam.Plane[4];

		binParam.GMInputBits = root["$GAMMA$inBits"].asInt();       //需要与采灵确认参数名称
		LOG(INFO) << "GMInputBits: " << binParam.GMInputBits;

		binParam.GMOutputBits = root["$GAMMA$outBits"].asInt();    //需要与采灵确认参数名称
		LOG(INFO) << "GMOutputBits: " << binParam.GMOutputBits;

		binParam.VcomProcFlag = root["$DEMURA$BIN$FUNCTION$VCOM"].asInt();
		LOG(INFO) << "VcomProcFlag:  " << binParam.VcomProcFlag;

		binParam.DemuraProcFlag = root["$DEMURA$BIN$FUNCTION$Demura"].asInt();     //需要格创Gamma和Demura是否启用的标志
		LOG(INFO) << "DemuraProcFlag:  " << binParam.DemuraProcFlag;

		binParam.GmProcFlag = root["$DEMURA$BIN$FUNCTION$GmType"].asInt();
		LOG(INFO) << "GmProcFlag:  " << binParam.GmProcFlag;

		binParam.TconType = root["$DEMURA$BIN$FUNCTION$TconType"].asInt();
		LOG(INFO) << "TconType:  " << binParam.TconType;

		binParam.BinName = (char*)malloc(34 * sizeof(char));
		switch (binParam.TconType) {
		case uTCONtype::uCSOT_FORMAT:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_HSV532.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 0;
			if (binParam.GmProcFlag == 1) binParam.BinSize = 0x100000;
			else binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.VcomAddress = 0xFE000;
			LOG(INFO) << "VcomAddress:  " << hex << binParam.VcomAddress;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			binParam.AccAddress = 0xEB000;
			LOG(INFO) << "AccAddress:  " << hex << binParam.AccAddress;
			break;

		case uTCONtype::uNT_71102:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_NT71102.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;

		case uTCONtype::uNT_71267:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_NT71267.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;
			break;

		case uTCONtype::uVD_Format:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_NIKEL.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 1;
			binParam.BinSize = 0x400000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.VcomAddress = 0x61000;
			LOG(INFO) << "VcomAddress:  " << hex << binParam.VcomAddress;

			binParam.DemuraAddress = 0x63000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			binParam.AccAddress = 0x373000;
			LOG(INFO) << "AccAddress:  " << hex << binParam.AccAddress;

			binParam.IqcAddress = 0x388000;
			LOG(INFO) << "IqcAddress:  " << hex << binParam.IqcAddress;
			break;

		case uTCONtype::uNT_71733B:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_NT71733.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;

		case uTCONtype::uNT_71265:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_NT71265.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;

		case uTCONtype::uCSOT_FORMAT_Dual:
			/*sprintf_s(binParam.BinName, 34,
					  "_PIXEL_%dX%d_HSV532_Dual.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.VcomAddress = 0xFE000;
			LOG(INFO) << "VcomAddress:  " << hex << binParam.VcomAddress;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			binParam.AccAddress = 0xEB000;
			LOG(INFO) << "AccAddress:  " << hex << binParam.AccAddress;
			break;
		case uTCONtype::uEU3:

			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x003AE4;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;
		case uTCONtype::uOscar:

			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;
		case uTCONtype::uOscar_Dual:

			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;
		case uTCONtype::uVD_Y21_8K:
			/*sprintf_s(binParam.BinName, 34, "_PIXEL_%dX%d_NIKEL.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 1;
			binParam.BinSize = 0x400000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.VcomAddress = 0x61000;
			LOG(INFO) << "VcomAddress:  " << hex << binParam.VcomAddress;

			binParam.DemuraAddress = 0x00000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			binParam.AccAddress = 0x373000;
			LOG(INFO) << "AccAddress:  " << hex << binParam.AccAddress;

			binParam.IqcAddress = 0x388000;
			LOG(INFO) << "IqcAddress:  " << hex << binParam.IqcAddress;
			break;
		default:
			/*sprintf_s(binParam.BinName, 34,
					  "_PIXEL_%dX%d_TCONtypeNotFound.bin",
					  binParam.BlockSizeH, binParam.BlockSizeV);*/
			binParam.CellInforFlag = 0;
			binParam.BinSize = 0x1000;
			LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;

			binParam.DemuraAddress = 0x000000;
			LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;

			break;
		}
        //LOG(INFO) << "BinName:  " << binParam.BinName;

        string PMicCodeDir = root["$SETUP$PMicCodeDir"].asString();
        string PMicCodeName = root["$DEMURA$BIN$PMicCodeName"].asString();
        PMicCodeDir.append(PMicCodeName);
        size_t size = PMicCodeDir.size() + 1;
        binParam.PMicCodeDir = (char*)malloc(PMicCodeDir.size() * sizeof(char));
		memcpy(binParam.PMicCodeDir, PMicCodeDir.c_str(), size);
		LOG(INFO) << "PMicCodeDir:  " << binParam.PMicCodeDir;

		string CellCode = root["$DEMURA$BIN$CellCode"].asString();
		size = CellCode.size() + 1;
		binParam.CellCode = (char*)malloc(CellCode.size() * sizeof(char));
		memcpy(binParam.CellCode, CellCode.c_str(), size);
		LOG(INFO) << "CellCode:  " << binParam.CellCode;

		string ModelCode = root["$DEMURA$BIN$ModelCode"].asString();
		size = ModelCode.size() + 1;
		binParam.ModelCode = (char*)malloc(ModelCode.size() * sizeof(char));
		memcpy(binParam.ModelCode, ModelCode.c_str(), size);
		LOG(INFO) << "ModelCode:  " << binParam.ModelCode;

		string Revision = root["$DEMURA$BIN$Revision"].asString();
		size = Revision.size() + 1;
		binParam.Revision = (char*)malloc(Revision.size() * sizeof(char));
		memcpy(binParam.Revision, Revision.c_str(), size);
		LOG(INFO) << "Revision:  " << binParam.Revision;

		string BinName = root["$DEMURA$BIN$Name"].asString();        //这个还要改
		size = BinName.size() + 1;
		binParam.BinName = (char*)malloc(BinName.size() * sizeof(char));

		memcpy(binParam.BinName, BinName.c_str(), size);
		LOG(INFO) << "BinName:  " << binParam.BinName;
		if (binParam.BinName == "" || strstr(BinName.c_str(), ".bin") == NULL)
		{
			LOG(INFO) << "JasonSring BinName err!";

			return 0;  // INPUT_NULL_PONTER;
		}

		binParam.DefaultVcom = root["$DEMURA$BIN$PMIC$DefaultVCOM"].asInt();
		LOG(INFO) << "Default VCOM:  " << binParam.DefaultVcom;

		return 1;
	}
	else{
	    LOG(INFO) << "JasonSring read err! " ;

		return 0;  // INPUT_NULL_PONTER;
	}

}
int ReadBinParamFromLiteJson(const char* JsonSring, BinParam& binParam) {

	Json::Reader reader;
	Json::Value root;
	char buff[250];
	_getcwd(buff, 250);
	std::string current_working_directory(buff);

	LOG(INFO) << "current_working_directory:" << current_working_directory.c_str();

	if (JsonSring == nullptr) {
		LOG(INFO) << "JsonSring == nullptr!";
		return 0;  // INPUT_NULL_PONTER;
	}
	//从Jason字符串中读取数据
	try {
		if (reader.parse(JsonSring, root)) {


			binParam.imageH = root["$PANEL$PanelResulotionWidth"].asInt();
			LOG(INFO) << "imageH:" << binParam.imageH;

			binParam.imageV = root["$PANEL$PanelResulotionHeight"].asInt();
			LOG(INFO) << "imageV:" << binParam.imageV;

			if (root["$DEMURA$compMode"].asInt() == 1) {
				binParam.channel = 3;
				LOG(INFO)
					<< "Demura_compMode:" << root["$DEMURA$compMode"].asInt();
			}
			else {
				binParam.channel = 1;
				LOG(INFO) << "Demura_compMode:" << root["$DEMURA$compMode"].asInt();
			}
			LOG(INFO) << "Demura_channel:" << binParam.channel;

			binParam.HighBound = root["$DEMURA$upperLimit"].asInt() * 4 + 4;
			if (binParam.HighBound > 1023)
				binParam.HighBound = 1023;
			LOG(INFO) << "Demura_HighBound:" << binParam.HighBound;

			binParam.LowBound = root["$DEMURA$lowerLimit"].asInt() * 4;
			LOG(INFO) << "Demura_LowBound:" << binParam.LowBound;

			binParam.BlockSizeH = root["$DEMURA$blockSizeWidth"].asInt();
			LOG(INFO) << "Demura_BlockSizeH:" << binParam.BlockSizeH;

			binParam.BlockSizeV = root["$DEMURA$blockSizeHeight"].asInt();
			LOG(INFO) << "Demura_BlockSizeV:" << binParam.BlockSizeV;

			binParam.H_total = binParam.imageH / binParam.BlockSizeH + 1;
			LOG(INFO) << "Demura_H_total:" << binParam.H_total;

			binParam.V_total = binParam.imageV / binParam.BlockSizeV + 1;
			LOG(INFO) << "Demura_V_total:" << binParam.V_total;

			binParam.PlaneNum = root["$DEMURA$compNum"].asInt();
			LOG(INFO) << "Demura_PlaneNum:" << binParam.PlaneNum;

			for (unsigned int i = 0; i < 5; i++) {
				binParam.Plane[i] = 0;
			}
			for (unsigned int i = 0; i < root["$DEMURA$compGrayList"].size(); i++) {
				binParam.Plane[i] = root["$DEMURA$compGrayList"][i].asInt();
			}
			LOG(INFO) << "Planes = " << binParam.Plane[0] << " " << binParam.Plane[1] << " " << binParam.Plane[2] << " " << binParam.Plane[3] << " " << binParam.Plane[4];

			binParam.GMInputBits = root["$GAMMA$inBits"].asInt();       //需要与采灵确认参数名称
			LOG(INFO) << "GMInputBits: " << binParam.GMInputBits;

			binParam.GMOutputBits = root["$GAMMA$outBits"].asInt();    //需要与采灵确认参数名称
			LOG(INFO) << "GMOutputBits: " << binParam.GMOutputBits;

			binParam.TconIndex = root["$DEMURA$BIN$TconIndex"].asInt();
			LOG(INFO) << "TconIndex:  " << binParam.TconIndex;

			binParam.VcomProcFlag = root["$DEMURA$BIN$FUNCTION$VCOM"].asInt();
			LOG(INFO) << "VcomProcFlag:  " << binParam.VcomProcFlag;

			binParam.DemuraProcFlag = root["$DEMURA$BIN$FUNCTION$Demura"].asInt();     //需要格创Gamma和Demura是否启用的标志
			LOG(INFO) << "DemuraProcFlag:  " << binParam.DemuraProcFlag;

			binParam.GmProcFlag = root["$DEMURA$BIN$FUNCTION$GmType"].asInt();
			LOG(INFO) << "GmProcFlag:  " << binParam.GmProcFlag;

			string CellCode = root["$DEMURA$BIN$CellCode"].asString();
			size_t size = CellCode.size() + 1;
			binParam.CellCode = (char*)malloc(CellCode.size() * sizeof(char));
			memcpy(binParam.CellCode, CellCode.c_str(), size);
			LOG(INFO) << "CellCode:  " << binParam.CellCode;

			string ModelCode = root["$DEMURA$BIN$ModelCode"].asString();
			size = ModelCode.size() + 1;
			binParam.ModelCode = (char*)malloc(ModelCode.size() * sizeof(char));
			memcpy(binParam.ModelCode, ModelCode.c_str(), size);
			LOG(INFO) << "ModelCode:  " << binParam.ModelCode;

			string Revision = root["$DEMURA$BIN$Revision"].asString();
			size = Revision.size() + 1;
			binParam.Revision = (char*)malloc(Revision.size() * sizeof(char));
			memcpy(binParam.Revision, Revision.c_str(), size);
			LOG(INFO) << "Revision:  " << binParam.Revision;

			string PMicCodeDir = root["$SETUP$PMicCodeDir"].asString();
			string PMicCodeName = root["$DEMURA$BIN$PMicCodeName"].asString();
			PMicCodeDir.append(PMicCodeName);
			size = PMicCodeDir.size() + 1;
			binParam.PMicCodeDir = (char*)malloc(PMicCodeDir.size() * sizeof(char));
			memcpy(binParam.PMicCodeDir, PMicCodeDir.c_str(), size);
			LOG(INFO) << "PMicCodeDir:  " << binParam.PMicCodeDir;

			string TconConfigDir = root["$SETUP$TconConfigDir"].asString();
			size = TconConfigDir.size() + 1;
			binParam.TconConfigDir = (char*)malloc(TconConfigDir.size() * sizeof(char));
			memcpy(binParam.TconConfigDir, TconConfigDir.c_str(), size);
			LOG(INFO) << "TconConfigDir:  " << binParam.TconConfigDir;

			binParam.DefaultVcom = root["$DEMURA$BIN$PMIC$DefaultVCOM"].asInt();
			LOG(INFO) << "Default VCOM:  " << binParam.DefaultVcom;

			return 1;
		}
		else {
			LOG(INFO) << "JasonSring read err! ";

			return 0;  // INPUT_NULL_PONTER;
		}
	}
	catch (...)
	{
		LOG(INFO) << "JasonSring err! ";
		return 0;  // INPUT_NULL_PONTER;

	}

}

int searchFileJson(const char* JsonDir, int Index, BinParam& binParam) {
	Json::Reader reader;
	Json::Value root;
	
	//从文件中读取，保证当前文件有demo.json文件
	ifstream in(JsonDir, ios::binary);
	if (!in.is_open()) {
		LOG(INFO) << "Error opening file!";
		return 0;
	}
	unsigned int i = 0;
	if (reader.parse(in, root)) {
		//读取根节点信息
		string Version = root["VERSION"].asString();
		LOG(INFO) << "Tcon_config Version is:  " << Version;

		for (i = 0; i < root["TCON"].size(); i++) {
			int ach = root["TCON"][i]["Nikname"].asInt();
			if (ach == Index) {
				LOG(INFO) << "Found Nikname:  " << ach;
				string Description = root["TCON"][i]["Description"].asString();
				LOG(INFO) << "TCON Description:  " << Description;
				binParam.TconType = root["TCON"][i]["TconType"].asInt();
				LOG(INFO) << "TconType:  " << binParam.TconType;
				binParam.TconNum = root["TCON"][i]["TconNum"].asInt();
				LOG(INFO) << "TconNum:  " << binParam.TconNum;
				binParam.BinSize = root["TCON"][i]["ADD"]["TotalSize"].asInt();
				LOG(INFO) << "BinSize:  " << hex << binParam.BinSize;
				binParam.VcomAddress = root["TCON"][i]["ADD"]["Vcom"].asInt();
				LOG(INFO) << "VcomAddress:  " << hex << binParam.VcomAddress;
				binParam.DemuraAddress = root["TCON"][i]["ADD"]["Demura"].asInt();
				LOG(INFO) << "DemuraAddress:  " << hex << binParam.DemuraAddress;
				binParam.AccAddress = root["TCON"][i]["ADD"]["ACC"].asInt();
				LOG(INFO) << "AccAddress:  " << hex << binParam.AccAddress;
				binParam.IqcAddress = root["TCON"][i]["ADD"]["IQC"].asInt();
				LOG(INFO) << "IqcAddress:  " << hex << binParam.IqcAddress;

				string FileLastName = root["TCON"][i]["FileLastName"].asString();
				size_t  size = FileLastName.size() + 1;
				binParam.FileLastName = (char*)malloc(FileLastName.size() * sizeof(char));
				memcpy(binParam.FileLastName, FileLastName.c_str(), size);
				LOG(INFO) << "BinName:  " << binParam.FileLastName;
				if (binParam.FileLastName == "" || strstr(FileLastName.c_str(), ".bin") == NULL)
				{
					LOG(INFO) << "JasonSring FileLastName err!";
					in.close();
					return 0;  // INPUT_NULL_PONTER;
				}
				binParam.CellInforFlag = root["TCON"][i]["Cell_INF"]["CellInforFlag"].asInt();
				LOG(INFO) << "CellInforFlag:  " << binParam.CellInforFlag;

				break;
			}
		}
		if (i == root["TCON"].size()) {
			LOG(INFO) << "TCON not found ! " << endl;
			in.close();
			return 0;
		}
		else {
			LOG(INFO) << "TCON found ! " << endl;
			in.close();
			return 1;
		}
	}
	else {
		LOG(INFO) << "parse error!" << endl;
	}

	in.close();
	return 0;
}