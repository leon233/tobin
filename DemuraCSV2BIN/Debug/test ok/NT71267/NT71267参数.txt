//NT71267 3plane 2TCON
	int plane[5] = { 25,60,225,0,0 };
	BinParam binParam;
	binParam.BlockSizeH = 8;
	binParam.BlockSizeV = 8;
	binParam.channel = 1;
	binParam.HighBound = 1020;
	binParam.LowBound = 12;
	binParam.H_total = 431;
	binParam.V_total = 181;
	binParam.imageH = 3440;
	binParam.imageV = 1440;
	binParam.PlaneNum = 3;