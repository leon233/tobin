﻿#pragma once

#if defined(DEMURACSV2BIN_EXPORTS)
#define DLLAPI_EXPORT __declspec(dllexport)
#else
#define DLLAPI_EXPORT __declspec(dllimport)
#endif
/*******************************************************
*支持demura 转bin的TCON 列表
********************************************************/
typedef enum {
	uNT_71102,
	uVD_Format,
	uNT_71267,
	uCSOT_FORMAT,
	uNT_71733B,
	uNT_71265,
	uCSOT_FORMAT_Dual
} uTCONtype;
/*******************************************************
*转bin过程所需的参数信息
int PlaneNum;demura 补偿的灰阶数量
int channel;补偿的通道数，mono demura：1，color demura：3 
int H_total;demura LUT 列数
int V_total;demura LUT 行数
int imageH;产品水平分辨率，UD：3840
int imageV;产品垂直分辨率，UD：2160
char BlockSizeH; //0:4x4  1:8x8   2:16x16
char BlockSizeV; //0:4x4  1:8x8   2:16x16
int LowBound;//Low limit:通常设定12@10bit
int HighBound;//High limit:通常设定1020@10bit
int Plane[5];//demura 补偿的灰阶；

********************************************************/
typedef struct BinParam {
	int PlaneNum;
	int channel;
	int H_total;
	int V_total;
	int imageH;
	int imageV;
	char BlockSizeH; //0:4x4  1:8x8   2:16x16
	char BlockSizeV; //0:4x4  1:8x8   2:16x16
	int LowBound;
	int HighBound;
	int Plane[5];
	float* burnDataIn;    //< 指向CPU内存的图像数据区
	unsigned long long burnDataAddress;
} BinParam;
class TCONabstract;

extern "C"  //__stdcallʹ��C/C++�������ܹ�����API
{
	/*******************************************************
	*CSV to bin file
	********************************************************/
	DLLAPI_EXPORT int __stdcall BinCreatApi(
		uTCONtype tconType,
		BinParam binParam,
		const char* saveDir);
	DLLAPI_EXPORT int __stdcall BinCreatFromMat(
		uTCONtype tconType,
		BinParam binParam,
		const char* saveDir);
	DLLAPI_EXPORT int __stdcall BinCreatFromMatAddress(
		uTCONtype tconType,
		BinParam binParam,
		const char* saveDir);
	DLLAPI_EXPORT char*  __stdcall GetDLLVersion();
}


