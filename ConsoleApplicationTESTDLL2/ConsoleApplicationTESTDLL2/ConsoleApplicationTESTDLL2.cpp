// ConsoleApplicationTESTDLL2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <sstream>  //
#include "json.h"
#include "DemuraCSV2BIN.h"

using namespace std;
#pragma comment(lib,"DemuraCSV2BIN.lib")
typedef struct CompMatApp {
	int compNum;            //< 待补偿的plane数量
	int compGrayList[10];   //< 待补偿的灰阶序列
	int channels;           //< 色彩通道数量
	int width;              //< 补偿图像宽，以像素为单位
	int height;             //< 补偿图像高，以像素为单位
	int nPixel;             //< 补偿图像像素个数
	int imageSize;          //< 补偿图像数据大小，以字节为单位
	int tcon_num;
	float* compData;        //< 指向CPU内存的补偿数据区
	float* compDeviceData;  //< 指向GPU内存的补偿数据区
    int R[1024];
    int G[1024];
    int B[1024];
							//最多支持5个plane补偿
	std::string levelStringList[5] = { "LEVEL 1", "LEVEL 2", "LEVEL 3", "LEVEL 4",
		"LEVEL 5" };
	//格式按照plane1 - RGB, plane2 - RGB, ......
	std::string colorStringList[3] = { "RED", "GREEN", "BLUE" };

	int burnWidth;      //< 烧录图像宽，以像素为单位
	int burnHeight;     //< 烧录图像高，以像素为单位
	int nBurnPixel;     //< 烧录图像素个数
	int burnImageSize;  //< 烧录图像数据大小，以字节为单位
	float* burnData;    //< 指向CPU内存的图像数据区
	unsigned int lut_checksum;
	unsigned int lut_size;
	unsigned int plane_mag[8];
	unsigned int plane_offset[8];

} CompMatApp;

bool ReadCSV(const char * strPanelID, CompMatApp *pCompMat)
{
	int row_cnt = 0, data_cnt = 0;
	string line;
	std::ifstream inFile(strPanelID, std::ios::in | std::ios::binary);
	if (!inFile.is_open()) {
		return false;//UN_VALID_FILE;
	}
	//检查是否开发Demura LUT 文件："Correction Data"开头
	getline(inFile, line); row_cnt++;
	if (strstr(line.c_str(), "Correction Data") == NULL) {
		inFile.close();
		return false;  //读取文件不是Demura LUT，或缺少"Correction Data"开头
	}
	else {
		getline(inFile, line); row_cnt++;
	}


	while (getline(inFile, line)) {
		string field;
		istringstream sin(line);
		//检查非有效数据列： "Blocksize" "Level"
		if (strstr(line.c_str(), "size") == NULL && strstr(line.c_str(), "Level") == NULL && strstr(line.c_str(), "Size") == NULL && strstr(line.c_str(), "LEVEL") == NULL)
			while (getline(sin, field, ',')) {
				pCompMat->burnData[data_cnt++] = atof(field.c_str());
			}
		row_cnt++;
	}
	if (row_cnt != pCompMat->burnHeight*pCompMat->compNum * 3 + 2 + 4 * pCompMat->compNum) {
		return false;  //读取数据长度不正确
	}
	inFile.close();
	return true;
}

bool ReadACCCSV(const char* strPanelID, CompMatApp* pCompMat) {
  int row_cnt = 0, data_cnt = 0;
  string line;
  std::ifstream inFile(strPanelID, std::ios::in | std::ios::binary);
  if (!inFile.is_open()) {
    return false;  // UN_VALID_FILE;
  }
  //检查是否开发Demura LUT 文件："Correction Data"开头
  getline(inFile, line);
  row_cnt++;

  while (getline(inFile, line)) {
    string field;
    istringstream sin(line);
    ////检查非有效数据列： "Blocksize" "Level"
      getline(sin, field, ',');
      getline(sin, field, ',');
      pCompMat->R[data_cnt] = atof(field.c_str());
      getline(sin, field, ',');
      pCompMat->G[data_cnt] = atof(field.c_str());
      getline(sin, field, ',');
      pCompMat->B[data_cnt] = atof(field.c_str());
      data_cnt++;
    row_cnt++;

    //WTValues = line.Split(new Char[]{','}, 5);  //把一个字符串分割成字符串数组

    //GammaLutA.r[j] = Convert.ToUInt16(WTValues[1]);
    //GammaLutA.g[j] = Convert.ToUInt16(WTValues[2]);
    //GammaLutA.b[j] = Convert.ToUInt16(WTValues[3]);
  }
  inFile.close();
  return true;
}
CompMatApp* CreateCompMat(int compNum, int* compGrayList,
	int channels, int width, int height,
	int burnWidth, int burnHeight)
{
	//CompMat CM;
	CompMatApp *pCM = new CompMatApp();
	pCM->compNum = compNum;

	for (int i = 0; i < compNum; i++) {
		pCM->compGrayList[i] = *compGrayList++;
		pCM->compGrayList[i] *= 4;
	}
	pCM->channels = channels;
	pCM->width = width;
	pCM->height = height;
	pCM->burnWidth = burnWidth;
	pCM->burnHeight = burnHeight;
	pCM->burnData = (float*)malloc(sizeof(float)*burnWidth*burnHeight*compNum * 3);
	

	return pCM;

}
int main()
{
	float ttemp = 0;
	float ttemp1 = 0;
	uint64_t address=0;
    uint64_t R_address = 0;
    uint64_t G_address = 0;
    uint64_t B_address = 0;
	float *temp=nullptr;
	float *Ptrtemp = nullptr;

	/////////////////////////////////////////
	int plane[5] = { 25,60,128 };
	BinParam binParam;
	binParam.BlockSizeH = 8;
	binParam.BlockSizeV = 8;
	binParam.channel = 3;
	binParam.HighBound = 1020;
	binParam.LowBound = 12;
	binParam.H_total = 481;
	binParam.V_total = 271;
	binParam.imageH = 1920;
	binParam.imageV = 1080;
	binParam.PlaneNum = 3;
	//Oscar 8k 120hz
	/*int plane[5] = { 25,60,128,0,0 };
	BinParam binParam;
	binParam.BlockSizeH = 8;
	binParam.BlockSizeV = 8;
	binParam.channel = 3;
	binParam.HighBound = 1023;
	binParam.LowBound = 0;
	binParam.H_total = 961;
	binParam.V_total = 541;
	binParam.imageH = 7680;
	binParam.imageV = 4320;
	binParam.PlaneNum = 3;*/

	for(int i=0;i<5;i++)
	{
		binParam.Plane[i] = plane[i];
	}

	CompMatApp *pCompMat = CreateCompMat(binParam.PlaneNum, binParam.Plane,
		binParam.channel, binParam.imageV, binParam.imageH,
		binParam.H_total, binParam.V_total);

	/*ReadACCCSV("../../DemuraCSV2BIN/Debug/OldWT.csv", pCompMat);

	char *csvPath = "../../DemuraCSV2BIN/Debug/Demura.csv";*/

	ReadACCCSV("../../DemuraCSV2BIN/Debug/8pcs 55D16/0303 IQC/IN0B_camera_64v3_accLut.csv", pCompMat);

	//char* csvPath = "../../DemuraCSV2BIN/Debug/0218-EdgeMuraTest/0A0B_UNEdge.csv";
	//char* csvPath = "../../DemuraCSV2BIN/Debug/8pcs 55D16/0222-EdgeMuraTest/IN0B_edgecopy_0.6_1_0.8.csv";
	char* csvPath = "../../DemuraCSV2BIN/Debug/Demura.csv";
	
	//char* csvPath1 = ;
	ReadCSV(csvPath, pCompMat);
	//binParam.burnDataIn = pCompMat->burnData;
	address = (uint64_t )pCompMat->burnData;
	//ttemp = *binParam.burnDataIn;
	temp = (float*)(address+ sizeof(float)*2);
	Ptrtemp= (float*)(address);
	binParam.burnDataAddress = address;
	BinData binData;
	binData.DM_burnDataMemoryADD= address;

	R_address = (uint64_t)pCompMat->R;
        binData.GM_R_burnDataMemoryADD = R_address;
    G_address = (uint64_t)pCompMat->G;
        binData.GM_G_burnDataMemoryADD = G_address;
    B_address = (uint64_t)pCompMat->B;
        binData.GM_B_burnDataMemoryADD = B_address;
	

	float tt[10];
	for (int h = 0; h < 10; h++)
	{
		tt[h] = *Ptrtemp++;
	}
	char* as = GetDLLVersion();
	cout << "Dll Version: " << as << endl;
	/*int result=BinCreatApi(
		uTCONtype::uCSOT_FORMAT,
		binParam, csvPath);*/

	/*int result = BinCreatFromMat(
		uTCONtype::uCSOT_FORMAT,
		binParam, csvPath);*/

	/*int result = BinCreatFromMatAddress(
		uTCONtype::uVD_Format,
		binParam, csvPath);*/
	//VD
		/*const char* strpara =
            "{\"$PANEL$PanelResulotionHeight\":2160,\"$PANEL$"
            "PanelResulotionWidth\":3840,"
            "\"$DEMURA$compMode\":0,\"$DEMURA$compNum\":3,\"$DEMURA$"
            "burnChannels\":3,"
            "\"$DEMURA$blockSizeWidth\":8,\"$DEMURA$blockSizeHeight\":8,\"$"
            "DEMURA$lowerLimit\":"
            "3,\"$DEMURA$upperLimit\":255,\"$DEMURA$compGrayList\":[25,60,128"
            "],\"$DEMURA$BIN$FUNCTION$VCOM\":1,"
            "\"$DEMURA$BIN$FUNCTION$Demura\":1,\"$DEMURA$BIN$FUNCTION$GmType\":"
            "3,\"$GAMMA$inBits\":8,\"$GAMMA$outBits\":11,\"$DEMURA$BIN$"
            "FUNCTION$TconType\":1,\"$DEMURA$BIN$FUNCTION$TconIndex\":101,"
            "\"$DEMURA$BIN$PMIC$DefaultVCOM\":64,\"$DEMURA$BIN$CellCode\":"
            "\"BN9652598A\",\"$DEMURA$BIN$ModelCode\":\"12T5461D16\",\"$DEMURA$"
            "BIN$Revision\":\"001\",\"$DEMURA$BIN$Name\":\"_PIXEL_8X8_NIKEM_C.bin\","
            "\"$DEMURA$BIN$PMicCodeName\":\"sample.bin\",\"$SETUP$"
            "PMicCodeDir\":\"../../DemuraCSV2BIN/Debug/PMIC default bin/\",\"$SETUP$TconConfigDir\":\"Tcon_config.json\"}";*/


	//VD
	/*	const char* strpara =
			"{\"$PANEL$PanelResulotionHeight\":2160,\"$PANEL$"
			"PanelResulotionWidth\":3840,"
			"\"$DEMURA$compMode\":0,\"$DEMURA$compNum\":3,\"$DEMURA$"
			"burnChannels\":3,"
			"\"$DEMURA$blockSizeWidth\":8,\"$DEMURA$blockSizeHeight\":8,\"$"
			"DEMURA$lowerLimit\":"
			"3,\"$DEMURA$upperLimit\":255,\"$DEMURA$compGrayList\":[25,60,128"
			"],\"$DEMURA$BIN$FUNCTION$VCOM\":1,"
			"\"$DEMURA$BIN$FUNCTION$Demura\":1,\"$DEMURA$BIN$FUNCTION$GmType\":"
			"3,\"$GAMMA$inBits\":8,\"$GAMMA$outBits\":11,"
			"\"$DEMURA$BIN$TconIndex\":101,"
			"\"$DEMURA$BIN$PMIC$DefaultVCOM\":64,"
			"\"$DEMURA$BIN$PMicCodeName\":\"sample.bin\",\"$SETUP$"
			"PMicCodeDir\":\"../../DemuraCSV2BIN/Debug/PMIC default bin/\",\"$SETUP$TconConfigDir\":\"Tcon_config.json\"}";*/
		//FHD CSOT
		const char* strpara =
			"{\"$PANEL$PanelResulotionHeight\":1080,\"$PANEL$"
			"PanelResulotionWidth\":1920,"
			"\"$DEMURA$compMode\":0,\"$DEMURA$compNum\":3,\"$DEMURA$"
			"burnChannels\":3,"
			"\"$DEMURA$blockSizeWidth\":8,\"$DEMURA$blockSizeHeight\":8,"
			"\"$DEMURA$lowerLimit\":"
			"3,\"$DEMURA$upperLimit\":254,\"$DEMURA$compGrayList\":[25,60,128"
			"],\"$DEMURA$BIN$FUNCTION$VCOM\":0,"
			"\"$DEMURA$BIN$FUNCTION$Demura\":1,\"$DEMURA$BIN$FUNCTION$GmType\":"
			"0,\"$GAMMA$inBits\":8,\"$GAMMA$outBits\":11,"
			"\"$DEMURA$BIN$CellCode\":\"BN9652598A\","
			"\"$DEMURA$BIN$ModelCode\":\"12T5461D16\","
			"\"$DEMURA$BIN$Revision\":\"001\","
			"\"$DEMURA$BIN$PMIC$DefaultVCOM\":64,"
			"\"$DEMURA$BIN$TconIndex\":901,"
			"\"$DEMURA$BIN$PMicCodeName\":\"sample.bin\",\"$SETUP$"
			"PMicCodeDir\":\"../../DemuraCSV2BIN/Debug/PMIC default bin/\",\"$SETUP$TconConfigDir\":\"Tcon_config.json\"}";
		//"\"$DEMURA$blockSizeWidth\":8,\"$DEMURA$blockSizeHeight\":8,"
		//"\"$DEMURA$BIN$TconIndex\":104,"
		//oscar 8k 120Hz
		/*const char* strpara =
			"{\"$PANEL$PanelResulotionHeight\":4320,\"$PANEL$"
			"PanelResulotionWidth\":7680,"
			"\"$DEMURA$compMode\":0,\"$DEMURA$compNum\":3,\"$DEMURA$"
			"burnChannels\":3,"
			"\"$DEMURA$blockSizeWidth\":8,\"$DEMURA$blockSizeHeight\":"
			"8,\"$DEMURA$lowerLimit\":"
			"0,\"$DEMURA$upperLimit\":255,\"$DEMURA$compGrayList\":[25,"
			"60,128,0,0],\"$DEMURA$BIN$FUNCTION$VCOM\":0,"
			"\"$DEMURA$BIN$FUNCTION$Demura\":1,\"$DEMURA$BIN$FUNCTION$"
			"GmType\":0,\"$GAMMA$inBits\":10,\"$GAMMA$outBits\":12,\"$"
			"DEMURA$BIN$FUNCTION$TconType\":9,\"$DEMURA$BIN$PMIC$DefaultVCOM\":368,\"$DEMURA$BIN$Name\":\"_PIXEL_8X8_Oscar_dual.bin"
			"\",\"$DEMURA$BIN$PMicCodeName\":\"12T5461D1820G1_pgma_default.bin\",\"$SETUP$"
			"PMicCodeDir\":\"../../DemuraCSV2BIN/Debug/PMIC default bin/\"}";*/
		/*const char* strpara =
			"{\"$DEMURA$correctImageHeight\":2160,\"$DEMURA$correctImageWidth\":3840,"
			"\"$DEMURA$colorChannels\":1,\"$DEMURA$compNum\":3,\"$DEMURA$burnChannels\":3,"
			"\"$DEMURA$burnWidth\":481,\"$DEMURA$burnHeight\":271,\"$DEMURA$lowerLimit\":"
			"3,\"$DEMURA$upperLimit\":255,\"$DEMURA$compGrayList\":[25,60,128,0,0],\"$BIN$FUNCTION$VCOM\":0,"
			"\"$BIN$FUNCTION$Demura\":1,\"$BIN$FUNCTION$GmType\":1,\"$BIN$FUNCTION$TconType\":6,\"$BIN$"
			"FUNCTION$IQC\":1,\"$BIN$ADD$VCOM\":0,\"$BIN$ADD$Demura\":"
			"0,\"$BIN$ADD$ACC\":1028096,\"$BIN$ADD$IQC\":0,\"$BIN$"
			"ADD$BinSize\":4096, \"$GAMMA$inBits\":10,\"$GAMMA$outBits\":12,\"$BIN$PMIC$VCOM\":70,\"$BIN$PMicCodeDir\":\"F:\\\\cxh\\\\cxh92\\\\DemuraCSV2BIN\\\\Debug\\\\sample.bin\",\"$Demura$BIN$Name\":\"_PIXEL_8X8_HSV532.bin\",\"$BIN$CellCode\":\"BN9652598A\",\"$BIN$ModelCode\":\"12T5461D16\",\"$BIN$Revision\":\"001\"}";
		*/
		binData.VcomData = 34;
		//int result = BinCreatFromMatAddressAndJS(strpara, "../../DemuraCSV2BIN/Debug/", "0A0C234002", binData);
		char binDir[1024] = { 0 };

		int result = BinCreatJSON(strpara, "../../DemuraCSV2BIN/Debug/", "2TCON", binData, binDir);
		cout << "result: " << result << endl;

		Json::Reader reader;
		Json::Value root;
		string ach="";
		if (reader.parse(binDir, root)) {
			//读取根节点信息
			for (int i = 0; i < root["binAdd"].size(); i++) {
				ach = root["binAdd"][i].asString();
				cout << "binDir["<<i<<"]: " << ach << endl;
			}
		}
	//	char** p_binDir = NULL;
	//	char* binDir = NULL;
	//	p_binDir = (char**)malloc(2 * sizeof(char*));     // key
	//	binDir = (char*)malloc(200 * sizeof(char));
	//	p_binDir[0] = binDir;
	//	p_binDir[1] = binDir + 100;
	//	int re = BinCreat_JSON(strpara, "../../DemuraCSV2BIN/Debug/", "0A0C281618", binData, p_binDir);

	//if (pCompMat->burnData != NULL) {
	//	free(pCompMat->burnData);
	//	pCompMat->burnData = NULL;
	//}

	////cout << "result: " << result << endl;
	//cout << "binDir: " << p_binDir[0] << endl;
	//cout << "binDir: " << p_binDir[1] << endl;
	//if (p_binDir != NULL) {
	//	free(p_binDir);
	//	p_binDir = NULL;
	//}
	//free(binDir);
    return 0;
}
